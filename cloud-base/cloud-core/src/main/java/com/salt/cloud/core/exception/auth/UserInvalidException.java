

package com.salt.cloud.core.exception.auth;


import com.salt.cloud.core.constants.RestCodeConstants;
import com.salt.cloud.core.exception.BaseException;

/**
 * Created by ace on 2017/9/8.
 */
public class UserInvalidException extends BaseException {
    public UserInvalidException(String message) {
        super(message, RestCodeConstants.EX_USER_PASS_INVALID_CODE);
    }
}
