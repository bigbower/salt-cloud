

package com.salt.cloud.core.exception.auth;


import com.salt.cloud.core.constants.RestCodeConstants;
import com.salt.cloud.core.exception.BaseException;

/**
 * Created by ace on 2017/9/12.
 */
public class ClientForbiddenException extends BaseException {
    public ClientForbiddenException(String message) {
        super(message, RestCodeConstants.EX_CLIENT_FORBIDDEN_CODE);
    }

}
