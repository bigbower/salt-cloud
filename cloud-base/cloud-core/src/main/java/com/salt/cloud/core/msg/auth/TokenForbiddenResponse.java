package com.salt.cloud.core.msg.auth;

import com.salt.cloud.core.constants.RestCodeConstants;
import com.salt.cloud.core.msg.BaseResponse;

/**
 * Created by ace on 2017/8/25.
 */
public class TokenForbiddenResponse  extends BaseResponse {
    public TokenForbiddenResponse(String message) {
        super(RestCodeConstants.EX_USER_FORBIDDEN_CODE, message);
    }
}
