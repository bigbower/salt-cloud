package com.salt.cloud.core.exception.base;

import com.salt.cloud.core.exception.BaseException;

/**
 * Dao基础异常类
 * @author kian
 * @version 2018/1/13.
 */
public class DAOException extends BaseException {
}
