

package com.salt.cloud.core.msg;


import com.salt.cloud.core.constants.RestCodeConstants;

/**
 * Created by Ace on 2017/6/11.
 */
public class ObjectRestResponse<T> extends BaseResponse {

    T data;

    public ObjectRestResponse data(T data) {
        this.setData(data);
        return this;
    }


    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public static ObjectRestResponse ok(Object data) {
        return new ObjectRestResponse<Object>().data(data);
    }
    public static ObjectRestResponse ok(String message) {
        ObjectRestResponse objectRestResponse=new ObjectRestResponse();
        objectRestResponse.setMessage(message);
        return objectRestResponse;
    }

    public static ObjectRestResponse ok() {
        return new ObjectRestResponse<Object>();
    }

    public static ObjectRestResponse error(int statusCode, String message) {
        ObjectRestResponse objectRestResponse= new ObjectRestResponse();
        objectRestResponse.setStatus(statusCode);
        objectRestResponse.setMessage(message);
        return objectRestResponse;
    }
    public static ObjectRestResponse error(String message) {
        ObjectRestResponse objectRestResponse= new ObjectRestResponse();
        objectRestResponse.setStatus(RestCodeConstants.EX_BUSINESS_BASE_CODE);
        objectRestResponse.setMessage(message);
        return objectRestResponse;
    }
}
