

package com.salt.cloud.core.util;

import com.salt.cloud.core.constants.CommonConstants;
import com.salt.cloud.core.constants.RedisKeyConstants;

import java.util.Date;

/**
 * @author kian
 * @create 2018/3/11.
 */
public class RedisKeyUtil {
    /**
     *
     * @param userId
     * @param expire
     * @return
     */
    public static String buildUserAbleKey(String userId,String cliendId,Date expire){
        return CommonConstants.REDIS_USER_TOKEN +cliendId+ RedisKeyConstants.USER_ABLE + userId + ":" + expire.getTime();
    }


    /**
     *
     * @param userId
     * @param expire
     * @return
     */
    public static String buildUserAbleKeyLike(String userId,String cliendId){
        return CommonConstants.REDIS_USER_TOKEN +cliendId+ RedisKeyConstants.USER_ABLE + userId + ":*" ;
    }

    /**
     * 
     * @param userId
     * @param expire
     * @return
     */
    public static String buildUserDisableKey(String userId,String cliendId,Date expire){
        return CommonConstants.REDIS_USER_TOKEN +cliendId+  RedisKeyConstants.USER_DISABLE + userId + ":" + expire.getTime();
    }
}
