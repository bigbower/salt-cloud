package com.salt.cloud.core.constants;

/**
 * rest接口返回码
 *
 * @author kian
 * @version 2017/8/23
 */
public class RestCodeConstants {
    //jwt异常
    public static final Integer EX_JWT_INVALID_CODE = 40001;
    public static final Integer EX_JWT_ILLEGAL_CODE = 40002;
    public static final Integer EX_JWT_EXPIRE_CODE = 40003;

    // 用户token异常
    public static final Integer EX_USER_INVALID_CODE = 40101;
    public static final Integer EX_USER_PASS_INVALID_CODE = 40102;
    public static final Integer EX_USER_FORBIDDEN_CODE = 40103;
    // 客户端token异常
    public static final Integer EX_CLIENT_INVALID_CODE = 40201;
    public static final Integer EX_CLIENT_FORBIDDEN_CODE = 40202;
    // 业务异常返回码
    public static final Integer EX_BUSINESS_BASE_CODE = 30101;


}
