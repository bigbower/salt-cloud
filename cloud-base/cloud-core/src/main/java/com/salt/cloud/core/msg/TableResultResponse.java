

package com.salt.cloud.core.msg;

import com.salt.cloud.core.constants.RestCodeConstants;

import java.util.List;

/**
 * ${DESCRIPTION}
 *
 * @author kian
 * @version 2017-06-14 22:40
 */
public class TableResultResponse<T> extends BaseResponse {

    TableData<T> data;

    public TableResultResponse(long total, List<T> rows) {
        this.data = new TableData<T>(total, rows);
    }
    //增加返回更多的分页参数
    public TableResultResponse(int pageNum,int pageSize,long total,int pages, List<T> rows) {
        this.data = new TableData<T>(pageNum,pageSize,total,pages, rows);
    }

    public TableResultResponse() {
        this.data = new TableData<T>();
    }

    TableResultResponse<T> total(int total) {
        this.data.setTotal(total);
        return this;
    }

    TableResultResponse<T> total(List<T> rows) {
        this.data.setRows(rows);
        return this;
    }

    public TableData<T> getData() {
        return data;
    }

    public void setData(TableData<T> data) {
        this.data = data;
    }

    public class TableData<T> {

        public int getPageNum() {
            return pageNum;
        }

        public void setPageNum(int pageNum) {
            this.pageNum = pageNum;
        }

        public int getPageSize() {
            return pageSize;
        }

        public void setPageSize(int pageSize) {
            this.pageSize = pageSize;
        }

        public int getPages() {
            return pages;
        }

        public void setPages(int pages) {
            this.pages = pages;
        }

        /**
         * 页码，从1开始
         */
        private int pageNum;
        /**
         * 页面大小
         */
        private int pageSize;
        /**
         * 总数
         */
        private long total;
        /**
         * 总页数
         */
        private int pages;

        List<T> rows;

        public TableData(long total, List<T> rows) {
            this.total = total;
            this.rows = rows;
        }

        public TableData(int pageNum,int pageSize,long total,int pages, List<T> rows) {
            this.pageNum=pageNum;
            this.pageSize=pageSize;
            this.pages=pages;
            this.total = total;
            this.rows = rows;
        }

        public TableData() {
        }

        public long getTotal() {
            return total;
        }

        public void setTotal(long total) {
            this.total = total;
        }

        public List<T> getRows() {
            return rows;
        }

        public void setRows(List<T> rows) {
            this.rows = rows;
        }
    }

    public static TableResultResponse ok() {
        return new TableResultResponse();
    }

    public static TableResultResponse error(int statusCode, String message) {
        TableResultResponse tableResultResponse= new TableResultResponse();
        tableResultResponse.setStatus(statusCode);
        tableResultResponse.setMessage(message);
        return tableResultResponse;
    }
    public static TableResultResponse error(String message) {
        TableResultResponse tableResultResponse= new TableResultResponse();
        tableResultResponse.setStatus(RestCodeConstants.EX_BUSINESS_BASE_CODE);
        tableResultResponse.setMessage(message);
        return tableResultResponse;
    }
}
