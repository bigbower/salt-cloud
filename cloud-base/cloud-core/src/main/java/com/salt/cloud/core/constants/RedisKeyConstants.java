

package com.salt.cloud.core.constants;

/**
 * @author kian
 * @version 2018/1/14.
 */
public class RedisKeyConstants {
    public final static String USER_DEPART_PREFIX = "USER:DEPART";
    public final static String ZUUL_ROUTE_KEY = "GATE:ROUTE";
    public final static String USER_DISABLE = ":dis:";
    public final static String USER_ABLE = ":able:";
    public static final String REDIS_USER_PRI_KEY = "AG:AUTH:JWT:PRI";
    public static final String REDIS_USER_PUB_KEY = "AG:AUTH:JWT:PUB";
    public static final String REDIS_SERVICE_PRI_KEY = "AG:AUTH:CLIENT:PRI";
    public static final String REDIS_SERVICE_PUB_KEY = "AG:AUTH:CLIENT:PUB";
}
