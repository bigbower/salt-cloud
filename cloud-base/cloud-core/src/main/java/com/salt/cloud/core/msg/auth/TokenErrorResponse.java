package com.salt.cloud.core.msg.auth;

import com.salt.cloud.core.constants.RestCodeConstants;
import com.salt.cloud.core.msg.BaseResponse;

/**
 * Created by ace on 2017/8/23.
 */
public class TokenErrorResponse extends BaseResponse {
    public TokenErrorResponse(String message) {
        super(RestCodeConstants.EX_USER_INVALID_CODE, message);
    }
}
