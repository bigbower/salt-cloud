package com.salt.cloud.common.data;

import java.util.List;

/**
 * @author kian
 * @create 2020/1/21.
 */
public interface IUserDepartDataService {
    /**
     * 根据用户获取用户可访问的数据部门Id
     * @param userId
     * @return
     */
    public List<String> getUserDataDepartIds(String userId);
}
