package com.salt.cloud.common.biz;

import com.salt.cloud.common.mapper.CommonMapper;
import com.salt.cloud.common.util.EntityUtils;

/**
 * 基础业务类
 * @author kian
 * @version 2018/1/13.
 */
public abstract class BusinessBiz<M extends CommonMapper<T>, T>  extends BaseBiz<M, T> {
    @Override
    public void insertSelective(T entity) {
        EntityUtils.setCreatAndUpdatInfo(entity);
         super.insertSelective(entity);
    }

    @Override
    public void updateById(T entity) {
        EntityUtils.setUpdatedInfo(entity);
         super.updateById(entity);
    }

    @Override
    public void updateSelectiveById(T entity) {
        EntityUtils.setUpdatedInfo(entity);
         super.updateSelectiveById(entity);
    }


    @Override
    public int insertSelective2(T entity) {
        EntityUtils.setCreatAndUpdatInfo(entity);
        return super.insertSelective2(entity);
    }

    @Override
    public int updateById2(T entity) {
        EntityUtils.setUpdatedInfo(entity);
        return super.updateById2(entity);
    }

    @Override
    public int updateSelectiveById2(T entity) {
        EntityUtils.setUpdatedInfo(entity);
        return super.updateSelectiveById2(entity);
    }
}
