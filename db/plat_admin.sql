/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50636
Source Host           : 127.0.0.1:3306
Source Database       : plat_admin

Target Server Type    : MYSQL
Target Server Version : 50636
File Encoding         : 65001

Date: 2020-01-25 23:24:45
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for auth_client
-- ----------------------------
DROP TABLE IF EXISTS `auth_client`;
CREATE TABLE `auth_client` (
  `id` varchar(36) NOT NULL,
  `code` varchar(255) DEFAULT NULL COMMENT '服务编码',
  `secret` varchar(255) DEFAULT NULL COMMENT '服务密钥',
  `name` varchar(255) DEFAULT NULL COMMENT '服务名',
  `locked` char(1) DEFAULT NULL COMMENT '是否锁定',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `crt_time` datetime DEFAULT NULL COMMENT '创建时间',
  `crt_user` varchar(255) DEFAULT NULL COMMENT '创建人',
  `crt_name` varchar(255) DEFAULT NULL COMMENT '创建人姓名',
  `crt_host` varchar(255) DEFAULT NULL COMMENT '创建主机',
  `upd_time` datetime DEFAULT NULL COMMENT '更新时间',
  `upd_user` varchar(255) DEFAULT NULL COMMENT '更新人',
  `upd_name` varchar(255) DEFAULT NULL COMMENT '更新姓名',
  `upd_host` varchar(255) DEFAULT NULL COMMENT '更新主机',
  `attr1` varchar(255) DEFAULT NULL,
  `attr2` varchar(255) DEFAULT NULL,
  `attr3` varchar(255) DEFAULT NULL,
  `attr4` varchar(255) DEFAULT NULL,
  `attr5` varchar(255) DEFAULT NULL,
  `attr6` varchar(255) DEFAULT NULL,
  `attr7` varchar(255) DEFAULT NULL,
  `attr8` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of auth_client
-- ----------------------------
INSERT INTO `auth_client` VALUES ('1', 'cloud-gate', '123456', 'cloud-gate', '0', '服务网关', null, '', '', '', '2017-07-07 21:51:32', '1', '管理员', '0:0:0:0:0:0:0:1', '', '', '', '', '', '', '', '');
INSERT INTO `auth_client` VALUES ('1e460c3dee67467f94585d9c2a518b76', 'health-auth', '123456', 'health-auth', '0', 'health-auth', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `auth_client` VALUES ('20', 'cloud-dict', '123456', 'cloud-dict', '0', '数据字典服务', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `auth_client` VALUES ('3', 'cloud-admin', '123456', 'cloud-admin', '0', '', null, null, null, null, '2017-07-06 21:42:17', '1', '管理员', '0:0:0:0:0:0:0:1', null, null, null, null, null, null, null, null);
INSERT INTO `auth_client` VALUES ('40', 'health-user', '123456', 'health-user', '0', 'health-user', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `auth_client` VALUES ('6', 'cloud-auth', '123456', 'cloud-auth', '0', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `auth_client` VALUES ('7', 'cloud-tool', '123456', 'cloud-tool', '0', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `auth_client` VALUES ('e2df317204ad4325a8fddafa7127bc0f', 'health-web', '123456', 'health-web', '0', 'health-web', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);

-- ----------------------------
-- Table structure for auth_client_service
-- ----------------------------
DROP TABLE IF EXISTS `auth_client_service`;
CREATE TABLE `auth_client_service` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service_id` varchar(255) DEFAULT NULL,
  `client_id` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `crt_time` datetime DEFAULT NULL,
  `crt_user` varchar(255) DEFAULT NULL,
  `crt_name` varchar(255) DEFAULT NULL,
  `crt_host` varchar(255) DEFAULT NULL,
  `attr1` varchar(255) DEFAULT NULL,
  `attr2` varchar(255) DEFAULT NULL,
  `attr3` varchar(255) DEFAULT NULL,
  `attr4` varchar(255) DEFAULT NULL,
  `attr5` varchar(255) DEFAULT NULL,
  `attr6` varchar(255) DEFAULT NULL,
  `attr7` varchar(255) DEFAULT NULL,
  `attr8` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=337 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of auth_client_service
-- ----------------------------
INSERT INTO `auth_client_service` VALUES ('21', '4', '5', null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `auth_client_service` VALUES ('303', '3', '6', null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `auth_client_service` VALUES ('304', '7', '6', null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `auth_client_service` VALUES ('332', '3', '20', null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `auth_client_service` VALUES ('333', '20', '1', null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `auth_client_service` VALUES ('334', '3', '1', null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `auth_client_service` VALUES ('335', '6', '1', null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `auth_client_service` VALUES ('336', '1e460c3dee67467f94585d9c2a518b76', '1', null, null, null, null, null, null, null, null, null, null, null, null, null);

-- ----------------------------
-- Table structure for base_depart
-- ----------------------------
DROP TABLE IF EXISTS `base_depart`;
CREATE TABLE `base_depart` (
  `id` varchar(36) NOT NULL COMMENT '主键',
  `name` varchar(255) DEFAULT NULL COMMENT '组织名称',
  `parent_id` varchar(36) DEFAULT NULL COMMENT '上级节点',
  `code` varchar(100) DEFAULT NULL COMMENT '编码',
  `path` varchar(4000) DEFAULT NULL COMMENT '路劲',
  `type` varchar(36) DEFAULT NULL COMMENT '部门类型',
  `crt_user_name` varchar(255) DEFAULT NULL COMMENT '创建人',
  `crt_user_id` varchar(36) DEFAULT NULL COMMENT '创建人ID',
  `crt_time` datetime DEFAULT NULL COMMENT '创建时间',
  `upd_user_name` varchar(255) DEFAULT NULL COMMENT '最后更新人',
  `upd_user_id` varchar(36) DEFAULT NULL COMMENT '最后更新人ID',
  `upd_time` datetime DEFAULT NULL COMMENT '最后更新时间',
  `attr1` varchar(255) DEFAULT NULL,
  `attr2` varchar(255) DEFAULT NULL,
  `attr3` varchar(255) DEFAULT NULL,
  `attr4` varchar(255) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_code` (`code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of base_depart
-- ----------------------------
INSERT INTO `base_depart` VALUES ('2b06ea24a3cf40659c183da3242cc669', '子节点', 'd8bb2fc8d51745458f77a78c42bf60ab', '1518502802626_tmp', null, null, '测试租户', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '2018-02-13 14:20:03', '测试租户', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '2018-02-13 14:20:03', null, null, null, null, '1ec08564dcc344018d6aaa910068f0f0');
INSERT INTO `base_depart` VALUES ('8782fcb72842440fa3254bf72717d7b0', '子节点', 'd8bb2fc8d51745458f77a78c42bf60ab', '1518500776414_tmp', null, null, '王小七', '59c28b8593c2472e99e54e3ed9b6efe8', '2018-02-13 13:46:17', '王小七', '59c28b8593c2472e99e54e3ed9b6efe8', '2018-02-13 13:46:17', null, null, null, null, '1ec08564dcc344018d6aaa910068f0f0');
INSERT INTO `base_depart` VALUES ('9a255e4ca80842d0bdb18029d916fb1e', '子节点', '8782fcb72842440fa3254bf72717d7b0', '1518505073066_tmp', null, null, '测试租户', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '2018-02-13 14:57:53', '测试租户', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '2018-02-13 14:57:53', null, null, null, null, '1ec08564dcc344018d6aaa910068f0f0');
INSERT INTO `base_depart` VALUES ('9f871185e4a648dbbdc7bb880c0eb90f', '子节点', '8782fcb72842440fa3254bf72717d7b0', '1518507609891_tmp', null, null, '测试租户', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '2018-02-13 15:40:10', '测试租户', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '2018-02-13 15:40:10', null, null, null, null, '1ec08564dcc344018d6aaa910068f0f0');
INSERT INTO `base_depart` VALUES ('a269b706b0b34ee89dd7e8e08e8514bc', '子节点', '8782fcb72842440fa3254bf72717d7b0', '1518507610091_tmp', null, null, '测试租户', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '2018-02-13 15:40:10', '测试租户', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '2018-02-13 15:40:10', null, null, null, null, '1ec08564dcc344018d6aaa910068f0f0');
INSERT INTO `base_depart` VALUES ('d583e7de6d2d48b78fb3c7dcb180cb1f', '普通租户', 'root', '1518438046753_tmp', null, 'group', 'Mr.AG', '1', '2018-02-12 20:20:47', 'Mr.AG', '1', '2018-02-12 20:26:26', null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `base_depart` VALUES ('d8bb2fc8d51745458f77a78c42bf60ab', '测试租户部门', '-1', '1518449909887', null, 'group', '测试租户', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '2018-02-12 23:38:30', '测试租户', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '2018-02-12 23:40:46', null, null, null, null, '1ec08564dcc344018d6aaa910068f0f0');
INSERT INTO `base_depart` VALUES ('root', '超级租户部门', '-1', '1518437538385', null, null, 'Mr.AG', '1', '2018-02-12 20:12:18', 'Mr.AG', '1', '2018-02-12 20:13:55', null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24');

-- ----------------------------
-- Table structure for base_depart_user
-- ----------------------------
DROP TABLE IF EXISTS `base_depart_user`;
CREATE TABLE `base_depart_user` (
  `id` varchar(36) NOT NULL,
  `user_id` varchar(36) DEFAULT NULL,
  `depart_id` varchar(36) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of base_depart_user
-- ----------------------------
INSERT INTO `base_depart_user` VALUES ('11c493ebe5fc4075952ace01901301f6', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `base_depart_user` VALUES ('4f9ba6144cae40baad260d1aa94e9351', '5b29e88e363c41c394ad50cf830f6e0a', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `base_depart_user` VALUES ('5c5330952de94f4d8e9534b69616ec7d', '59c28b8593c2472e99e54e3ed9b6efe8', 'd8bb2fc8d51745458f77a78c42bf60ab', '1ec08564dcc344018d6aaa910068f0f0');
INSERT INTO `base_depart_user` VALUES ('ba3000d719a94459bc432b59794bf4ff', 'f2c2fa758a7f418d9d7cfebf4002e523', 'root', 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `base_depart_user` VALUES ('d2ead90dbff449db979bf454cb99b6c1', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'ac88ceb386aa4231b09bf472cb937c24');

-- ----------------------------
-- Table structure for base_element
-- ----------------------------
DROP TABLE IF EXISTS `base_element`;
CREATE TABLE `base_element` (
  `id` varchar(36) NOT NULL,
  `code` varchar(255) DEFAULT NULL COMMENT '资源编码',
  `type` varchar(255) DEFAULT NULL COMMENT '资源类型',
  `name` varchar(255) DEFAULT NULL COMMENT '资源名称',
  `uri` varchar(255) DEFAULT NULL COMMENT '资源路径',
  `menu_id` varchar(255) DEFAULT NULL COMMENT '资源关联菜单',
  `parent_id` varchar(255) DEFAULT NULL,
  `path` varchar(2000) DEFAULT NULL COMMENT '资源树状检索路径',
  `method` varchar(10) DEFAULT NULL COMMENT '资源请求类型',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `crt_time` datetime DEFAULT NULL,
  `crt_user` varchar(255) DEFAULT NULL,
  `crt_name` varchar(255) DEFAULT NULL,
  `crt_host` varchar(255) DEFAULT NULL,
  `attr1` varchar(255) DEFAULT NULL,
  `attr2` varchar(255) DEFAULT NULL,
  `attr3` varchar(255) DEFAULT NULL,
  `attr4` varchar(255) DEFAULT NULL,
  `attr5` varchar(255) DEFAULT NULL,
  `attr6` varchar(255) DEFAULT NULL,
  `attr7` varchar(255) DEFAULT NULL,
  `attr8` varchar(255) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL COMMENT '租户Id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of base_element
-- ----------------------------
INSERT INTO `base_element` VALUES ('10', 'menuManager:btn_add', 'button', '新增', '/admin/menu', '6', null, null, 'POST', '', null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('103593127199487794f9fd8fd573db5f', 'positionManager:btn_depart', 'button', '授权部门权限', '/admin/position/{*}/depart', 'b5211cc69d234b28a97f27e63edc9a58', null, null, 'PUT', null, '2018-02-11 12:44:45', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('11', 'menuManager:btn_edit', 'button', '编辑', '/admin/menu/{*}', '6', '', '', 'PUT', '', '2017-06-24 00:00:00', '', '', '', '', '', '', '', '', '', '', '', null);
INSERT INTO `base_element` VALUES ('12', 'menuManager:btn_del', 'button', '删除', '/admin/menu/{*}', '6', '', '', 'DELETE', '', '2017-06-24 00:00:00', '', '', '', '', '', '', '', '', '', '', '', null);
INSERT INTO `base_element` VALUES ('13', 'menuManager:btn_element_add', 'button', '新增元素', '/admin/element', '6', null, null, 'POST', '', null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('14', 'menuManager:btn_element_edit', 'button', '编辑元素', '/admin/element/{*}', '6', null, null, 'PUT', '', null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('15', 'menuManager:btn_element_del', 'button', '删除元素', '/admin/element/{*}', '6', null, null, 'DELETE', '', null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('16', 'groupManager:btn_add', 'button', '新增', '/admin/group', '7', null, null, 'POST', '', null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('17', 'groupManager:btn_edit', 'button', '编辑', '/admin/group/{*}', '7', null, null, 'PUT', '', null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('18', 'groupManager:btn_del', 'button', '删除', '/admin/group/{*}', '7', null, null, 'DELETE', '', null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('19', 'groupManager:btn_userManager', 'button', '分配用户', '/admin/group/{*}/user', '7', null, null, 'PUT', '', null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('20', 'groupManager:btn_resourceManager', 'button', '分配权限', '/admin/group/{*}/authority', '7', null, null, 'GET', '', null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('21', 'groupManager:menu', 'uri', '分配菜单', '/admin/group/{*}/authority/menu', '7', null, null, 'POST', '', null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('21b3fe683a6040ed8c57423e55cd94a2', 'groupManager:authorize_menu', 'button', '菜单下发', '/admin/group/{*}/authorize/menu', '7', null, null, 'POST', null, '2018-02-12 14:54:57', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `base_element` VALUES ('22', 'groupManager:element', 'uri', '分配资源', '/admin/group/{*}/authority/element', '7', null, null, 'POST', '', null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('23', 'userManager:view', 'uri', '查看', '/admin/user/{*}', '1', '', '', 'GET', '', '2017-06-26 00:00:00', '', '', '', '', '', '', '', '', '', '', '', null);
INSERT INTO `base_element` VALUES ('24', 'menuManager:view', 'uri', '查看', '/admin/menu/{*}', '6', '', '', 'GET', '', '2017-06-26 00:00:00', '', '', '', '', '', '', '', '', '', '', '', null);
INSERT INTO `base_element` VALUES ('24ecd755a1ea4cf6b76c8b72ea1858fe', 'tenantManager:btn_user', 'button', '授予用户', '/admin/tenant/user', '7574b969c9fa4e5895d6cc9c2b8a9a62', null, null, 'POST', null, '2018-02-12 21:05:48', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `base_element` VALUES ('261687195b814cfc838bf1d45954abb3', 'appUserManager:btn_del', 'button', '会员账户删除', '/app/admin/appUser/{*}', '342a587a51d647a1b53c6d97363cf428', null, null, 'DELETE', null, '2018-05-26 10:15:24', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `base_element` VALUES ('27', 'menuManager:element_view', 'uri', '查看', '/admin/element/{*}', '6', null, null, 'GET', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('28', 'groupManager:view', 'uri', '查看', '/admin/group/{*}', '7', null, null, 'GET', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('2b7b250b2b7346759938a05da57d2fcb', 'tenantManager:view', 'uri', '查看租户', '/admin/tenant/{*}', '7574b969c9fa4e5895d6cc9c2b8a9a62', null, null, 'GET', null, '2018-02-09 12:26:27', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('3', 'userManager:btn_add', 'button', '新增', '/admin/user', '1', null, null, 'POST', '', null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('32', 'groupTypeManager:view', 'uri', '查看', '/admin/groupType/{*}', '8', null, null, 'GET', '', null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('33', 'groupTypeManager:btn_add', 'button', '新增', '/admin/groupType', '8', null, null, 'POST', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('34', 'groupTypeManager:btn_edit', 'button', '编辑', '/admin/groupType/{*}', '8', null, null, 'PUT', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('35', 'groupTypeManager:btn_del', 'button', '删除', '/admin/groupType/{*}', '8', null, null, 'DELETE', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('375b04fe927c44e88593d02670acd90f', 'groupManager:btn_authorizeManager', 'button', '权限下发', '/admin/group/{*}/authorize', '7', null, null, 'GET', null, '2018-02-12 14:52:55', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `base_element` VALUES ('39925b8263664c9e923f9c34f9bc2a37', 'positionManager:btn_user', 'button', '分配岗位人员', '/admin/position/{*}/user', 'b5211cc69d234b28a97f27e63edc9a58', null, null, 'PUT', null, '2018-02-10 16:49:57', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('3c21f1959be4416ca85c17157fd2d9ab', 'appUserManager:btn_view', 'uri', '会员账户查看', '/app/admin/appUser/{*}', '342a587a51d647a1b53c6d97363cf428', null, null, 'GET', null, '2018-05-26 10:16:45', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `base_element` VALUES ('4', 'userManager:btn_edit', 'button', '编辑', '/admin/user/{*}', '1', null, null, 'PUT', '', null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('41', 'gateLogManager:view', 'button', '查看', '/admin/gateLog/{*}', '27', null, null, 'GET', '', '2017-07-01 00:00:00', '1', 'admin', '0:0:0:0:0:0:0:1', null, null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('415eac050ab64fc3b41b11e43645f3b5', 'oauthClientDetailsManager:btn_view', 'uri', '查看客户端', '/auth/oauthClientDetails/{*}', '68ee45b69c0f41cb9d6a5c33d46fb8a6', null, null, 'GET', null, '2018-03-28 20:20:48', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `base_element` VALUES ('42', 'serviceManager:view', 'uri', '查看', '/auth/service/{*}', '30', null, null, 'GET', null, '2017-12-26 20:17:42', '1', 'Mr.AG', '127.0.0.1', null, null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('43', 'serviceManager:btn_add', 'button', '新增', '/auth/service', '30', null, null, 'POST', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('44', 'serviceManager:btn_edit', 'button', '编辑', '/auth/service/{*}', '30', null, null, 'PUT', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('45', 'serviceManager:btn_del', 'button', '删除', '/auth/service/{*}', '30', null, null, 'DELETE', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('46', 'serviceManager:btn_clientManager', 'button', '授权服务', '/auth/service/{*}/client', '30', null, null, 'POST', null, '2017-12-30 16:32:48', '1', 'Mr.AG', '127.0.0.1', null, null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('47', 'serviceManager:btn_buidlProject', 'button', '构建工程', '/auth/generator/build', '30', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('48', 'dictTypeManager:view', 'uri', '查看字典目录', '/dict/dictType/{*}', '21', null, null, 'GET', null, '2017-12-26 20:17:42', '1', 'Mr.AG', '127.0.0.1', null, null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('49', 'dictTypeManager:btn_add', 'button', '新增字典目录', '/dict/dictType', '21', null, null, 'POST', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('4ee4f57a2d92494abe2595bff4f32057', 'groupManager:authorize_element', 'button', '资源下发', '/admin/group/{*}/authorize/element', '7', null, null, 'POST', null, '2018-02-12 14:55:30', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `base_element` VALUES ('5', 'userManager:btn_del', 'button', '删除', '/admin/user', '1', null, null, 'DELETE', '', null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('50', 'dictTypeManager:btn_edit', 'button', '编辑字典目录', '/dict/dictType/{*}', '21', null, null, 'PUT', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('51', 'dictTypeManager:btn_del', 'button', '删除字典目录', '/dict/dictType/{*}', '21', null, null, 'DELETE', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('52', 'dictValueManager:view', 'uri', '查看字典值', '/dict/dictValue/{*}', '21', null, null, 'GET', null, '2017-12-26 20:17:42', '1', 'Mr.AG', '127.0.0.1', null, null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('53', 'dictValueManager:btn_add', 'button', '新增字典值', '/dict/dictValue', '21', null, null, 'POST', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('54', 'dictValueManager:btn_edit', 'button', '编辑字典值', '/dict/dictValue/{*}', '21', null, null, 'PUT', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('55', 'dictValueManager:btn_del', 'button', '删除字典值', '/dict/dictValue/{*}', '21', null, null, 'DELETE', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('56', 'departManager:view', 'uri', '查看部门值', '/admin/depart/{*}', 'b5211cc69d234b28a97f27e63edc9a58', null, null, 'GET', null, '2017-12-26 20:17:42', '1', 'Mr.AG', '127.0.0.1', null, null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('57', 'departManager:btn_add', 'button', '新增部门值', '/admin/depart', 'b5211cc69d234b28a97f27e63edc9a58', null, null, 'POST', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('58', 'departManager:btn_edit', 'button', '编辑部门值', '/admin/depart/{*}', 'b5211cc69d234b28a97f27e63edc9a58', null, null, 'PUT', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('59', 'departManager:btn_del', 'button', '删除部门值', '/admin/depart/{*}', 'b5211cc69d234b28a97f27e63edc9a58', null, null, 'DELETE', null, null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('6d7ea83bb71d476dbfbda2bcbc1a01bd', 'positionManager:btn_del', 'button', '删除岗位', '/admin/position/{*}', 'b5211cc69d234b28a97f27e63edc9a58', null, null, 'DELETE', null, '2018-02-10 16:47:45', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('73db20cb63e6439f80b2f992e64f545d', 'gatewayRouteManager:btn_edit', 'button', '编辑路由', '/auth/gatewayRoute/{*}', '68ee45b69c0f41cb9d6a5c33d46fb7a6', null, null, 'PUT', null, '2018-02-25 13:46:18', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `base_element` VALUES ('76797ac67e9a4b6ba9177d26a603ffb0', 'oauthClientDetailsManager:btn_add', 'button', '新增客户端', '/auth/oauthClientDetails', '68ee45b69c0f41cb9d6a5c33d46fb8a6', null, null, 'POST', null, '2018-03-28 20:20:22', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `base_element` VALUES ('7a16202657c3478c8b4ece27a792e026', 'positionManager:btn_group', 'button', '分配岗位角色', '/admin/position/{*}/group', 'b5211cc69d234b28a97f27e63edc9a58', null, null, 'PUT', null, '2018-02-10 16:51:31', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('8b5ff030c76341059a335e0653fef5ea', 'departManager:btn_user_del', 'button', '删除部门人员', '/admin/depart/user', 'b5211cc69d234b28a97f27e63edc9a58', null, null, 'DELETE', null, '2018-02-10 16:45:10', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('9', 'menuManager:element', 'uri', '按钮页面', '/admin/element/{*}', '6', null, null, 'GET', '', null, null, null, null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('a03cac538f794958bc7fa6458f85b8ae', 'tenantManager:btn_del', 'uri', '删除租户', '/admin/tenant/{*}', '7574b969c9fa4e5895d6cc9c2b8a9a62', null, null, 'DELETE', null, '2018-02-09 12:28:42', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('a440bc6a31f6457493c7461fd9afae4b', 'appUserManager:btn_add', 'button', '会员账户新增', '/app/admin/appUser/{*}', '342a587a51d647a1b53c6d97363cf428', null, null, 'POST', null, '2018-05-26 10:16:02', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `base_element` VALUES ('af44e799b90240d1a4f51a36ae3995d1', 'oauthClientDetailsManager:btn_edit', 'button', '编辑客户端', '/auth/oauthClientDetails/{*}', '68ee45b69c0f41cb9d6a5c33d46fb8a6', null, null, 'PUT', null, '2018-03-28 20:19:08', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `base_element` VALUES ('c6469be83d8e4d5ca404b46b347f7d4c', 'positionManager:btn_add', 'button', '新增岗位', '/admin/position', 'b5211cc69d234b28a97f27e63edc9a58', null, null, 'POST', null, '2018-02-10 16:47:23', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('cac49646909c4750b23f223f53eafc5b', 'positionManager:btn_edit', 'button', '编辑岗位', '/admin/posision/{*}', 'b5211cc69d234b28a97f27e63edc9a58', null, null, 'PUT', null, '2018-02-10 16:48:24', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('cdf65eea98fa4679bd3f1041d5c06d68', 'gatewayRouteManager:view', 'uri', '查看路由', '/auth/gatewayRoute/{*}', '68ee45b69c0f41cb9d6a5c33d46fb7a6', null, null, 'GET', null, '2018-02-25 13:42:23', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `base_element` VALUES ('d26625c13796423c8353176197cd7f0d', 'gatewayRouteManager:btn_del', 'uri', '禁用路由', '/auth/gatewayRoute/{*}', '68ee45b69c0f41cb9d6a5c33d46fb7a6', null, null, 'DELETE', null, '2018-02-25 13:47:19', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `base_element` VALUES ('d2773b3568c6438c8f9cc21b06b660ee', 'departManager:btn_user_add', 'button', '新增部门人员', '/admin/depart/user', 'b5211cc69d234b28a97f27e63edc9a58', null, null, 'POST', null, '2018-02-10 16:44:07', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('d6b714eff47a42d0a654e55c39304dd5', 'tenantManager:btn_edit', 'button', '编辑租户', '/admin/tenant/{*}', '7574b969c9fa4e5895d6cc9c2b8a9a62', null, null, 'PUT', null, '2018-02-09 12:29:26', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('dd68f557717a483a9b808182c8557e01', 'appUserManager:btn_edit', 'button', '会员账户编辑', '/app/admin/appUser/{*}', '342a587a51d647a1b53c6d97363cf428', null, null, 'PUT', null, '2018-05-26 10:13:44', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `base_element` VALUES ('e18f6f0dde564e62ba91de1657595ddc', 'oauthClientDetailsManager:btn_del', 'button', '删除客户端', '/auth/oauthClientDetails/{*}', '68ee45b69c0f41cb9d6a5c33d46fb8a6', null, null, 'DELETE', null, '2018-03-28 20:19:55', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `base_element` VALUES ('eddad1f3d2d54ba0ac50e8c4781764cb', 'tenantManager:btn_add', 'button', '新增租户', '/admin/tenant', '7574b969c9fa4e5895d6cc9c2b8a9a62', null, null, 'POST', null, '2018-02-09 12:27:11', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('ef73cc9bf70d448caf72e3f45015700a', 'positionManager:view', 'uri', '查看岗位', '/admin/depart/{*}', 'b5211cc69d234b28a97f27e63edc9a58', null, null, 'GET', null, '2018-02-10 16:48:49', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_element` VALUES ('f7b1d97edd98407aab36a01d290683ee', 'gatewayRouteManager:btn_add', 'button', '新建路由', '/auth/gatewayRoute', '68ee45b69c0f41cb9d6a5c33d46fb7a6', null, null, 'POST', null, '2018-02-25 13:43:16', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24');

-- ----------------------------
-- Table structure for base_group
-- ----------------------------
DROP TABLE IF EXISTS `base_group`;
CREATE TABLE `base_group` (
  `id` varchar(36) NOT NULL,
  `code` varchar(255) DEFAULT NULL COMMENT '角色编码',
  `name` varchar(255) DEFAULT NULL COMMENT '角色名称',
  `parent_id` varchar(36) NOT NULL COMMENT '上级节点',
  `path` varchar(2000) DEFAULT NULL COMMENT '树状关系',
  `type` char(1) DEFAULT NULL COMMENT '类型',
  `group_type` varchar(36) NOT NULL COMMENT '角色组类型',
  `description` varchar(255) DEFAULT NULL,
  `crt_time` datetime DEFAULT NULL,
  `crt_user_id` varchar(255) DEFAULT NULL,
  `crt_user_name` varchar(255) DEFAULT NULL,
  `upd_time` datetime DEFAULT NULL,
  `upd_user_id` varchar(255) DEFAULT NULL,
  `upd_user_name` varchar(255) DEFAULT NULL,
  `attr1` varchar(255) DEFAULT NULL,
  `attr2` varchar(255) DEFAULT NULL,
  `attr3` varchar(255) DEFAULT NULL,
  `attr4` varchar(255) DEFAULT NULL,
  `attr5` varchar(255) DEFAULT NULL,
  `attr6` varchar(255) DEFAULT NULL,
  `attr7` varchar(255) DEFAULT NULL,
  `attr8` varchar(255) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL COMMENT '租户Id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of base_group
-- ----------------------------
INSERT INTO `base_group` VALUES ('258651cfd4ca41cbaa70d1b233659b5c', 'commonAdmin', '普通租户管理员', '-1', '/commonAdmin', null, 'role', '普通租户管理员1', '2018-02-12 23:20:54', '1', 'Mr.AG', '2020-01-14 16:07:10', '1', 'admin', null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `base_group` VALUES ('366af0a7fd79453fbe54c0d24917ec4b', 'dongshiz', '董事长', '-1', '/dongshiz', null, 'org', null, '2018-02-13 00:04:27', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', '2018-02-13 00:04:27', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', null, null, null, null, null, null, null, null, '1ec08564dcc344018d6aaa910068f0f0');
INSERT INTO `base_group` VALUES ('ee7ae8f3da9f4789ab19550eecfbc9b2', '0001', '测试岗位组1', '-1', '/0001', null, 'org', null, '2018-08-22 10:38:17', '1', 'Mr.AG', '2018-08-22 10:38:17', '1', 'Mr.AG', null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24');

-- ----------------------------
-- Table structure for base_group_leader
-- ----------------------------
DROP TABLE IF EXISTS `base_group_leader`;
CREATE TABLE `base_group_leader` (
  `id` varchar(36) NOT NULL,
  `group_id` varchar(255) DEFAULT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `crt_time` datetime DEFAULT NULL,
  `crt_user` varchar(255) DEFAULT NULL,
  `crt_name` varchar(255) DEFAULT NULL,
  `crt_host` varchar(255) DEFAULT NULL,
  `upd_time` datetime DEFAULT NULL,
  `upd_user` varchar(255) DEFAULT NULL,
  `upd_name` varchar(255) DEFAULT NULL,
  `upd_host` varchar(255) DEFAULT NULL,
  `attr1` varchar(255) DEFAULT NULL,
  `attr2` varchar(255) DEFAULT NULL,
  `attr3` varchar(255) DEFAULT NULL,
  `attr4` varchar(255) DEFAULT NULL,
  `attr5` varchar(255) DEFAULT NULL,
  `attr6` varchar(255) DEFAULT NULL,
  `attr7` varchar(255) DEFAULT NULL,
  `attr8` varchar(255) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL COMMENT '租户Id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of base_group_leader
-- ----------------------------
INSERT INTO `base_group_leader` VALUES ('a92d887fa6814c0e865e3a7071bbdeab', '258651cfd4ca41cbaa70d1b233659b5c', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24');

-- ----------------------------
-- Table structure for base_group_member
-- ----------------------------
DROP TABLE IF EXISTS `base_group_member`;
CREATE TABLE `base_group_member` (
  `id` varchar(36) NOT NULL,
  `group_id` varchar(255) DEFAULT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `crt_time` datetime DEFAULT NULL,
  `crt_user` varchar(255) DEFAULT NULL,
  `crt_name` varchar(255) DEFAULT NULL,
  `crt_host` varchar(255) DEFAULT NULL,
  `upd_time` datetime DEFAULT NULL,
  `upd_user` varchar(255) DEFAULT NULL,
  `upd_name` varchar(255) DEFAULT NULL,
  `upd_host` varchar(255) DEFAULT NULL,
  `attr1` varchar(255) DEFAULT NULL,
  `attr2` varchar(255) DEFAULT NULL,
  `attr3` varchar(255) DEFAULT NULL,
  `attr4` varchar(255) DEFAULT NULL,
  `attr5` varchar(255) DEFAULT NULL,
  `attr6` varchar(255) DEFAULT NULL,
  `attr7` varchar(255) DEFAULT NULL,
  `attr8` varchar(255) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL COMMENT '租户Id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of base_group_member
-- ----------------------------

-- ----------------------------
-- Table structure for base_group_type
-- ----------------------------
DROP TABLE IF EXISTS `base_group_type`;
CREATE TABLE `base_group_type` (
  `id` varchar(32) NOT NULL,
  `code` varchar(255) DEFAULT NULL COMMENT '编码',
  `name` varchar(255) DEFAULT NULL COMMENT '类型名称',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `crt_time` datetime DEFAULT NULL COMMENT '创建时间',
  `crt_user` varchar(255) DEFAULT NULL COMMENT '创建人ID',
  `crt_name` varchar(255) DEFAULT NULL COMMENT '创建人',
  `crt_host` varchar(255) DEFAULT NULL COMMENT '创建主机',
  `upd_time` datetime DEFAULT NULL COMMENT '最后更新时间',
  `upd_user` varchar(255) DEFAULT NULL COMMENT '最后更新人ID',
  `upd_name` varchar(255) DEFAULT NULL COMMENT '最后更新人',
  `upd_host` varchar(255) DEFAULT NULL COMMENT '最后更新主机',
  `attr1` varchar(255) DEFAULT NULL,
  `attr2` varchar(255) DEFAULT NULL,
  `attr3` varchar(255) DEFAULT NULL,
  `attr4` varchar(255) DEFAULT NULL,
  `attr5` varchar(255) DEFAULT NULL,
  `attr6` varchar(255) DEFAULT NULL,
  `attr7` varchar(255) DEFAULT NULL,
  `attr8` varchar(255) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL COMMENT '租户Id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of base_group_type
-- ----------------------------
INSERT INTO `base_group_type` VALUES ('free', 'free', '自定义类型', 'sad', null, null, null, null, '2018-01-22 12:59:12', '1', 'Mr.AG', '127.0.0.1', null, null, null, null, null, null, null, null, null);
INSERT INTO `base_group_type` VALUES ('org', 'org', '岗位组（不可删）', null, null, null, null, null, '2017-08-25 17:52:43', '1', 'Mr.AG', '127.0.0.1', null, null, null, null, null, null, null, null, null);
INSERT INTO `base_group_type` VALUES ('role', 'role', '角色组（不可删）', 'role', null, null, null, null, '2017-08-25 17:52:37', '1', 'Mr.AG', '127.0.0.1', null, null, null, null, null, null, null, null, null);

-- ----------------------------
-- Table structure for base_menu
-- ----------------------------
DROP TABLE IF EXISTS `base_menu`;
CREATE TABLE `base_menu` (
  `id` varchar(36) NOT NULL,
  `code` varchar(255) DEFAULT NULL COMMENT '路径编码',
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `parent_id` varchar(36) NOT NULL COMMENT '父级节点',
  `href` varchar(255) DEFAULT NULL COMMENT '资源路径',
  `icon` varchar(255) DEFAULT NULL COMMENT '图标',
  `type` char(10) DEFAULT NULL,
  `order_num` int(11) NOT NULL DEFAULT '0' COMMENT '排序',
  `description` varchar(255) DEFAULT NULL COMMENT '描述',
  `path` varchar(500) DEFAULT NULL COMMENT '菜单上下级关系',
  `enabled` char(1) DEFAULT NULL COMMENT '启用禁用',
  `crt_time` datetime DEFAULT NULL,
  `crt_user` varchar(255) DEFAULT NULL,
  `crt_name` varchar(255) DEFAULT NULL,
  `crt_host` varchar(255) DEFAULT NULL,
  `upd_time` datetime DEFAULT NULL,
  `upd_user` varchar(255) DEFAULT NULL,
  `upd_name` varchar(255) DEFAULT NULL,
  `upd_host` varchar(255) DEFAULT NULL,
  `attr1` varchar(255) DEFAULT NULL,
  `attr2` varchar(255) DEFAULT NULL,
  `attr3` varchar(255) DEFAULT NULL,
  `attr4` varchar(255) DEFAULT NULL,
  `attr5` varchar(255) DEFAULT NULL,
  `attr6` varchar(255) DEFAULT NULL,
  `attr7` varchar(255) DEFAULT NULL,
  `attr8` varchar(255) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL COMMENT '租户Id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of base_menu
-- ----------------------------
INSERT INTO `base_menu` VALUES ('1', 'userManager', '用户管理', '5', '/admin/user', 'user', 'menu', '1', '', '/adminSys/baseManager/userManager', null, null, null, null, null, '2018-01-23 15:35:50', '1', 'Mr.AG', '127.0.0.1', '_import(\'admin/user/index\')', null, null, null, null, null, null, null, null);
INSERT INTO `base_menu` VALUES ('13', 'adminSys', '权限管理系统', '-1', '/base', 'setting', 'dirt', '0', '', '/adminSys', null, null, null, null, null, '2018-02-02 19:48:22', '1', 'Mr.AG', '127.0.0.1', 'Layout', null, null, null, null, null, null, null, null);
INSERT INTO `base_menu` VALUES ('1e97528855db446cb6d4ddbf4772c8fa', 'projectClass', '类别管理', 'be7bc967f5e548c09cbf8d1a5c8d1f11', null, null, 'menu', '0', null, '/projectManager/projectClass', null, '2019-07-03 09:55:38', '1', 'Mr.AG', null, '2019-07-03 09:55:38', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `base_menu` VALUES ('1ee768f0c5c44c8bbdf114d6b508d253', 'articleProjectList', '文章项目列表', 'be7bc967f5e548c09cbf8d1a5c8d1f11', null, null, 'menu', '0', null, '/projectManager/articleProjectList', null, '2019-08-02 10:05:32', '1', 'Mr.AG', null, '2019-08-02 10:05:55', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `base_menu` VALUES ('21', 'dictManager', '数据字典', '5', '', 'documentation', 'menu', '5', '', '/adminSys/baseManager/dictManager', null, null, null, null, null, '2018-02-04 14:48:47', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_menu` VALUES ('27', 'gateLogManager', '操作日志', '5', '/admin/gateLog', 'log', 'menu', '6', '', '/adminSys/baseManager/gateLogManager', null, '2017-07-01 00:00:00', '1', 'admin', '0:0:0:0:0:0:0:1', '2017-09-05 22:32:55', '1', 'Mr.AG', '127.0.0.1', '_import(\'admin/gateLog/index\')', null, null, null, null, null, null, null, null);
INSERT INTO `base_menu` VALUES ('29', 'authManager', '服务管理', '13', '/auth', 'service', 'dirt', '3', '服务权限管理', '/adminSys/authManager', null, '2017-12-26 19:54:45', '1', 'Mr.AG', '127.0.0.1', '2018-02-02 19:48:42', '1', 'Mr.AG', '127.0.0.1', null, null, null, null, null, null, null, null, null);
INSERT INTO `base_menu` VALUES ('30', 'serviceManager', '服务权限管理', '29', '/auth/service', 'client', 'menu', '1', '服务管理', '/adminSys/authManager/serviceManager', null, '2017-12-26 19:56:06', '1', 'Mr.AG', '127.0.0.1', '2018-02-02 19:48:46', '1', 'Mr.AG', '127.0.0.1', null, null, null, null, null, null, null, null, null);
INSERT INTO `base_menu` VALUES ('31', 'monitorManager', '监控运维管理', '13', null, 'setting', 'dirt', '3', null, '/adminSys/monitorManager', null, '2018-01-07 11:35:19', '1', 'Mr.AG', '127.0.0.1', '2018-02-04 22:33:41', '1', 'Mr.AG', '127.0.0.1', null, null, null, null, null, null, null, null, null);
INSERT INTO `base_menu` VALUES ('33', 'serviceMonitorManager', '服务监控管理', '31', null, 'client', 'menu', '2', null, '/adminSys/monitorManager/serviceMonitorManager', null, '2018-01-07 11:38:13', '1', 'Mr.AG', '127.0.0.1', '2018-01-07 11:38:29', '1', 'Mr.AG', '127.0.0.1', null, null, null, null, null, null, null, null, null);
INSERT INTO `base_menu` VALUES ('34', 'serviceEurekaManager', '服务注册管理', '31', null, 'client', 'menu', '1', null, '/adminSys/monitorManager/serviceEurekaManager', null, '2018-01-07 12:20:31', '1', 'Mr.AG', '127.0.0.1', '2018-01-07 12:20:31', '1', 'Mr.AG', '127.0.0.1', null, null, null, null, null, null, null, null, null);
INSERT INTO `base_menu` VALUES ('342a587a51d647a1b53c6d97363cf428', 'appUserManager', '会员账户模块', '3a61b3472fc54332a38464fb03d309ec', null, 'user', 'menu', '0', null, '/appManager/appUserManager', null, '2018-05-26 09:53:44', '1', 'Mr.AG', null, '2018-05-26 09:57:16', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `base_menu` VALUES ('35', 'serviceZipkinManager', '服务链路监控', '31', null, 'client', 'menu', '3', null, '/adminSys/monitorManager/serviceZipkinManager', null, '2018-01-07 17:58:14', '1', 'Mr.AG', '127.0.0.1', '2018-01-07 17:58:14', '1', 'Mr.AG', '127.0.0.1', null, null, null, null, null, null, null, null, null);
INSERT INTO `base_menu` VALUES ('3a61b3472fc54332a38464fb03d309ec', 'appManager', '会员管理模块', '-1', null, 'app', 'dirt', '1', null, '/appManager', null, '2018-05-26 09:53:20', '1', 'Mr.AG', null, '2018-08-29 17:01:05', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `base_menu` VALUES ('40', 'jobManager', '定时作业管理', '5', null, 'job', 'menu', '8', null, '/adminSys/baseManager/jobManager', null, '2018-03-09 19:56:55', '1', 'XDiamond', null, '2018-03-09 19:56:59', '1', 'XDiamond', '127.0.0.1', null, null, null, null, null, null, null, null, null);
INSERT INTO `base_menu` VALUES ('40c190d6188f4e9993031437e05179f8', 'delectUserManager', '已删用户管理', 'c25b4bbbc80d4ed7a7855424f33676e4', null, null, 'menu', '4', null, '/frontUserManager/delectUserManager', null, '2019-07-15 13:52:18', '1', 'Mr.AG', null, '2019-07-15 13:52:18', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `base_menu` VALUES ('4b44862472284dcfb5209b543bfe8ee1', 'videoProjectList', '视频项目列表', 'be7bc967f5e548c09cbf8d1a5c8d1f11', null, null, 'menu', '0', null, '/projectManager/videoProjectList', null, '2019-08-02 10:05:09', '1', 'Mr.AG', null, '2019-08-02 10:05:50', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `base_menu` VALUES ('5', 'baseManager', '基础配置管理', '13', '/admin', 'setting', 'dirt', '0', '', '/adminSys/baseManager', null, null, null, null, null, '2018-02-02 19:48:27', '1', 'Mr.AG', '127.0.0.1', 'Layout', null, null, null, null, null, null, null, null);
INSERT INTO `base_menu` VALUES ('52f709db3cd84d70886e500ab9dbed6e', 'adoptUserList', '已注册用户', 'c25b4bbbc80d4ed7a7855424f33676e4', null, null, 'menu', '2', null, '/frontUserManager/adoptUserList', null, '2019-07-04 11:35:31', '1', 'Mr.AG', null, '2019-07-12 09:57:17', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `base_menu` VALUES ('5447b09ca7b447ffa567494f4a21b3f3', '11', '11', '7a9e874d3cfa4799bad101979b88cb36', null, null, 'menu', '0', null, '/adminSys/2/11', null, '2018-08-22 16:34:26', '1', 'Mr.AG', null, '2018-08-22 16:34:26', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `base_menu` VALUES ('6', 'menuManager', '菜单管理', '5', '/admin/menu', 'category', 'menu', '2', '', '/adminSys/baseManager/menuManager', null, null, null, null, null, '2017-09-05 21:10:25', '1', 'Mr.AG', '127.0.0.1', '_import(\'admin/menu/index\')', null, null, null, null, null, null, null, null);
INSERT INTO `base_menu` VALUES ('61f2cbdd2b424bd0b619dc871a44f210', '22', '22', '7a9e874d3cfa4799bad101979b88cb36', null, null, 'dirt', '0', null, '/adminSys/2/22', null, '2018-08-22 16:34:36', '1', 'Mr.AG', null, '2018-08-22 16:34:36', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `base_menu` VALUES ('68ee45b69c0f41cb9d6a5c33d46fb7a6', 'gatewayManager', '网关路由管理', '29', null, 'navigate', 'menu', '3', null, '/adminSys/authManager/gatewayManager', null, '2018-02-25 13:30:49', '1', 'Mr.AG', null, '2018-02-25 13:30:49', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `base_menu` VALUES ('68ee45b69c0f41cb9d6a5c33d46fb8a6', 'oauthClientDetailsManager', '客户端管理', '29', null, 'navigate', 'menu', '4', null, '/adminSys/authManager/oauthClientDetailsManager', null, '2018-02-25 13:30:49', '1', 'Mr.AG', null, '2018-03-28 20:17:57', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `base_menu` VALUES ('7', 'groupManager', '角色权限管理', '5', '/admin/group', 'group_fill', 'menu', '3', '', '/adminSys/baseManager/groupManager', null, null, null, null, null, '2017-09-05 21:11:34', '1', 'Mr.AG', '127.0.0.1', 'import(\'admin/group/index\')', null, null, null, null, null, null, null, null);
INSERT INTO `base_menu` VALUES ('7494902cc9e948668e51f4596042b084', 'apiManager', '服务接口文档', '29', '/auth/api', 'documentation', 'menu', '2', null, '/adminSys/authManager/apiManager', null, '2018-02-07 13:28:05', '1', 'Mr.AG', null, '2018-02-07 13:30:42', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_menu` VALUES ('7574b969c9fa4e5895d6cc9c2b8a9a62', 'tenantManager', '租户管理', '5', '/admin/tenantManager', 'tenant', 'menu', '7', null, '/adminSys/baseManager/tenantManager', null, '2018-02-09 08:56:43', '1', 'Mr.AG', null, '2018-02-12 13:23:54', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_menu` VALUES ('7a0a75752c7d422abd2e30b7aad744d7', 'orgManager', '组织部门管理', '13', '/org', 'org', 'dirt', '2', null, '/adminSys/orgManager', null, '2018-02-04 22:33:33', '1', 'Mr.AG', null, '2018-02-04 22:35:57', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_menu` VALUES ('8', 'groupTypeManager', '角色类型管理', '5', '/admin/groupType', 'group', 'menu', '4', '', '/adminSys/baseManager/groupTypeManager', null, null, null, null, null, '2017-09-05 21:12:28', '1', 'Mr.AG', '127.0.0.1', '_import(\'admin/groupType/index\')', null, null, null, null, null, null, null, null);
INSERT INTO `base_menu` VALUES ('a78ddff416394236ae1ee22fc3ac442b', 'urlProjectList', '外链项目列表', 'be7bc967f5e548c09cbf8d1a5c8d1f11', null, null, 'menu', '1', null, '/projectManager/projectList', null, '2019-07-03 09:57:52', '1', 'Mr.AG', null, '2019-08-02 09:56:15', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `base_menu` VALUES ('b5211cc69d234b28a97f27e63edc9a58', 'departManager', '部门管理', '7a0a75752c7d422abd2e30b7aad744d7', '/org/depart', 'depart', 'menu', '1', null, '/adminSys/orgManager/departManager', null, '2018-02-04 22:40:01', '1', 'Mr.AG', null, '2018-02-04 22:40:01', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, null);
INSERT INTO `base_menu` VALUES ('be7bc967f5e548c09cbf8d1a5c8d1f11', 'projectManager', '项目管理', '-1', null, null, 'dirt', '3', null, '/projectManager', null, '2019-07-03 09:54:49', '1', 'Mr.AG', null, '2019-07-03 09:54:49', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `base_menu` VALUES ('c1c1a265cc994a8aa2cbe67f6933e15e', 'authUserList', '审核信息列表', 'c25b4bbbc80d4ed7a7855424f33676e4', null, null, 'menu', '1', null, '/frontUserManager/authUserList', null, '2019-07-04 11:34:32', '1', 'Mr.AG', null, '2019-07-12 09:57:11', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `base_menu` VALUES ('c25b4bbbc80d4ed7a7855424f33676e4', 'frontUserManager', '用户管理', '-1', null, null, 'dirt', '0', null, '/frontUserManager', null, '2019-07-04 09:46:34', '1', 'Mr.AG', null, '2019-07-04 09:46:34', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `base_menu` VALUES ('db5e9dc6f5f54d2d9ddfc47b4b5956f5', '222', '2222', 'bd99e1efe84d4784a3d6df2b6207bd14', null, null, 'menu', '0', null, '/appManager/11111/222', null, '2018-08-29 17:03:08', '1', 'Mr.AG', null, '2018-08-29 17:03:08', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `base_menu` VALUES ('e969fa0206e8439cba63406a4228d8d5', 'userProjectStatistics', '用户项目统计数据', 'c25b4bbbc80d4ed7a7855424f33676e4', null, null, 'menu', '3', null, '/frontUserManager/userProjectStatistics', null, '2019-07-04 11:36:28', '1', 'Mr.AG', null, '2019-07-12 09:57:23', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `base_menu` VALUES ('f62c8311c0c64319aee2723f1b8e80b9', 'userType', '用户类别', 'c25b4bbbc80d4ed7a7855424f33676e4', null, null, 'menu', '0', null, '/frontUserManager/userType', null, '2019-07-12 09:55:39', '1', 'Mr.AG', null, '2019-07-12 09:57:29', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24');

-- ----------------------------
-- Table structure for base_position
-- ----------------------------
DROP TABLE IF EXISTS `base_position`;
CREATE TABLE `base_position` (
  `id` varchar(36) NOT NULL,
  `name` varchar(255) DEFAULT NULL COMMENT '职位',
  `code` varchar(100) DEFAULT NULL COMMENT '编码',
  `depart_id` varchar(36) DEFAULT NULL COMMENT '部门ID',
  `type` varchar(36) DEFAULT NULL COMMENT '类型',
  `crt_user_name` varchar(255) DEFAULT NULL COMMENT '创建人',
  `crt_user_id` varchar(36) DEFAULT NULL COMMENT '创建人ID',
  `crt_time` datetime DEFAULT NULL COMMENT '创建时间',
  `upd_user_name` varchar(255) DEFAULT NULL COMMENT '最后更新人',
  `upd_user_id` varchar(36) DEFAULT NULL COMMENT '最后更新人ID',
  `upd_time` datetime DEFAULT NULL COMMENT '最后更新时间',
  `attr1` varchar(255) DEFAULT NULL,
  `attr2` varchar(255) DEFAULT NULL,
  `attr3` varchar(255) DEFAULT NULL,
  `attr4` varchar(255) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_code` (`code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of base_position
-- ----------------------------
INSERT INTO `base_position` VALUES ('0087e16c97b04ac085cc186042064ddb', '测试岗', '001', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'role', 'Mr.AG', '1', '2018-08-22 10:34:53', 'Mr.AG', '1', '2018-08-22 10:34:53', null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `base_position` VALUES ('9c2f9ab789634c2ca11456a1f28b3545', '部门经理', '12', 'root', 'flow', 'Mr.AG', '1', '2018-02-15 10:57:28', 'Mr.AG', '1', '2018-04-04 19:15:18', null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `base_position` VALUES ('c5f51dcdb64f4706acb4caa867359721', '董事长', 'dongshiz', 'd8bb2fc8d51745458f77a78c42bf60ab', null, '测试租户', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '2018-02-13 00:14:19', '测试租户', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '2018-02-13 00:14:19', null, null, null, null, '1ec08564dcc344018d6aaa910068f0f0');

-- ----------------------------
-- Table structure for base_position_depart
-- ----------------------------
DROP TABLE IF EXISTS `base_position_depart`;
CREATE TABLE `base_position_depart` (
  `id` varchar(36) NOT NULL,
  `position_id` varchar(36) DEFAULT NULL,
  `depart_id` varchar(36) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of base_position_depart
-- ----------------------------
INSERT INTO `base_position_depart` VALUES ('06fcf4a245574548b26483a7935d6b80', 'c5f51dcdb64f4706acb4caa867359721', '2b06ea24a3cf40659c183da3242cc669', '1ec08564dcc344018d6aaa910068f0f0');
INSERT INTO `base_position_depart` VALUES ('52e4769712644574b510375d2cd67193', '9c2f9ab789634c2ca11456a1f28b3545', 'root', 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `base_position_depart` VALUES ('a3b52b00c9db421dac71a0cf8515292f', 'c5f51dcdb64f4706acb4caa867359721', '8782fcb72842440fa3254bf72717d7b0', '1ec08564dcc344018d6aaa910068f0f0');
INSERT INTO `base_position_depart` VALUES ('a56fa87a91024418bcb9c455eb6dbe4a', 'c5f51dcdb64f4706acb4caa867359721', 'a269b706b0b34ee89dd7e8e08e8514bc', '1ec08564dcc344018d6aaa910068f0f0');
INSERT INTO `base_position_depart` VALUES ('e9e7ae7b39ec4b1095005a1d58a96db0', 'c5f51dcdb64f4706acb4caa867359721', '6cc8e1c706ec4119afbf12bf1da2b9ad', '1ec08564dcc344018d6aaa910068f0f0');
INSERT INTO `base_position_depart` VALUES ('f7a5501555794e3fbc1e423e4182f542', '0087e16c97b04ac085cc186042064ddb', 'd583e7de6d2d48b78fb3c7dcb180cb1f', 'ac88ceb386aa4231b09bf472cb937c24');

-- ----------------------------
-- Table structure for base_position_group
-- ----------------------------
DROP TABLE IF EXISTS `base_position_group`;
CREATE TABLE `base_position_group` (
  `id` varchar(36) NOT NULL,
  `position_id` varchar(36) DEFAULT NULL,
  `group_id` varchar(36) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of base_position_group
-- ----------------------------
INSERT INTO `base_position_group` VALUES ('4eb85c39babc44b5ab3c0dcc569068b2', 'ba87c899a1404f43bb3caa3c072a88cc', '366af0a7fd79453fbe54c0d24917ec4b', '1ec08564dcc344018d6aaa910068f0f0');
INSERT INTO `base_position_group` VALUES ('d020f0f465104f5ca32b1a00a3a81836', 'c5f51dcdb64f4706acb4caa867359721', '366af0a7fd79453fbe54c0d24917ec4b', '1ec08564dcc344018d6aaa910068f0f0');
INSERT INTO `base_position_group` VALUES ('d79e9be9a82e434f912b85399a8f223b', '0087e16c97b04ac085cc186042064ddb', 'ee7ae8f3da9f4789ab19550eecfbc9b2', 'ac88ceb386aa4231b09bf472cb937c24');

-- ----------------------------
-- Table structure for base_position_user
-- ----------------------------
DROP TABLE IF EXISTS `base_position_user`;
CREATE TABLE `base_position_user` (
  `id` varchar(36) NOT NULL,
  `position_id` varchar(36) DEFAULT NULL,
  `user_id` varchar(36) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of base_position_user
-- ----------------------------
INSERT INTO `base_position_user` VALUES ('12', '9c2f9ab789634c2ca11456a1f28b3545', '59c28b8593c2472e99e54e3ed9b6efe8', 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `base_position_user` VALUES ('30366d57e3914cdeaff3676e5d3e1d61', 'ba87c899a1404f43bb3caa3c072a88cc', '59c28b8593c2472e99e54e3ed9b6efe8', '1ec08564dcc344018d6aaa910068f0f0');

-- ----------------------------
-- Table structure for base_resource_authority
-- ----------------------------
DROP TABLE IF EXISTS `base_resource_authority`;
CREATE TABLE `base_resource_authority` (
  `id` varchar(36) NOT NULL,
  `authority_id` varchar(255) DEFAULT NULL COMMENT '角色ID',
  `authority_type` varchar(255) DEFAULT NULL COMMENT '角色类型',
  `resource_id` varchar(255) DEFAULT NULL COMMENT '资源ID',
  `resource_type` varchar(255) DEFAULT NULL COMMENT '资源类型',
  `parent_id` varchar(255) DEFAULT NULL,
  `path` varchar(2000) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `crt_time` datetime DEFAULT NULL,
  `crt_user` varchar(255) DEFAULT NULL,
  `crt_name` varchar(255) DEFAULT NULL,
  `crt_host` varchar(255) DEFAULT NULL,
  `attr1` varchar(255) DEFAULT NULL,
  `attr2` varchar(255) DEFAULT NULL,
  `attr3` varchar(255) DEFAULT NULL,
  `attr4` varchar(255) DEFAULT NULL,
  `attr5` varchar(255) DEFAULT NULL,
  `attr6` varchar(255) DEFAULT NULL,
  `attr7` varchar(255) DEFAULT NULL,
  `attr8` varchar(255) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL COMMENT '租户Id',
  `type` varchar(1) DEFAULT NULL COMMENT '类型',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of base_resource_authority
-- ----------------------------
INSERT INTO `base_resource_authority` VALUES ('00c911a324e0498fa8dc8cda500fb2b4', 'b0bfc23bd12f47418044c556405b52ac', 'group', '21', 'button', '-1', null, null, '2018-03-06 11:34:07', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', null, null, null, null, null, null, null, null, null, '1ec08564dcc344018d6aaa910068f0f0', '0');
INSERT INTO `base_resource_authority` VALUES ('01b41eb9bac1480382ed933acfb84d15', '258651cfd4ca41cbaa70d1b233659b5c', 'group', '13', 'menu', '-1', null, null, '2018-03-06 11:56:07', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24', '1');
INSERT INTO `base_resource_authority` VALUES ('066227dadc524d10a91fb0ce24a4cd5d', 'b0bfc23bd12f47418044c556405b52ac', 'group', '19', 'button', '-1', null, null, '2018-03-06 11:34:07', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', null, null, null, null, null, null, null, null, null, '1ec08564dcc344018d6aaa910068f0f0', '0');
INSERT INTO `base_resource_authority` VALUES ('0757704647af4651ab9e19daa52f5cb7', '366af0a7fd79453fbe54c0d24917ec4b', 'group', '1', 'menu', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '0');
INSERT INTO `base_resource_authority` VALUES ('0b65c6e846d44793a7a411b6d6c2e8d5', '258651cfd4ca41cbaa70d1b233659b5c', 'group', '5', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '1');
INSERT INTO `base_resource_authority` VALUES ('0c9ed5b75eff4e0da05ca80e47d88ad3', '258651cfd4ca41cbaa70d1b233659b5c', 'group', '7a16202657c3478c8b4ece27a792e026', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '0');
INSERT INTO `base_resource_authority` VALUES ('0d9c8af07720466fbe40ed96eb4d9247', '258651cfd4ca41cbaa70d1b233659b5c', 'group', '21', 'menu', '-1', null, null, '2019-07-03 10:05:32', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24', '0');
INSERT INTO `base_resource_authority` VALUES ('0e35f3f47d07402daa82c13e4bd1eeea', '258651cfd4ca41cbaa70d1b233659b5c', 'group', '57', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '1');
INSERT INTO `base_resource_authority` VALUES ('0fa164ab87c540ca92e81f7bc3f1ca56', '258651cfd4ca41cbaa70d1b233659b5c', 'group', '28', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '0');
INSERT INTO `base_resource_authority` VALUES ('13315a1c9ad54e9784fbf1360f3ea362', '258651cfd4ca41cbaa70d1b233659b5c', 'group', '4ee4f57a2d92494abe2595bff4f32057', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '1');
INSERT INTO `base_resource_authority` VALUES ('13de9dce30a44f7c8479577506def590', '258651cfd4ca41cbaa70d1b233659b5c', 'group', '4ee4f57a2d92494abe2595bff4f32057', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '0');
INSERT INTO `base_resource_authority` VALUES ('151ca9a05f764174adfd257367b2c03a', '258651cfd4ca41cbaa70d1b233659b5c', 'group', '-1', 'menu', '-1', null, null, '2018-03-06 11:56:07', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24', '1');
INSERT INTO `base_resource_authority` VALUES ('1587b15affaa4c89a7310769ccad7aef', '258651cfd4ca41cbaa70d1b233659b5c', 'group', '21b3fe683a6040ed8c57423e55cd94a2', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '0');
INSERT INTO `base_resource_authority` VALUES ('1b31971f38e545629c656d64d9d2d0cd', '258651cfd4ca41cbaa70d1b233659b5c', 'group', '56', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '0');
INSERT INTO `base_resource_authority` VALUES ('1bc5bdb1d6b14237b44afb206e575bcc', '-1', 'group', '-1', 'menu', '-1', null, null, '2018-03-06 09:57:13', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', null, null, null, null, null, null, null, null, null, '1ec08564dcc344018d6aaa910068f0f0', '1');
INSERT INTO `base_resource_authority` VALUES ('1c0d5e7cda03471299091f6d967a91e5', '258651cfd4ca41cbaa70d1b233659b5c', 'group', '5', 'menu', '-1', null, null, '2019-07-03 10:05:32', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24', '0');
INSERT INTO `base_resource_authority` VALUES ('1c5d719bbdbf47baa6df465bf44aa3ea', 'b0bfc23bd12f47418044c556405b52ac', 'group', '4ee4f57a2d92494abe2595bff4f32057', 'button', '-1', null, null, '2018-03-06 11:34:07', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', null, null, null, null, null, null, null, null, null, '1ec08564dcc344018d6aaa910068f0f0', '0');
INSERT INTO `base_resource_authority` VALUES ('1f0a36b8b5d648159dbfbf6f3db1ab91', '258651cfd4ca41cbaa70d1b233659b5c', 'group', '32', 'button', '-1', null, null, '2018-08-22 10:26:53', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24', '0');
INSERT INTO `base_resource_authority` VALUES ('1f7e43d004004e5493b6c5e66c517536', '258651cfd4ca41cbaa70d1b233659b5c', 'group', 'cac49646909c4750b23f223f53eafc5b', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '1');
INSERT INTO `base_resource_authority` VALUES ('204313d648d1497ba1b0049b14269ea5', '258651cfd4ca41cbaa70d1b233659b5c', 'group', '1e97528855db446cb6d4ddbf4772c8fa', 'menu', '-1', null, null, '2019-07-03 10:05:32', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24', '0');
INSERT INTO `base_resource_authority` VALUES ('204ec668b05444d197168ebaccbadb77', '-1', 'group', '13', 'menu', '-1', null, null, '2019-07-03 10:04:43', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24', '0');
INSERT INTO `base_resource_authority` VALUES ('20643a004e8c4751a980649485989499', '258651cfd4ca41cbaa70d1b233659b5c', 'group', '21b3fe683a6040ed8c57423e55cd94a2', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '1');
INSERT INTO `base_resource_authority` VALUES ('21195979aabd4fd2b174845eef8f3588', '258651cfd4ca41cbaa70d1b233659b5c', 'group', '-1', 'menu', '-1', null, null, '2019-07-03 10:05:32', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24', '0');
INSERT INTO `base_resource_authority` VALUES ('21341cc7429446bcb7a4eb39a16fb05f', 'b0bfc23bd12f47418044c556405b52ac', 'group', '1', 'menu', '-1', null, null, '2018-03-06 08:31:38', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', null, null, null, null, null, null, null, null, null, '1ec08564dcc344018d6aaa910068f0f0', '0');
INSERT INTO `base_resource_authority` VALUES ('21471cad403a4bb883864b67912893b3', '258651cfd4ca41cbaa70d1b233659b5c', 'group', '6d7ea83bb71d476dbfbda2bcbc1a01bd', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '0');
INSERT INTO `base_resource_authority` VALUES ('25c45a4e2b48442491405a92d5660c8e', '258651cfd4ca41cbaa70d1b233659b5c', 'group', '8b5ff030c76341059a335e0653fef5ea', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '0');
INSERT INTO `base_resource_authority` VALUES ('25c85684ec83485db4fc2db8300e5879', '258651cfd4ca41cbaa70d1b233659b5c', 'group', '103593127199487794f9fd8fd573db5f', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '0');
INSERT INTO `base_resource_authority` VALUES ('29c127f85ceb49f89124240eb034760a', '258651cfd4ca41cbaa70d1b233659b5c', 'group', 'c6469be83d8e4d5ca404b46b347f7d4c', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '0');
INSERT INTO `base_resource_authority` VALUES ('2a0da4a9320a485bbe47bfb9fe3b0686', '258651cfd4ca41cbaa70d1b233659b5c', 'group', 'b5211cc69d234b28a97f27e63edc9a58', 'menu', '-1', null, null, '2019-07-03 10:05:32', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24', '0');
INSERT INTO `base_resource_authority` VALUES ('2a39bd04c0fd4b7292b8558e10eb25a0', 'b0bfc23bd12f47418044c556405b52ac', 'group', '18', 'button', '-1', null, null, '2018-03-06 11:34:07', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', null, null, null, null, null, null, null, null, null, '1ec08564dcc344018d6aaa910068f0f0', '0');
INSERT INTO `base_resource_authority` VALUES ('2aab60c9003742019582cd8f9376eb3e', 'a38aa1d3913742ec86370a17b2ed0507', 'group', '3', 'button', '-1', null, null, '2018-03-06 11:31:10', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', null, null, null, null, null, null, null, null, null, '1ec08564dcc344018d6aaa910068f0f0', '0');
INSERT INTO `base_resource_authority` VALUES ('2b35de5bea0e4bb6b1f2832e755cc7d3', '-1', 'group', '1', 'menu', '-1', null, null, '2019-07-03 10:04:43', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24', '0');
INSERT INTO `base_resource_authority` VALUES ('2f643a3c464146d2b4796f405f87a049', '366af0a7fd79453fbe54c0d24917ec4b', 'group', 'b5211cc69d234b28a97f27e63edc9a58', 'menu', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '0');
INSERT INTO `base_resource_authority` VALUES ('2ff2081f499a4129a17feaace47753cf', '258651cfd4ca41cbaa70d1b233659b5c', 'group', 'be7bc967f5e548c09cbf8d1a5c8d1f11', 'menu', '-1', null, null, '2019-07-03 10:05:32', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24', '0');
INSERT INTO `base_resource_authority` VALUES ('31263c0e45d843bbb2d3c27136c4920c', '258651cfd4ca41cbaa70d1b233659b5c', 'group', '22', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '1');
INSERT INTO `base_resource_authority` VALUES ('32fd0f08ed77450c89e161cd01c5dcfb', '258651cfd4ca41cbaa70d1b233659b5c', 'group', '48', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '0');
INSERT INTO `base_resource_authority` VALUES ('3421b7806bf049309762b3e5ef30d52c', '258651cfd4ca41cbaa70d1b233659b5c', 'group', '19', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '1');
INSERT INTO `base_resource_authority` VALUES ('349ae50eded647a0be4073e3a1c3248b', 'b0bfc23bd12f47418044c556405b52ac', 'group', '13', 'menu', '-1', null, null, '2018-03-06 08:31:38', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', null, null, null, null, null, null, null, null, null, '1ec08564dcc344018d6aaa910068f0f0', '0');
INSERT INTO `base_resource_authority` VALUES ('352e20b29a304ddc955f2d1ecba7b9b5', '258651cfd4ca41cbaa70d1b233659b5c', 'group', '7574b969c9fa4e5895d6cc9c2b8a9a62', 'menu', '-1', null, null, '2019-07-03 10:05:32', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24', '0');
INSERT INTO `base_resource_authority` VALUES ('35d6b9542e2e463b9fd156658dc02913', '258651cfd4ca41cbaa70d1b233659b5c', 'group', '7a0a75752c7d422abd2e30b7aad744d7', 'menu', '-1', null, null, '2019-07-03 10:05:32', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24', '0');
INSERT INTO `base_resource_authority` VALUES ('35e79f43a44a430dba65fd0a0cfa6691', '258651cfd4ca41cbaa70d1b233659b5c', 'group', '41', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '0');
INSERT INTO `base_resource_authority` VALUES ('360d787e14ee48f9ad41d2c343b7a9d1', '258651cfd4ca41cbaa70d1b233659b5c', 'group', 'a78ddff416394236ae1ee22fc3ac442b', 'menu', '-1', null, null, '2019-07-03 10:05:32', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24', '0');
INSERT INTO `base_resource_authority` VALUES ('3ad89a3bf7244c6f915eb0039eaf5fdb', 'b0bfc23bd12f47418044c556405b52ac', 'group', '28', 'button', '-1', null, null, '2018-03-06 11:34:07', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', null, null, null, null, null, null, null, null, null, '1ec08564dcc344018d6aaa910068f0f0', '0');
INSERT INTO `base_resource_authority` VALUES ('3e7dfb6d37d846d697bdd3ec1a32eb94', '258651cfd4ca41cbaa70d1b233659b5c', 'group', '56', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '1');
INSERT INTO `base_resource_authority` VALUES ('3f306f8e4f6c407c83216883dc59b7f2', '258651cfd4ca41cbaa70d1b233659b5c', 'group', '103593127199487794f9fd8fd573db5f', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '1');
INSERT INTO `base_resource_authority` VALUES ('469e8c81b2704adc8c4b0819eee892d3', '258651cfd4ca41cbaa70d1b233659b5c', 'group', '17', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '0');
INSERT INTO `base_resource_authority` VALUES ('46c5bff802a04e6e91bdb7f2e818aca5', '-1', 'group', '1e97528855db446cb6d4ddbf4772c8fa', 'menu', '-1', null, null, '2019-07-03 10:04:43', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24', '0');
INSERT INTO `base_resource_authority` VALUES ('486d95c66505416898fea812f956400f', 'b0bfc23bd12f47418044c556405b52ac', 'group', '7', 'menu', '-1', null, null, '2018-03-06 08:31:38', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', null, null, null, null, null, null, null, null, null, '1ec08564dcc344018d6aaa910068f0f0', '0');
INSERT INTO `base_resource_authority` VALUES ('4988c7b20a6540fa85191736f9ebd877', '258651cfd4ca41cbaa70d1b233659b5c', 'group', '13', 'menu', '-1', null, null, '2019-07-03 10:05:32', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24', '0');
INSERT INTO `base_resource_authority` VALUES ('4f144c816618461c824f5936e8f53cd8', '258651cfd4ca41cbaa70d1b233659b5c', 'group', '3', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '1');
INSERT INTO `base_resource_authority` VALUES ('51a0b58224344d36bf0a00b748add4c5', '-1', 'group', 'be7bc967f5e548c09cbf8d1a5c8d1f11', 'menu', '-1', null, null, '2019-07-03 10:04:43', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24', '0');
INSERT INTO `base_resource_authority` VALUES ('549045f119644bac9a4442fe7afe3b41', '-1', 'group', '-1', 'menu', '-1', null, null, '2019-07-03 10:04:43', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24', '0');
INSERT INTO `base_resource_authority` VALUES ('5f68f37f838941b0b05ee22a7974a8af', '258651cfd4ca41cbaa70d1b233659b5c', 'group', '30', 'menu', '-1', null, null, '2019-07-03 10:05:32', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24', '0');
INSERT INTO `base_resource_authority` VALUES ('5f7be9ff9a7b49c8ac5a809d6696f5cd', 'a38aa1d3913742ec86370a17b2ed0507', 'group', '5', 'button', '-1', null, null, '2018-03-06 11:30:50', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', null, null, null, null, null, null, null, null, null, '1ec08564dcc344018d6aaa910068f0f0', '0');
INSERT INTO `base_resource_authority` VALUES ('5f8ac59b48e44ca2b3661be9047ffe18', '258651cfd4ca41cbaa70d1b233659b5c', 'group', '33', 'button', '-1', null, null, '2018-08-22 10:26:54', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24', '0');
INSERT INTO `base_resource_authority` VALUES ('6027aa841e104c9a9ce3eefa176d2881', 'b0bfc23bd12f47418044c556405b52ac', 'group', '6', 'menu', '-1', null, null, '2018-03-06 08:31:38', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', null, null, null, null, null, null, null, null, null, '1ec08564dcc344018d6aaa910068f0f0', '0');
INSERT INTO `base_resource_authority` VALUES ('60d9b7e37b6d418694347a352b975500', '258651cfd4ca41cbaa70d1b233659b5c', 'group', '34', 'button', '-1', null, null, '2018-08-22 10:26:54', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24', '0');
INSERT INTO `base_resource_authority` VALUES ('6116121c0283433a8b74ff25c70a8700', '258651cfd4ca41cbaa70d1b233659b5c', 'group', '21', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '0');
INSERT INTO `base_resource_authority` VALUES ('63e366e2e7b1442b8745beb80997c823', '-1', 'group', 'a78ddff416394236ae1ee22fc3ac442b', 'menu', '-1', null, null, '2019-07-03 10:04:43', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24', '0');
INSERT INTO `base_resource_authority` VALUES ('64587eea883f4a188af1637f1773863d', 'b0bfc23bd12f47418044c556405b52ac', 'group', '3', 'button', '-1', null, null, '2018-03-06 08:30:41', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', null, null, null, null, null, null, null, null, null, '1ec08564dcc344018d6aaa910068f0f0', '0');
INSERT INTO `base_resource_authority` VALUES ('6462a77688ad42e1b27b67c0405a5e63', '-1', 'group', '5', 'menu', '-1', null, null, '2018-03-06 09:57:13', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', null, null, null, null, null, null, null, null, null, '1ec08564dcc344018d6aaa910068f0f0', '1');
INSERT INTO `base_resource_authority` VALUES ('69388de8e28c4f6f9d0d8186edc18c38', '258651cfd4ca41cbaa70d1b233659b5c', 'group', '103593127199487794f9fd8fd573db5f', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '0');
INSERT INTO `base_resource_authority` VALUES ('6969f9f5b4b24e96be9793971d85dd91', '366af0a7fd79453fbe54c0d24917ec4b', 'group', '23', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '0');
INSERT INTO `base_resource_authority` VALUES ('6b6114349f8d4395a03ab769a299ea43', '258651cfd4ca41cbaa70d1b233659b5c', 'group', '28', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '1');
INSERT INTO `base_resource_authority` VALUES ('6bfa784e7d5f4986b901de03d74d6463', '258651cfd4ca41cbaa70d1b233659b5c', 'group', '29', 'menu', '-1', null, null, '2019-07-03 10:05:32', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24', '0');
INSERT INTO `base_resource_authority` VALUES ('6d6f8b0c827d466b814ba39d4a5a8ccf', '366af0a7fd79453fbe54c0d24917ec4b', 'group', '7a0a75752c7d422abd2e30b7aad744d7', 'menu', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '0');
INSERT INTO `base_resource_authority` VALUES ('6fd8894851d140e09b6762077951f674', 'b0bfc23bd12f47418044c556405b52ac', 'group', '17', 'button', '-1', null, null, '2018-03-06 11:34:07', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', null, null, null, null, null, null, null, null, null, '1ec08564dcc344018d6aaa910068f0f0', '0');
INSERT INTO `base_resource_authority` VALUES ('70345def316a4721a95beb6266c4e621', '258651cfd4ca41cbaa70d1b233659b5c', 'group', '20', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '1');
INSERT INTO `base_resource_authority` VALUES ('714ed72bf4794c05a7bee6b7156b2450', 'b0bfc23bd12f47418044c556405b52ac', 'group', '375b04fe927c44e88593d02670acd90f', 'button', '-1', null, null, '2018-03-06 11:34:07', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', null, null, null, null, null, null, null, null, null, '1ec08564dcc344018d6aaa910068f0f0', '0');
INSERT INTO `base_resource_authority` VALUES ('74ab7b0ea38c4efeaab7b85cf0601280', '258651cfd4ca41cbaa70d1b233659b5c', 'group', '16', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '0');
INSERT INTO `base_resource_authority` VALUES ('7538845fffa144a09b5cc804eea7fa74', '366af0a7fd79453fbe54c0d24917ec4b', 'group', '-1', 'menu', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '0');
INSERT INTO `base_resource_authority` VALUES ('765d458acb0c4b5cbb9afafa6ebab24c', '258651cfd4ca41cbaa70d1b233659b5c', 'group', '7a16202657c3478c8b4ece27a792e026', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '1');
INSERT INTO `base_resource_authority` VALUES ('776967e442d44f23895050d2a86fc8d3', 'b0bfc23bd12f47418044c556405b52ac', 'group', '4', 'button', '-1', null, null, '2018-03-06 08:30:44', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', null, null, null, null, null, null, null, null, null, '1ec08564dcc344018d6aaa910068f0f0', '0');
INSERT INTO `base_resource_authority` VALUES ('7925a385408a4a21a99182268a9fe1ae', '258651cfd4ca41cbaa70d1b233659b5c', 'group', '57', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '0');
INSERT INTO `base_resource_authority` VALUES ('7c8e84550571470da9dcd79860c5260e', '258651cfd4ca41cbaa70d1b233659b5c', 'group', '52', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '0');
INSERT INTO `base_resource_authority` VALUES ('7f4938bf77084207a2c349d20cc9d826', '258651cfd4ca41cbaa70d1b233659b5c', 'group', 'cac49646909c4750b23f223f53eafc5b', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '0');
INSERT INTO `base_resource_authority` VALUES ('7f612d1600914729908e166d16914d52', '258651cfd4ca41cbaa70d1b233659b5c', 'group', '1', 'menu', '-1', null, null, '2018-03-06 11:56:07', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24', '1');
INSERT INTO `base_resource_authority` VALUES ('8366fb11f76646c595027278129a147c', 'b0bfc23bd12f47418044c556405b52ac', 'group', '-1', 'menu', '-1', null, null, '2018-03-06 08:31:38', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', null, null, null, null, null, null, null, null, null, '1ec08564dcc344018d6aaa910068f0f0', '0');
INSERT INTO `base_resource_authority` VALUES ('851abc264ac84b568fedc7d38db08eed', '258651cfd4ca41cbaa70d1b233659b5c', 'group', '21', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '1');
INSERT INTO `base_resource_authority` VALUES ('861039f0330349b1a76ae142bd010d28', 'b0bfc23bd12f47418044c556405b52ac', 'group', '3', 'button', '-1', null, null, '2018-03-06 08:30:44', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', null, null, null, null, null, null, null, null, null, '1ec08564dcc344018d6aaa910068f0f0', '0');
INSERT INTO `base_resource_authority` VALUES ('88afe766177943f8963393b997b136b2', 'b0bfc23bd12f47418044c556405b52ac', 'group', '5', 'menu', '-1', null, null, '2018-03-06 08:31:38', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', null, null, null, null, null, null, null, null, null, '1ec08564dcc344018d6aaa910068f0f0', '0');
INSERT INTO `base_resource_authority` VALUES ('89761d39099740daacd7a35565be5d2c', '-1', 'group', '1', 'menu', '-1', null, null, '2018-03-06 09:57:13', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', null, null, null, null, null, null, null, null, null, '1ec08564dcc344018d6aaa910068f0f0', '1');
INSERT INTO `base_resource_authority` VALUES ('8c6b36c8730747f796fab2176ee01e85', '-1', 'group', 'b5211cc69d234b28a97f27e63edc9a58', 'menu', '-1', null, null, '2018-03-06 09:57:13', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', null, null, null, null, null, null, null, null, null, '1ec08564dcc344018d6aaa910068f0f0', '1');
INSERT INTO `base_resource_authority` VALUES ('8e6ad67a2f1b443e85f84e5f037dea33', '366af0a7fd79453fbe54c0d24917ec4b', 'group', '59', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '0');
INSERT INTO `base_resource_authority` VALUES ('8e85205bebe64d809af60850c4cc2546', '258651cfd4ca41cbaa70d1b233659b5c', 'group', '6', 'menu', '-1', null, null, '2019-07-03 10:05:32', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24', '0');
INSERT INTO `base_resource_authority` VALUES ('8edbac86de834d40a67204f162815f12', '258651cfd4ca41cbaa70d1b233659b5c', 'group', '8b5ff030c76341059a335e0653fef5ea', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '1');
INSERT INTO `base_resource_authority` VALUES ('90e7de53222a4a048d14d6109ba4be5f', '258651cfd4ca41cbaa70d1b233659b5c', 'group', '39925b8263664c9e923f9c34f9bc2a37', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '0');
INSERT INTO `base_resource_authority` VALUES ('9315ea2672ba4d55a1319ebc20cea8df', '258651cfd4ca41cbaa70d1b233659b5c', 'group', '7', 'menu', '-1', null, null, '2018-03-06 11:56:07', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24', '1');
INSERT INTO `base_resource_authority` VALUES ('93837a92348e4c27a02e57797568aa1f', 'b0bfc23bd12f47418044c556405b52ac', 'group', '5', 'button', '-1', null, null, '2018-03-06 08:30:44', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', null, null, null, null, null, null, null, null, null, '1ec08564dcc344018d6aaa910068f0f0', '0');
INSERT INTO `base_resource_authority` VALUES ('94dd147289a3430bb037d7dd0ef2a872', '366af0a7fd79453fbe54c0d24917ec4b', 'group', '57', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '0');
INSERT INTO `base_resource_authority` VALUES ('a005ea0cb4564f2988ac39e8138e28a3', '258651cfd4ca41cbaa70d1b233659b5c', 'group', '7a0a75752c7d422abd2e30b7aad744d7', 'menu', '-1', null, null, '2018-03-06 11:56:07', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24', '1');
INSERT INTO `base_resource_authority` VALUES ('a1a13b751b914d749678579e4c9a0195', '258651cfd4ca41cbaa70d1b233659b5c', 'group', '18', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '0');
INSERT INTO `base_resource_authority` VALUES ('a1bd9de474494fc08de0651352165e2d', '258651cfd4ca41cbaa70d1b233659b5c', 'group', '5', 'menu', '-1', null, null, '2018-03-06 11:56:07', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24', '1');
INSERT INTO `base_resource_authority` VALUES ('a735ad8b48c1486ea383dee07bc627aa', '258651cfd4ca41cbaa70d1b233659b5c', 'group', 'd2773b3568c6438c8f9cc21b06b660ee', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '1');
INSERT INTO `base_resource_authority` VALUES ('a773b34e6a2949d2958f8895aab95761', '-1', 'group', '13', 'menu', '-1', null, null, '2018-03-06 09:57:13', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', null, null, null, null, null, null, null, null, null, '1ec08564dcc344018d6aaa910068f0f0', '1');
INSERT INTO `base_resource_authority` VALUES ('a91b9bd837b047b49c7645681d948e69', '258651cfd4ca41cbaa70d1b233659b5c', 'group', '7', 'menu', '-1', null, null, '2019-07-03 10:05:32', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24', '0');
INSERT INTO `base_resource_authority` VALUES ('aa3b1922117847f8905b6b13182696c7', '258651cfd4ca41cbaa70d1b233659b5c', 'group', '6d7ea83bb71d476dbfbda2bcbc1a01bd', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '1');
INSERT INTO `base_resource_authority` VALUES ('aaa5247869d54ee6873dd470badf6960', '258651cfd4ca41cbaa70d1b233659b5c', 'group', '375b04fe927c44e88593d02670acd90f', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '1');
INSERT INTO `base_resource_authority` VALUES ('afcc73fc556e40a39af61a24014455ff', '366af0a7fd79453fbe54c0d24917ec4b', 'group', '56', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '0');
INSERT INTO `base_resource_authority` VALUES ('b399763b839a481abfbf3b759710fc85', '258651cfd4ca41cbaa70d1b233659b5c', 'group', 'd2773b3568c6438c8f9cc21b06b660ee', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '0');
INSERT INTO `base_resource_authority` VALUES ('b39bd9e4219244b498bc96f321f51e0d', '258651cfd4ca41cbaa70d1b233659b5c', 'group', '23', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '1');
INSERT INTO `base_resource_authority` VALUES ('b59edec22f664787aa4186f2867908b9', '258651cfd4ca41cbaa70d1b233659b5c', 'group', '58', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '0');
INSERT INTO `base_resource_authority` VALUES ('b5df9e8e2ed34acebc9ddfb103b87b60', 'b0bfc23bd12f47418044c556405b52ac', 'group', '21b3fe683a6040ed8c57423e55cd94a2', 'button', '-1', null, null, '2018-03-06 11:34:07', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', null, null, null, null, null, null, null, null, null, '1ec08564dcc344018d6aaa910068f0f0', '0');
INSERT INTO `base_resource_authority` VALUES ('b5f00cb818f24dd5a241009aa71c030e', '366af0a7fd79453fbe54c0d24917ec4b', 'group', '58', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '0');
INSERT INTO `base_resource_authority` VALUES ('bd9395bb258b401b95e712a698724d24', '258651cfd4ca41cbaa70d1b233659b5c', 'group', '3', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '0');
INSERT INTO `base_resource_authority` VALUES ('c03f77f8561e4d63a251adbe8cb13609', '258651cfd4ca41cbaa70d1b233659b5c', 'group', '27', 'menu', '-1', null, null, '2019-07-03 10:05:32', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24', '0');
INSERT INTO `base_resource_authority` VALUES ('c17441b541ef40d19faefcd6a30160a5', '366af0a7fd79453fbe54c0d24917ec4b', 'group', '23', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '0');
INSERT INTO `base_resource_authority` VALUES ('c2571d38498e45fcb7fcd06f96174608', '258651cfd4ca41cbaa70d1b233659b5c', 'group', '16', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '1');
INSERT INTO `base_resource_authority` VALUES ('c420da58959146ec989933575b3c1d87', '258651cfd4ca41cbaa70d1b233659b5c', 'group', '39925b8263664c9e923f9c34f9bc2a37', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '1');
INSERT INTO `base_resource_authority` VALUES ('c568c45e5e8042a1aa6fe9ad157b9ea9', '258651cfd4ca41cbaa70d1b233659b5c', 'group', '68ee45b69c0f41cb9d6a5c33d46fb7a6', 'menu', '-1', null, null, '2019-07-03 10:05:32', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24', '0');
INSERT INTO `base_resource_authority` VALUES ('c70e0c9fe1cc487c8c8b1e7c3f2ab268', '366af0a7fd79453fbe54c0d24917ec4b', 'group', '5', 'menu', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '0');
INSERT INTO `base_resource_authority` VALUES ('c86c123b98bc40fdafb525529c432c8c', 'b0bfc23bd12f47418044c556405b52ac', 'group', '20', 'button', '-1', null, null, '2018-03-06 11:34:07', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', null, null, null, null, null, null, null, null, null, '1ec08564dcc344018d6aaa910068f0f0', '0');
INSERT INTO `base_resource_authority` VALUES ('c8ba65c87dcd40e3adc27fd9270d85b2', '258651cfd4ca41cbaa70d1b233659b5c', 'group', '4', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '0');
INSERT INTO `base_resource_authority` VALUES ('cb0ac5ca7a924f13b23460d6936295de', '258651cfd4ca41cbaa70d1b233659b5c', 'group', '23', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '0');
INSERT INTO `base_resource_authority` VALUES ('d13e961443fc4f56817ae155792e5ed6', '258651cfd4ca41cbaa70d1b233659b5c', 'group', '59', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '0');
INSERT INTO `base_resource_authority` VALUES ('d18d845218ed41e5b15fc97acac94c00', '-1', 'group', '7', 'menu', '-1', null, null, '2018-03-06 09:57:13', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', null, null, null, null, null, null, null, null, null, '1ec08564dcc344018d6aaa910068f0f0', '1');
INSERT INTO `base_resource_authority` VALUES ('d2aa9986802d418f845514fd6a286832', '-1', 'group', '6', 'menu', '-1', null, null, '2018-03-06 09:57:13', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', null, null, null, null, null, null, null, null, null, '1ec08564dcc344018d6aaa910068f0f0', '1');
INSERT INTO `base_resource_authority` VALUES ('d4b0c3ddb45d484a8a17d2a038b82161', '366af0a7fd79453fbe54c0d24917ec4b', 'group', '13', 'menu', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '0');
INSERT INTO `base_resource_authority` VALUES ('d4b8556b3dda429ebbfbbe0b9ec90ba8', 'b0bfc23bd12f47418044c556405b52ac', 'group', '16', 'button', '-1', null, null, '2018-03-06 11:34:07', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', null, null, null, null, null, null, null, null, null, '1ec08564dcc344018d6aaa910068f0f0', '0');
INSERT INTO `base_resource_authority` VALUES ('da2c26d274634f0c8ab393ad852a8b09', '258651cfd4ca41cbaa70d1b233659b5c', 'group', 'c6469be83d8e4d5ca404b46b347f7d4c', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '1');
INSERT INTO `base_resource_authority` VALUES ('da86e6922a664a58afdd1880b8db37db', '-1', 'group', '5', 'menu', '-1', null, null, '2019-07-03 10:04:43', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24', '0');
INSERT INTO `base_resource_authority` VALUES ('db0c5dcf55874e50843286ed62ff8a66', '258651cfd4ca41cbaa70d1b233659b5c', 'group', '6', 'menu', '-1', null, null, '2018-03-06 11:56:07', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24', '1');
INSERT INTO `base_resource_authority` VALUES ('e19bf5b10e8f431b9c01e0213fa39b41', '258651cfd4ca41cbaa70d1b233659b5c', 'group', 'ef73cc9bf70d448caf72e3f45015700a', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '1');
INSERT INTO `base_resource_authority` VALUES ('e1b31c5828ce4d3485c9d35de0752dca', '258651cfd4ca41cbaa70d1b233659b5c', 'group', '22', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '0');
INSERT INTO `base_resource_authority` VALUES ('e1bf257da1314d78b8ee2c92f87c13ec', '366af0a7fd79453fbe54c0d24917ec4b', 'group', '3', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '0');
INSERT INTO `base_resource_authority` VALUES ('e38822dfa84840e6afa9e67dbaf2edcd', 'b0bfc23bd12f47418044c556405b52ac', 'group', '22', 'button', '-1', null, null, '2018-03-06 11:34:07', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', null, null, null, null, null, null, null, null, null, '1ec08564dcc344018d6aaa910068f0f0', '0');
INSERT INTO `base_resource_authority` VALUES ('e43ac2d407d14891a5e55d815f144338', '258651cfd4ca41cbaa70d1b233659b5c', 'group', '17', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '1');
INSERT INTO `base_resource_authority` VALUES ('e72e2b709b4c4e5f8f65ee9dd21fd057', '258651cfd4ca41cbaa70d1b233659b5c', 'group', 'ef73cc9bf70d448caf72e3f45015700a', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '0');
INSERT INTO `base_resource_authority` VALUES ('f2fe1150f0ab486c85443b5052516e63', '-1', 'group', '7a0a75752c7d422abd2e30b7aad744d7', 'menu', '-1', null, null, '2018-03-06 09:57:13', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', null, null, null, null, null, null, null, null, null, '1ec08564dcc344018d6aaa910068f0f0', '1');
INSERT INTO `base_resource_authority` VALUES ('f3b8b7d4fd224b92a182f5edac3b0522', '258651cfd4ca41cbaa70d1b233659b5c', 'group', '19', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '0');
INSERT INTO `base_resource_authority` VALUES ('f3bf9ea034e444c08b7ca2c3a86d0c7e', '258651cfd4ca41cbaa70d1b233659b5c', 'group', '59', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '1');
INSERT INTO `base_resource_authority` VALUES ('f4044e45425d4f93b834fd91d9c3ea32', '258651cfd4ca41cbaa70d1b233659b5c', 'group', 'b5211cc69d234b28a97f27e63edc9a58', 'menu', '-1', null, null, '2018-03-06 11:56:07', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24', '1');
INSERT INTO `base_resource_authority` VALUES ('f63ca558b7904bc4ba88905dcc4f27f7', '258651cfd4ca41cbaa70d1b233659b5c', 'group', '1', 'menu', '-1', null, null, '2019-07-03 10:05:32', '1', 'Mr.AG', null, null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24', '0');
INSERT INTO `base_resource_authority` VALUES ('f7f7d1e5ffb84c38909e99a6940ca03e', '258651cfd4ca41cbaa70d1b233659b5c', 'group', '18', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '1');
INSERT INTO `base_resource_authority` VALUES ('f8c2f63256d740ac93fca56c040bff4d', 'b0bfc23bd12f47418044c556405b52ac', 'group', '23', 'button', '-1', null, null, '2018-03-06 08:30:44', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', null, null, null, null, null, null, null, null, null, '1ec08564dcc344018d6aaa910068f0f0', '0');
INSERT INTO `base_resource_authority` VALUES ('f96a9425b188416e9b7165c07ed5579e', '258651cfd4ca41cbaa70d1b233659b5c', 'group', '20', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '0');
INSERT INTO `base_resource_authority` VALUES ('faebfb6f224743f89db2c6e4b42ef568', '258651cfd4ca41cbaa70d1b233659b5c', 'group', '58', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '1');
INSERT INTO `base_resource_authority` VALUES ('fb0811cdcb2743a0abaccab0033e680b', '258651cfd4ca41cbaa70d1b233659b5c', 'group', '375b04fe927c44e88593d02670acd90f', 'button', '-1', null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, '0');

-- ----------------------------
-- Table structure for base_tenant
-- ----------------------------
DROP TABLE IF EXISTS `base_tenant`;
CREATE TABLE `base_tenant` (
  `id` varchar(36) NOT NULL,
  `code` varchar(255) DEFAULT NULL COMMENT '编码',
  `name` varchar(255) DEFAULT NULL COMMENT '最后更新时间',
  `crt_user_name` varchar(255) DEFAULT NULL COMMENT '创建人',
  `crt_user_id` varchar(36) DEFAULT NULL COMMENT '创建人ID',
  `crt_time` datetime DEFAULT NULL COMMENT '创建时间',
  `upd_user_name` varchar(255) DEFAULT NULL COMMENT '最后更新人',
  `upd_user_id` varchar(36) DEFAULT NULL COMMENT '最后更新人ID',
  `upd_time` datetime DEFAULT NULL COMMENT '最后更新时间',
  `attr1` varchar(255) DEFAULT NULL,
  `attr2` varchar(255) DEFAULT NULL,
  `attr3` varchar(255) DEFAULT NULL,
  `attr4` varchar(255) DEFAULT NULL,
  `description` varchar(500) DEFAULT NULL,
  `is_super_tenant` varchar(1) DEFAULT NULL COMMENT '是否超级租户',
  `tenant_id` varchar(36) DEFAULT NULL,
  `owner` varchar(36) DEFAULT NULL COMMENT '拥有者',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='租户表';

-- ----------------------------
-- Records of base_tenant
-- ----------------------------
INSERT INTO `base_tenant` VALUES ('1ec08564dcc344018d6aaa910068f0f0', 'testTenant', '测试租户', 'Mr.AG', '1', '2018-02-12 15:30:54', 'Mr.AG', '1', '2018-02-15 09:58:39', null, null, null, null, '测试租户', '0', 'ac88ceb386aa4231b09bf472cb937c24', 'a1a2cb17b4f24e50bbde2f1b8a233bcb');
INSERT INTO `base_tenant` VALUES ('343914e2e39544678cb574aa773787cc', 'salerPersonTenant', '商城卖家-个人', 'Mr.AG', '1', '2018-08-29 09:25:31', 'Mr.AG', '1', '2018-08-29 09:27:12', null, null, null, null, null, '0', 'ac88ceb386aa4231b09bf472cb937c24', null);
INSERT INTO `base_tenant` VALUES ('352ebd09f0de4387acd3d687894b3198', 'buyerTenant', '商城买家', 'Mr.AG', '1', '2018-08-29 09:24:44', 'Mr.AG', '1', '2018-08-29 09:24:44', null, null, null, null, null, '0', 'ac88ceb386aa4231b09bf472cb937c24', null);
INSERT INTO `base_tenant` VALUES ('5378f476ec7742fdbef44193bd0d2c5b', 'salerGroupTenant', '商城卖家-集团', 'Mr.AG', '1', '2018-08-29 09:27:00', 'Mr.AG', '1', '2018-08-29 09:27:00', null, null, null, null, null, '0', 'ac88ceb386aa4231b09bf472cb937c24', null);
INSERT INTO `base_tenant` VALUES ('62ac9f4e51104588a3610ac586bc5c5a', 'salerCommpanyTenant', '商城卖家-企业', 'Mr.AG', '1', '2018-08-29 09:26:16', 'Mr.AG', '1', '2019-07-16 12:51:00', null, null, null, null, null, '0', 'ac88ceb386aa4231b09bf472cb937c24', null);
INSERT INTO `base_tenant` VALUES ('ac88ceb386aa4231b09bf472cb937c24', 'superTenant', '超级租户', 'Mr.AG', '1', '2018-02-09 16:31:20', 'Mr.AG', '1', '2018-02-15 10:57:13', null, null, null, null, '超级租户', '1', 'ac88ceb386aa4231b09bf472cb937c24', '1');

-- ----------------------------
-- Table structure for base_user
-- ----------------------------
DROP TABLE IF EXISTS `base_user`;
CREATE TABLE `base_user` (
  `id` varchar(36) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `birthday` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `mobile_phone` varchar(255) DEFAULT NULL,
  `tel_phone` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `sex` varchar(16) DEFAULT NULL,
  `type` char(1) DEFAULT NULL,
  `status` char(1) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `crt_time` datetime DEFAULT NULL,
  `crt_user_id` varchar(255) DEFAULT NULL,
  `crt_user_name` varchar(255) DEFAULT NULL,
  `upd_time` datetime DEFAULT NULL,
  `upd_user_id` varchar(255) DEFAULT NULL,
  `upd_user_name` varchar(255) DEFAULT NULL,
  `attr1` varchar(255) DEFAULT NULL,
  `attr2` varchar(255) DEFAULT NULL,
  `attr3` varchar(255) DEFAULT NULL,
  `attr4` varchar(255) DEFAULT NULL,
  `attr5` varchar(255) DEFAULT NULL,
  `attr6` varchar(255) DEFAULT NULL,
  `attr7` varchar(255) DEFAULT NULL,
  `attr8` varchar(255) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL COMMENT '租户Id',
  `is_deleted` char(1) DEFAULT NULL COMMENT '是否删除',
  `is_disabled` char(1) DEFAULT NULL COMMENT '是否作废',
  `depart_id` varchar(36) DEFAULT NULL COMMENT '默认部门',
  `is_super_admin` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_usernane` (`username`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of base_user
-- ----------------------------
INSERT INTO `base_user` VALUES ('1', 'admin', '$2a$10$WhxyKXLYhZg0r5AbErY0RuHVw7Cw/EzdCPZVjnWxtTg2O5gBPfkFu', 'admin', '', null, '', null, '', '男', null, null, '', null, null, null, '2019-12-13 10:19:57', '1', 'Mr.AG', null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24', '0', '0', 'root', '1');
INSERT INTO `base_user` VALUES ('59c28b8593c2472e99e54e3ed9b6efe8', 'wxiaoqi', '$2a$10$WhxyKXLYhZg0r5AbErY0RuHVw7Cw/EzdCPZVjnWxtTg2O5gBPfkFu', '王小七', null, null, null, null, null, '男', null, '1', null, '2018-02-12 23:41:35', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', '2018-02-12 23:54:07', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24', '0', '0', 'd8bb2fc8d51745458f77a78c42bf60ab', '0');
INSERT INTO `base_user` VALUES ('5b29e88e363c41c394ad50cf830f6e0a', 'test', '$2a$10$WhxyKXLYhZg0r5AbErY0RuHVw7Cw/EzdCPZVjnWxtTg2O5gBPfkFu', 'test', null, null, null, null, null, '男', null, null, null, '2018-03-28 20:11:07', '1', 'Mr.AG', '2018-03-28 20:11:07', '1', 'Mr.AG', null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24', '0', '0', 'root', '0');
INSERT INTO `base_user` VALUES ('9305e0dd01014842b758aa806c46952b', '123', '$2a$10$WhxyKXLYhZg0r5AbErY0RuHVw7Cw/EzdCPZVjnWxtTg2O5gBPfkFu', '123', null, null, null, null, null, '男', null, null, null, '2018-04-04 19:43:54', '1', 'Mr.AG', '2018-04-04 19:44:40', '1', 'Mr.AG', null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24', '1', '0', 'root', '0');
INSERT INTO `base_user` VALUES ('a1a2cb17b4f24e50bbde2f1b8a233bcb', 'tenant', '$2a$10$WhxyKXLYhZg0r5AbErY0RuHVw7Cw/EzdCPZVjnWxtTg2O5gBPfkFu', '测试租户', null, null, null, null, null, '男', null, null, null, '2018-02-12 20:27:14', '1', 'Mr.AG', '2018-03-10 13:19:10', '1', 'Mr.AG', null, null, null, null, null, null, null, null, '1ec08564dcc344018d6aaa910068f0f0', '0', '0', 'd583e7de6d2d48b78fb3c7dcb180cb1f', '0');
INSERT INTO `base_user` VALUES ('f2c2fa758a7f418d9d7cfebf4002e523', 'hushming', '$2a$10$WhxyKXLYhZg0r5AbErY0RuHVw7Cw/EzdCPZVjnWxtTg2O5gBPfkFu', '胡善铭', null, null, null, null, null, '男', null, null, null, '2019-07-16 12:46:28', '1', 'Mr.AG', '2019-07-16 12:46:28', '1', 'Mr.AG', null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24', '0', '0', 'root', '0');

-- ----------------------------
-- Table structure for dict_type
-- ----------------------------
DROP TABLE IF EXISTS `dict_type`;
CREATE TABLE `dict_type` (
  `id` varchar(36) NOT NULL COMMENT '主键',
  `code` varchar(100) DEFAULT NULL COMMENT '编码',
  `type` varchar(1) DEFAULT NULL COMMENT '类型',
  `name` varchar(255) DEFAULT NULL COMMENT '目录名',
  `parent_id` varchar(36) DEFAULT NULL,
  `crt_user_name` varchar(255) DEFAULT NULL COMMENT '创建人',
  `crt_user_id` varchar(36) DEFAULT NULL COMMENT '创建人ID',
  `crt_time` datetime DEFAULT NULL COMMENT '创建时间',
  `upd_user_name` varchar(255) DEFAULT NULL COMMENT '最后更新人',
  `upd_user_id` varchar(36) DEFAULT NULL COMMENT '最后更新人ID',
  `upd_time` datetime DEFAULT NULL COMMENT '最后更新时间',
  `attr1` varchar(255) DEFAULT NULL,
  `attr2` varchar(255) DEFAULT NULL,
  `attr3` varchar(255) DEFAULT NULL,
  `attr4` varchar(255) DEFAULT NULL,
  `tenant_id` varchar(36) DEFAULT NULL COMMENT '租户ID',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_code` (`code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of dict_type
-- ----------------------------
INSERT INTO `dict_type` VALUES ('2e7a6f2dab184e3c891040d7b9b7e7ec', 'authority_element', null, '资源类型', '8ab51d3c861d476eb10600b2ed0f1f0d', '', null, '2018-02-01 12:23:39', '', null, '2018-02-01 12:24:04', null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `dict_type` VALUES ('4de6efea2456471ca3a4efa9f757d7cc', 'org', null, '组织部门', '64fce2915a213504be2b08ad12746af2', 'Mr.AG', '1', '2018-02-04 23:04:11', 'Mr.AG', '1', '2018-02-04 23:04:28', null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `dict_type` VALUES ('53b7b71f63273f53aa2c7590b446fb33', 'comm', '0', '公共字典', '64fce2915a213504be2b08ad12746af2', null, null, null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `dict_type` VALUES ('64fce2915a213504be2b08ad12746af2', 'root', '0', '根节点', '-1', null, null, null, null, null, null, null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `dict_type` VALUES ('67374d6aebeb4cc28484c7f303ec85f5', 'authority_menu', null, '菜单类型', '8ab51d3c861d476eb10600b2ed0f1f0d', '', null, '2018-02-01 12:23:26', '', null, '2018-02-01 12:23:37', null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `dict_type` VALUES ('6db3bb1a0bcb4cfca75a1e9d87996ff0', 'org_position', null, '岗位类型', '4de6efea2456471ca3a4efa9f757d7cc', 'Mr.AG', '1', '2018-04-04 18:56:30', 'Mr.AG', '1', '2018-04-04 19:06:30', null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `dict_type` VALUES ('8ab51d3c861d476eb10600b2ed0f1f0d', 'authority', null, '权限字典', '64fce2915a213504be2b08ad12746af2', '', null, '2018-02-01 12:22:58', '', null, '2018-02-01 12:23:22', null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `dict_type` VALUES ('cd9c72b4554a4061a0aa50755bd5419a', 'org_depart', null, '部门类型', '4de6efea2456471ca3a4efa9f757d7cc', 'Mr.AG', '1', '2018-02-04 23:04:31', 'Mr.AG', '1', '2018-02-04 23:11:43', null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24');
INSERT INTO `dict_type` VALUES ('d62b242fde833653bce1498787ea6c31', 'comm_sex', '1', '性别', '53b7b71f63273f53aa2c7590b446fb33', '', null, '2018-01-31 17:36:38', '', null, '2018-01-31 17:36:49', null, null, null, null, 'ac88ceb386aa4231b09bf472cb937c24');

-- ----------------------------
-- Table structure for dict_value
-- ----------------------------
DROP TABLE IF EXISTS `dict_value`;
CREATE TABLE `dict_value` (
  `id` varchar(36) NOT NULL COMMENT '主键',
  `tenant_id` varchar(36) DEFAULT NULL COMMENT '租户ID',
  `code` varchar(100) DEFAULT NULL COMMENT '编码',
  `value` varchar(255) DEFAULT NULL COMMENT '值',
  `label_default` varchar(255) DEFAULT NULL COMMENT '默认显示',
  `label_en_US` varchar(255) DEFAULT NULL COMMENT '英文显示',
  `label_zh_CH` varchar(255) DEFAULT NULL COMMENT '中文显示',
  `type_id` varchar(36) DEFAULT NULL,
  `label_attr1` varchar(255) DEFAULT NULL COMMENT '值',
  `label_attr2` varchar(255) DEFAULT NULL COMMENT '值',
  `label_attr3` varchar(255) DEFAULT NULL COMMENT '值',
  `crt_user_name` varchar(255) DEFAULT NULL COMMENT '创建人',
  `crt_user_id` varchar(36) DEFAULT NULL COMMENT '创建人ID',
  `crt_time` datetime DEFAULT NULL COMMENT '创建时间',
  `upd_user_name` varchar(255) DEFAULT NULL COMMENT '最后更新人',
  `upd_user_id` varchar(36) DEFAULT NULL COMMENT '最后更新人ID',
  `upd_time` datetime DEFAULT NULL COMMENT '最后更新时间',
  `parent_id` varchar(36) DEFAULT NULL,
  `attr1` varchar(255) DEFAULT NULL,
  `attr2` varchar(255) DEFAULT NULL,
  `attr3` varchar(255) DEFAULT NULL,
  `attr4` varchar(255) DEFAULT NULL,
  `order_num` int(11) DEFAULT '1' COMMENT '排序',
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_code` (`code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of dict_value
-- ----------------------------
INSERT INTO `dict_value` VALUES ('063ac8469d2b48398cc8263b8b786356', 'ac88ceb386aa4231b09bf472cb937c24', 'org_depart_group', 'group', '集团', 'Group', '集团', 'cd9c72b4554a4061a0aa50755bd5419a', null, null, null, 'Mr.AG', '1', '2018-02-04 23:08:46', 'Mr.AG', '1', '2018-02-05 12:40:41', null, null, null, null, null, '1');
INSERT INTO `dict_value` VALUES ('276fc4a924b241d6820209c42e1b2ccc', 'ac88ceb386aa4231b09bf472cb937c24', 'authority_element_button', 'button', '按钮', 'button', 'button', '2e7a6f2dab184e3c891040d7b9b7e7ec', null, null, null, '', null, '2018-02-01 12:26:38', 'Mr.AG', '1', '2018-02-02 19:43:43', null, null, null, null, null, '1');
INSERT INTO `dict_value` VALUES ('49000e5b738544df9f864489142e518c', 'ac88ceb386aa4231b09bf472cb937c24', 'org_depart_ou', 'ou', '公司', 'OU', '公司', 'cd9c72b4554a4061a0aa50755bd5419a', null, null, null, 'Mr.AG', '1', '2018-02-04 23:10:17', 'Mr.AG', '1', '2018-02-05 12:40:46', null, null, null, null, null, '2');
INSERT INTO `dict_value` VALUES ('4b1112f4d71c492796690062aeedeb25', 'ac88ceb386aa4231b09bf472cb937c24', 'authority_element_uri', 'uri', '资源', 'uri', 'uri', '2e7a6f2dab184e3c891040d7b9b7e7ec', null, null, null, '', null, '2018-02-01 12:26:01', 'Mr.AG', '1', '2018-02-02 19:43:54', null, null, null, null, null, '1');
INSERT INTO `dict_value` VALUES ('648eba56aba04fdbb5be553c27234ab9', 'ac88ceb386aa4231b09bf472cb937c24', 'org_position_role', 'role', '权限岗位', '权限岗位', '权限岗位', '6db3bb1a0bcb4cfca75a1e9d87996ff0', null, null, null, 'Mr.AG', '1', '2018-04-04 18:57:38', 'Mr.AG', '1', '2018-04-04 18:57:38', null, null, null, null, null, '2');
INSERT INTO `dict_value` VALUES ('657ae6bd192a36729d39446174bcf64c', 'ac88ceb386aa4231b09bf472cb937c24', 'comm_sex_woman', 'woman', '女', 'Woman', '女', 'd62b242fde833653bce1498787ea6c31', null, null, null, null, null, null, '', null, '2018-02-01 12:51:52', null, null, null, null, null, '1');
INSERT INTO `dict_value` VALUES ('687a9d26f11049d29ea5e33eecd6a4fd', 'ac88ceb386aa4231b09bf472cb937c24', 'org_depart_dept', 'dept', '部门', 'Depart', '部门', 'cd9c72b4554a4061a0aa50755bd5419a', null, null, null, 'Mr.AG', '1', '2018-02-04 23:11:02', 'Mr.AG', '1', '2018-02-05 12:40:52', null, null, null, null, null, '4');
INSERT INTO `dict_value` VALUES ('75a832a6d3684b4a9aa62e04e84213d0', 'ac88ceb386aa4231b09bf472cb937c24', 'org_depart_bu', 'bu', '事业部', 'BU', '事业部', 'cd9c72b4554a4061a0aa50755bd5419a', null, null, null, 'Mr.AG', '1', '2018-02-04 23:10:45', 'Mr.AG', '1', '2018-02-05 12:40:49', null, null, null, null, null, '3');
INSERT INTO `dict_value` VALUES ('8571cabddd083a5ab732b1df81bdf392', 'ac88ceb386aa4231b09bf472cb937c24', 'comm_sex_man', 'man', '男', 'Man', '男', 'd62b242fde833653bce1498787ea6c31', null, null, null, null, null, null, '', null, '2018-02-01 12:51:55', null, null, null, null, null, '1');
INSERT INTO `dict_value` VALUES ('adc4459f7af64bc5975da46fb8b430b3', 'ac88ceb386aa4231b09bf472cb937c24', 'org_position_flow', 'flow', '流程岗位', '流程岗位', '流程岗位', '6db3bb1a0bcb4cfca75a1e9d87996ff0', null, null, null, 'Mr.AG', '1', '2018-04-04 18:57:26', 'Mr.AG', '1', '2018-04-04 18:57:26', null, null, null, null, null, '1');
INSERT INTO `dict_value` VALUES ('c5158e555e164e648b149dafe2e89e0a', 'ac88ceb386aa4231b09bf472cb937c24', 'comm_sex_unknown', 'unknown', '未知', 'Unknown', '未知', 'd62b242fde833653bce1498787ea6c31', null, null, null, '', null, '2018-01-31 19:27:27', '', null, '2018-02-01 12:51:58', null, null, null, null, null, '1');
INSERT INTO `dict_value` VALUES ('d55d6ed5a0024779b959f43288857089', 'ac88ceb386aa4231b09bf472cb937c24', 'authority_menu_dirt', 'dirt', '目录', 'Dirt', '目录', '67374d6aebeb4cc28484c7f303ec85f5', null, null, null, '', null, '2018-02-01 12:25:09', '', null, '2018-02-01 12:52:49', null, null, null, null, null, '1');
INSERT INTO `dict_value` VALUES ('ee4920be6f424be0bfba54d7b1202068', 'ac88ceb386aa4231b09bf472cb937c24', 'authority_menu_menu', 'menu', '菜单', 'Menu', '菜单', '67374d6aebeb4cc28484c7f303ec85f5', null, null, null, '', null, '2018-02-01 12:24:23', '', null, '2018-02-01 12:52:51', null, null, null, null, null, '1');

-- ----------------------------
-- Table structure for gateway_route
-- ----------------------------
DROP TABLE IF EXISTS `gateway_route`;
CREATE TABLE `gateway_route` (
  `id` varchar(50) NOT NULL,
  `path` varchar(255) NOT NULL COMMENT '映射路劲',
  `service_id` varchar(50) DEFAULT NULL COMMENT '映射服务',
  `url` varchar(255) DEFAULT NULL COMMENT '映射外连接',
  `retryable` tinyint(1) DEFAULT NULL COMMENT '是否重试',
  `enabled` tinyint(1) NOT NULL COMMENT '是否启用',
  `strip_prefix` tinyint(1) DEFAULT NULL COMMENT '是否忽略前缀',
  `crt_user_name` varchar(255) DEFAULT NULL COMMENT '创建人',
  `crt_user_id` varchar(36) DEFAULT NULL COMMENT '创建人ID',
  `crt_time` datetime DEFAULT NULL COMMENT '创建时间',
  `upd_user_name` varchar(255) DEFAULT NULL COMMENT '最后更新人',
  `upd_user_id` varchar(36) DEFAULT NULL COMMENT '最后更新人ID',
  `upd_time` datetime DEFAULT NULL COMMENT '最后更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of gateway_route
-- ----------------------------
INSERT INTO `gateway_route` VALUES ('admin', '/api/admin/**', 'cloud-admin', null, '0', '1', '1', 'Mr.AG', '1', '2018-02-25 14:33:30', 'Mr.AG', '1', '2018-02-25 14:38:31');
INSERT INTO `gateway_route` VALUES ('auth', '/api/auth/**', 'cloud-auth', null, '0', '1', '1', null, null, null, 'Mr.AG', '1', '2018-02-25 14:29:51');
INSERT INTO `gateway_route` VALUES ('center', '/api/center/**', 'cloud-center', null, '0', '1', '1', 'Mr.AG', '1', '2018-02-26 12:50:51', 'Mr.AG', '1', '2018-02-26 12:50:51');
INSERT INTO `gateway_route` VALUES ('dict', '/api/dict/**', 'cloud-dict', null, '0', '1', '1', null, null, null, 'Mr.AG', '1', '2018-02-25 14:41:07');
INSERT INTO `gateway_route` VALUES ('health-web', '/api/web/**', 'health-web', null, '0', '1', '1', 'Mr.AG', '1', '2019-09-02 11:02:54', 'Mr.AG', '1', '2019-09-02 11:02:54');
INSERT INTO `gateway_route` VALUES ('healthauth', '/api/eauth/**', 'health-auth', null, '0', '1', '1', 'Mr.AG', '1', '2019-08-29 17:07:12', 'Mr.AG', '1', '2019-08-29 17:07:12');
INSERT INTO `gateway_route` VALUES ('tool', '/api/tool/**', 'cloud-tool', null, '0', '1', '1', null, null, '2018-04-02 21:04:47', 'Mr.AG', '1', '2018-09-21 18:50:16');
INSERT INTO `gateway_route` VALUES ('workflow', '/api/wf/**', 'cloud-workflow', null, '0', '1', '1', null, null, '2018-04-05 13:58:08', null, null, '2018-04-05 13:58:14');

-- ----------------------------
-- Table structure for gate_log
-- ----------------------------
DROP TABLE IF EXISTS `gate_log`;
CREATE TABLE `gate_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '序号',
  `tenant_id` varchar(36) DEFAULT NULL COMMENT '租户Id',
  `menu` varchar(255) DEFAULT NULL COMMENT '菜单',
  `opt` varchar(255) DEFAULT NULL COMMENT '操作',
  `uri` varchar(255) DEFAULT NULL COMMENT '资源路径',
  `crt_time` datetime DEFAULT NULL COMMENT '操作时间',
  `crt_user` varchar(255) DEFAULT NULL COMMENT '操作人ID',
  `crt_name` varchar(255) DEFAULT NULL COMMENT '操作人',
  `crt_host` varchar(255) DEFAULT NULL COMMENT '操作主机',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=353 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of gate_log
-- ----------------------------
INSERT INTO `gate_log` VALUES ('1', 'ac88ceb386aa4231b09bf472cb937c24', '部门管理', '新增部门值', '/admin/depart', '2018-02-12 20:09:46', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('2', 'ac88ceb386aa4231b09bf472cb937c24', '部门管理', '删除部门值', '/admin/depart', '2018-02-12 20:12:14', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('3', 'ac88ceb386aa4231b09bf472cb937c24', '部门管理', '删除部门值', '/admin/depart', '2018-02-12 20:12:14', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('4', 'ac88ceb386aa4231b09bf472cb937c24', '部门管理', '新增部门值', '/admin/depart', '2018-02-12 20:12:18', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('5', 'ac88ceb386aa4231b09bf472cb937c24', '部门管理', '编辑部门值', '/admin/depart', '2018-02-12 20:13:55', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('6', 'ac88ceb386aa4231b09bf472cb937c24', '部门管理', '新增部门值', '/admin/depart', '2018-02-12 20:15:49', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('7', 'ac88ceb386aa4231b09bf472cb937c24', '部门管理', '新增部门值', '/admin/depart', '2018-02-12 20:19:34', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('8', 'ac88ceb386aa4231b09bf472cb937c24', '部门管理', '新增部门值', '/admin/depart', '2018-02-12 20:19:42', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('9', 'ac88ceb386aa4231b09bf472cb937c24', '部门管理', '新增部门值', '/admin/depart', '2018-02-12 20:20:09', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('10', 'ac88ceb386aa4231b09bf472cb937c24', '部门管理', '新增部门值', '/admin/depart', '2018-02-12 20:20:46', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('11', 'ac88ceb386aa4231b09bf472cb937c24', '部门管理', '删除部门值', '/admin/depart', '2018-02-12 20:25:55', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('12', 'ac88ceb386aa4231b09bf472cb937c24', '部门管理', '删除部门值', '/admin/depart', '2018-02-12 20:25:57', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('13', 'ac88ceb386aa4231b09bf472cb937c24', '部门管理', '编辑部门值', '/admin/depart', '2018-02-12 20:26:25', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('14', 'ac88ceb386aa4231b09bf472cb937c24', '用户管理', '新增', '/admin/user', '2018-02-12 20:27:10', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('15', 'ac88ceb386aa4231b09bf472cb937c24', '用户管理', '编辑', '/admin/user', '2018-02-12 20:29:42', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('16', 'ac88ceb386aa4231b09bf472cb937c24', '菜单管理', '新增元素', '/admin/element', '2018-02-12 21:05:47', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('17', 'ac88ceb386aa4231b09bf472cb937c24', '租户管理', '编辑租户', '/admin/tenant', '2018-02-12 21:22:59', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('18', 'ac88ceb386aa4231b09bf472cb937c24', '租户管理', '编辑租户', '/admin/tenant', '2018-02-12 21:32:00', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('19', 'ac88ceb386aa4231b09bf472cb937c24', '租户管理', '编辑租户', '/admin/tenant', '2018-02-12 21:33:02', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('20', 'ac88ceb386aa4231b09bf472cb937c24', '租户管理', '编辑租户', '/admin/tenant', '2018-02-12 21:34:47', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('21', 'ac88ceb386aa4231b09bf472cb937c24', '租户管理', '编辑租户', '/admin/tenant', '2018-02-12 21:41:39', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('22', 'ac88ceb386aa4231b09bf472cb937c24', '角色权限管理', '新增', '/admin/group', '2018-02-12 23:20:07', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('23', 'ac88ceb386aa4231b09bf472cb937c24', '角色权限管理', '新增', '/admin/group', '2018-02-12 23:20:47', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('24', 'ac88ceb386aa4231b09bf472cb937c24', '角色权限管理', '新增', '/admin/group', '2018-02-12 23:20:53', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('25', 'ac88ceb386aa4231b09bf472cb937c24', '角色权限管理', '新增', '/admin/group', '2018-02-12 23:25:14', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('26', 'ac88ceb386aa4231b09bf472cb937c24', '角色权限管理', '编辑', '/admin/group', '2018-02-12 23:25:22', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('27', 'ac88ceb386aa4231b09bf472cb937c24', '角色权限管理', '新增', '/admin/group', '2018-02-12 23:25:46', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('28', 'ac88ceb386aa4231b09bf472cb937c24', '角色权限管理', '新增', '/admin/group', '2018-02-12 23:26:12', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('29', 'ac88ceb386aa4231b09bf472cb937c24', '角色权限管理', '新增', '/admin/group', '2018-02-12 23:26:27', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('30', 'ac88ceb386aa4231b09bf472cb937c24', '角色权限管理', '新增', '/admin/group', '2018-02-12 23:26:36', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('31', 'ac88ceb386aa4231b09bf472cb937c24', '角色权限管理', '新增', '/admin/group', '2018-02-12 23:26:36', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('32', 'ac88ceb386aa4231b09bf472cb937c24', '角色权限管理', '新增', '/admin/group', '2018-02-12 23:26:36', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('33', 'ac88ceb386aa4231b09bf472cb937c24', '角色权限管理', '新增', '/admin/group', '2018-02-12 23:26:36', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('34', 'ac88ceb386aa4231b09bf472cb937c24', '角色权限管理', '新增', '/admin/group', '2018-02-12 23:26:46', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('35', 'ac88ceb386aa4231b09bf472cb937c24', '角色权限管理', '新增', '/admin/group', '2018-02-12 23:26:50', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('36', 'ac88ceb386aa4231b09bf472cb937c24', '角色权限管理', '新增', '/admin/group', '2018-02-12 23:26:50', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('37', 'ac88ceb386aa4231b09bf472cb937c24', '角色权限管理', '新增', '/admin/group', '2018-02-12 23:26:50', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('38', 'ac88ceb386aa4231b09bf472cb937c24', '角色权限管理', '新增', '/admin/group', '2018-02-12 23:26:50', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('39', 'ac88ceb386aa4231b09bf472cb937c24', '角色权限管理', '新增', '/admin/group', '2018-02-12 23:26:50', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('40', 'ac88ceb386aa4231b09bf472cb937c24', '角色权限管理', '新增', '/admin/group', '2018-02-12 23:26:50', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('41', 'ac88ceb386aa4231b09bf472cb937c24', '角色权限管理', '新增', '/admin/group', '2018-02-12 23:26:50', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('42', 'ac88ceb386aa4231b09bf472cb937c24', '角色权限管理', '新增', '/admin/group', '2018-02-12 23:26:50', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('43', 'ac88ceb386aa4231b09bf472cb937c24', '角色权限管理', '新增', '/admin/group', '2018-02-12 23:26:50', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('44', 'ac88ceb386aa4231b09bf472cb937c24', '角色权限管理', '新增', '/admin/group', '2018-02-12 23:26:50', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('45', 'ac88ceb386aa4231b09bf472cb937c24', '角色权限管理', '新增', '/admin/group', '2018-02-12 23:26:50', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('46', 'ac88ceb386aa4231b09bf472cb937c24', '角色权限管理', '新增', '/admin/group', '2018-02-12 23:26:55', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('47', 'ac88ceb386aa4231b09bf472cb937c24', '角色权限管理', '新增', '/admin/group', '2018-02-12 23:26:58', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('48', 'ac88ceb386aa4231b09bf472cb937c24', '角色权限管理', '新增', '/admin/group', '2018-02-12 23:26:59', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('49', 'ac88ceb386aa4231b09bf472cb937c24', '角色权限管理', '新增', '/admin/group', '2018-02-12 23:26:59', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('50', 'ac88ceb386aa4231b09bf472cb937c24', '角色权限管理', '新增', '/admin/group', '2018-02-12 23:26:59', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('51', 'ac88ceb386aa4231b09bf472cb937c24', '角色权限管理', '新增', '/admin/group', '2018-02-12 23:26:59', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('52', 'ac88ceb386aa4231b09bf472cb937c24', '角色权限管理', '新增', '/admin/group', '2018-02-12 23:26:59', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('53', 'ac88ceb386aa4231b09bf472cb937c24', '角色权限管理', '新增', '/admin/group', '2018-02-12 23:26:59', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('54', 'ac88ceb386aa4231b09bf472cb937c24', '角色权限管理', '新增', '/admin/group', '2018-02-12 23:27:00', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('55', 'ac88ceb386aa4231b09bf472cb937c24', '角色权限管理', '新增', '/admin/group', '2018-02-12 23:27:00', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('56', 'ac88ceb386aa4231b09bf472cb937c24', '角色权限管理', '新增', '/admin/group', '2018-02-12 23:27:00', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('57', 'ac88ceb386aa4231b09bf472cb937c24', '角色权限管理', '新增', '/admin/group', '2018-02-12 23:27:00', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('58', 'ac88ceb386aa4231b09bf472cb937c24', '角色权限管理', '新增', '/admin/group', '2018-02-12 23:27:00', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('59', 'ac88ceb386aa4231b09bf472cb937c24', '角色权限管理', '新增', '/admin/group', '2018-02-12 23:27:00', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('60', 'ac88ceb386aa4231b09bf472cb937c24', '角色权限管理', '新增', '/admin/group', '2018-02-12 23:27:00', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('61', 'ac88ceb386aa4231b09bf472cb937c24', '角色权限管理', '新增', '/admin/group', '2018-02-12 23:27:01', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('62', 'ac88ceb386aa4231b09bf472cb937c24', '角色权限管理', '新增', '/admin/group', '2018-02-12 23:27:07', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('63', 'ac88ceb386aa4231b09bf472cb937c24', '角色权限管理', '新增', '/admin/group', '2018-02-12 23:27:07', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('64', 'ac88ceb386aa4231b09bf472cb937c24', '角色权限管理', '新增', '/admin/group', '2018-02-12 23:27:07', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('65', 'ac88ceb386aa4231b09bf472cb937c24', '角色权限管理', '新增', '/admin/group', '2018-02-12 23:27:07', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('66', 'ac88ceb386aa4231b09bf472cb937c24', '角色权限管理', '新增', '/admin/group', '2018-02-12 23:27:09', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('67', 'ac88ceb386aa4231b09bf472cb937c24', '角色权限管理', '新增', '/admin/group', '2018-02-12 23:27:09', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('68', 'ac88ceb386aa4231b09bf472cb937c24', '角色权限管理', '新增', '/admin/group', '2018-02-12 23:27:09', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('69', 'ac88ceb386aa4231b09bf472cb937c24', '角色权限管理', '新增', '/admin/group', '2018-02-12 23:27:09', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('70', 'ac88ceb386aa4231b09bf472cb937c24', '角色权限管理', '新增', '/admin/group', '2018-02-12 23:27:10', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('71', 'ac88ceb386aa4231b09bf472cb937c24', '角色权限管理', '新增', '/admin/group', '2018-02-12 23:27:10', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('72', 'ac88ceb386aa4231b09bf472cb937c24', '角色权限管理', '新增', '/admin/group', '2018-02-12 23:27:10', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('73', 'ac88ceb386aa4231b09bf472cb937c24', '角色权限管理', '新增', '/admin/group', '2018-02-12 23:27:10', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('74', 'ac88ceb386aa4231b09bf472cb937c24', '角色权限管理', '新增', '/admin/group', '2018-02-12 23:27:10', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('75', 'ac88ceb386aa4231b09bf472cb937c24', '角色权限管理', '新增', '/admin/group', '2018-02-12 23:27:10', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('76', 'ac88ceb386aa4231b09bf472cb937c24', '角色权限管理', '新增', '/admin/group', '2018-02-12 23:27:10', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('77', 'ac88ceb386aa4231b09bf472cb937c24', '角色权限管理', '新增', '/admin/group', '2018-02-12 23:27:13', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('78', 'ac88ceb386aa4231b09bf472cb937c24', '角色权限管理', '新增', '/admin/group', '2018-02-12 23:27:13', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('79', 'ac88ceb386aa4231b09bf472cb937c24', '角色权限管理', '新增', '/admin/group', '2018-02-12 23:27:13', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('80', 'ac88ceb386aa4231b09bf472cb937c24', '角色权限管理', '新增', '/admin/group', '2018-02-12 23:27:13', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('81', 'ac88ceb386aa4231b09bf472cb937c24', '角色权限管理', '新增', '/admin/group', '2018-02-12 23:27:13', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('82', 'ac88ceb386aa4231b09bf472cb937c24', '角色权限管理', '新增', '/admin/group', '2018-02-12 23:27:13', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('83', 'ac88ceb386aa4231b09bf472cb937c24', '角色权限管理', '新增', '/admin/group', '2018-02-12 23:27:13', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('84', 'ac88ceb386aa4231b09bf472cb937c24', '角色权限管理', '新增', '/admin/group', '2018-02-12 23:27:13', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('85', 'ac88ceb386aa4231b09bf472cb937c24', '角色权限管理', '新增', '/admin/group', '2018-02-12 23:27:13', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('86', 'ac88ceb386aa4231b09bf472cb937c24', '角色权限管理', '新增', '/admin/group', '2018-02-12 23:27:13', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('87', 'ac88ceb386aa4231b09bf472cb937c24', '角色权限管理', '新增', '/admin/group', '2018-02-12 23:27:13', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('88', 'ac88ceb386aa4231b09bf472cb937c24', '角色权限管理', '新增', '/admin/group', '2018-02-12 23:27:13', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('89', 'ac88ceb386aa4231b09bf472cb937c24', '角色权限管理', '新增', '/admin/group', '2018-02-12 23:27:14', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('90', 'ac88ceb386aa4231b09bf472cb937c24', '角色权限管理', '新增', '/admin/group', '2018-02-12 23:27:15', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('91', 'ac88ceb386aa4231b09bf472cb937c24', '角色权限管理', '新增', '/admin/group', '2018-02-12 23:34:56', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('92', 'ac88ceb386aa4231b09bf472cb937c24', '角色权限管理', '新增', '/admin/group', '2018-02-12 23:36:12', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('93', 'ac88ceb386aa4231b09bf472cb937c24', '角色权限管理', '新增', '/admin/group', '2018-02-12 23:38:15', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('94', 'ac88ceb386aa4231b09bf472cb937c24', '角色权限管理', '新增', '/admin/group', '2018-02-12 23:38:16', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('95', 'ac88ceb386aa4231b09bf472cb937c24', '角色权限管理', '新增', '/admin/group', '2018-02-12 23:38:18', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('96', '1ec08564dcc344018d6aaa910068f0f0', '部门管理', '新增部门值', '/admin/depart', '2018-02-12 23:38:29', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('97', '1ec08564dcc344018d6aaa910068f0f0', '部门管理', '编辑部门值', '/admin/depart', '2018-02-12 23:40:45', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('98', '1ec08564dcc344018d6aaa910068f0f0', '用户管理', '新增', '/admin/user', '2018-02-12 23:41:32', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('99', '1ec08564dcc344018d6aaa910068f0f0', '用户管理', '编辑', '/admin/user', '2018-02-12 23:54:06', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('100', '1ec08564dcc344018d6aaa910068f0f0', '用户管理', '编辑', '/admin/user', '2018-02-12 23:54:12', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('101', '1ec08564dcc344018d6aaa910068f0f0', '部门管理', '删除部门值', '/admin/depart', '2018-02-12 23:58:38', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('102', '1ec08564dcc344018d6aaa910068f0f0', '部门管理', '删除部门值', '/admin/depart', '2018-02-12 23:58:41', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('103', '1ec08564dcc344018d6aaa910068f0f0', '角色权限管理', '新增', '/admin/group', '2018-02-13 00:04:26', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('104', '1ec08564dcc344018d6aaa910068f0f0', '角色权限管理', '新增', '/admin/group', '2018-02-13 00:05:06', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('105', '1ec08564dcc344018d6aaa910068f0f0', '角色权限管理', '新增', '/admin/group', '2018-02-13 00:05:07', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('106', '1ec08564dcc344018d6aaa910068f0f0', '角色权限管理', '新增', '/admin/group', '2018-02-13 00:05:09', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('107', '1ec08564dcc344018d6aaa910068f0f0', '部门管理', '新增岗位', '/admin/position', '2018-02-13 00:05:28', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('108', '1ec08564dcc344018d6aaa910068f0f0', '部门管理', '新增岗位', '/admin/position', '2018-02-13 00:06:50', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('109', '1ec08564dcc344018d6aaa910068f0f0', '部门管理', '新增岗位', '/admin/position', '2018-02-13 00:07:28', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('110', '1ec08564dcc344018d6aaa910068f0f0', '部门管理', '新增岗位', '/admin/position', '2018-02-13 00:12:54', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('111', '1ec08564dcc344018d6aaa910068f0f0', '部门管理', '新增岗位', '/admin/position', '2018-02-13 00:14:18', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('112', '1ec08564dcc344018d6aaa910068f0f0', '部门管理', '新增岗位', '/admin/position', '2018-02-13 00:14:50', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('113', '1ec08564dcc344018d6aaa910068f0f0', '部门管理', '新增岗位', '/admin/position', '2018-02-13 00:15:46', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('114', '1ec08564dcc344018d6aaa910068f0f0', '部门管理', '分配岗位角色', '/admin/position/{*}/group', '2018-02-13 00:16:30', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('115', '1ec08564dcc344018d6aaa910068f0f0', '部门管理', '分配岗位人员', '/admin/position/{*}/user', '2018-02-13 00:17:36', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('116', '1ec08564dcc344018d6aaa910068f0f0', '部门管理', '分配岗位角色', '/admin/position/{*}/group', '2018-02-13 00:26:23', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('117', '1ec08564dcc344018d6aaa910068f0f0', '角色权限管理', '新增', '/admin/group', '2018-02-13 13:44:58', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('118', '1ec08564dcc344018d6aaa910068f0f0', '角色权限管理', '新增', '/admin/group', '2018-02-13 13:45:08', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('119', '1ec08564dcc344018d6aaa910068f0f0', '角色权限管理', '新增', '/admin/group', '2018-02-13 13:45:13', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('120', '1ec08564dcc344018d6aaa910068f0f0', '角色权限管理', '新增', '/admin/group', '2018-02-13 13:45:20', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('121', '1ec08564dcc344018d6aaa910068f0f0', '角色权限管理', '新增', '/admin/group', '2018-02-13 13:45:20', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('122', '1ec08564dcc344018d6aaa910068f0f0', '角色权限管理', '新增', '/admin/group', '2018-02-13 13:45:28', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('123', '1ec08564dcc344018d6aaa910068f0f0', '角色权限管理', '新增', '/admin/group', '2018-02-13 13:45:29', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('124', '1ec08564dcc344018d6aaa910068f0f0', '角色权限管理', '新增', '/admin/group', '2018-02-13 13:45:32', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('125', '1ec08564dcc344018d6aaa910068f0f0', '部门管理', '新增部门值', '/admin/depart', '2018-02-13 13:46:16', '59c28b8593c2472e99e54e3ed9b6efe8', '王小七', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('126', '1ec08564dcc344018d6aaa910068f0f0', '部门管理', '授权部门权限', '/admin/position/{*}/depart', '2018-02-13 13:58:08', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('127', '1ec08564dcc344018d6aaa910068f0f0', '部门管理', '新增部门值', '/admin/depart', '2018-02-13 14:20:02', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('128', '1ec08564dcc344018d6aaa910068f0f0', '部门管理', '授权部门权限', '/admin/position/{*}/depart', '2018-02-13 14:20:30', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('129', '1ec08564dcc344018d6aaa910068f0f0', '部门管理', '授权部门权限', '/admin/position/{*}/depart', '2018-02-13 14:21:38', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('130', '1ec08564dcc344018d6aaa910068f0f0', '部门管理', '授权部门权限', '/admin/position/{*}/depart', '2018-02-13 14:30:15', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('131', '1ec08564dcc344018d6aaa910068f0f0', '部门管理', '授权部门权限', '/admin/position/{*}/depart', '2018-02-13 14:30:25', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('132', '1ec08564dcc344018d6aaa910068f0f0', '部门管理', '授权部门权限', '/admin/position/{*}/depart', '2018-02-13 14:30:30', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('133', '1ec08564dcc344018d6aaa910068f0f0', '部门管理', '授权部门权限', '/admin/position/{*}/depart', '2018-02-13 14:30:35', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('134', '1ec08564dcc344018d6aaa910068f0f0', '部门管理', '授权部门权限', '/admin/position/{*}/depart', '2018-02-13 14:38:26', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('135', '1ec08564dcc344018d6aaa910068f0f0', '部门管理', '授权部门权限', '/admin/position/{*}/depart', '2018-02-13 14:38:41', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('136', '1ec08564dcc344018d6aaa910068f0f0', '部门管理', '授权部门权限', '/admin/position/{*}/depart', '2018-02-13 14:39:13', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('137', '1ec08564dcc344018d6aaa910068f0f0', '部门管理', '授权部门权限', '/admin/position/{*}/depart', '2018-02-13 14:40:17', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('138', '1ec08564dcc344018d6aaa910068f0f0', '部门管理', '授权部门权限', '/admin/position/{*}/depart', '2018-02-13 14:54:55', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('139', '1ec08564dcc344018d6aaa910068f0f0', '部门管理', '授权部门权限', '/admin/position/{*}/depart', '2018-02-13 14:55:46', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('140', '1ec08564dcc344018d6aaa910068f0f0', '部门管理', '授权部门权限', '/admin/position/{*}/depart', '2018-02-13 14:55:51', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('141', '1ec08564dcc344018d6aaa910068f0f0', '部门管理', '授权部门权限', '/admin/position/{*}/depart', '2018-02-13 14:55:55', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('142', '1ec08564dcc344018d6aaa910068f0f0', '部门管理', '授权部门权限', '/admin/position/{*}/depart', '2018-02-13 14:57:51', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('143', '1ec08564dcc344018d6aaa910068f0f0', '部门管理', '新增部门值', '/admin/depart', '2018-02-13 14:57:53', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('144', '1ec08564dcc344018d6aaa910068f0f0', '部门管理', '授权部门权限', '/admin/position/{*}/depart', '2018-02-13 15:39:45', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('145', '1ec08564dcc344018d6aaa910068f0f0', '部门管理', '新增部门值', '/admin/depart', '2018-02-13 15:40:08', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('146', '1ec08564dcc344018d6aaa910068f0f0', '部门管理', '新增部门值', '/admin/depart', '2018-02-13 15:40:09', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('147', '1ec08564dcc344018d6aaa910068f0f0', '部门管理', '新增部门值', '/admin/depart', '2018-02-13 15:40:10', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('148', '1ec08564dcc344018d6aaa910068f0f0', '部门管理', '新增部门值', '/admin/depart', '2018-02-13 15:40:10', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('149', '1ec08564dcc344018d6aaa910068f0f0', '部门管理', '授权部门权限', '/admin/position/{*}/depart', '2018-02-13 15:40:41', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('150', '1ec08564dcc344018d6aaa910068f0f0', '部门管理', '授权部门权限', '/admin/position/{*}/depart', '2018-02-13 15:41:04', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('151', '1ec08564dcc344018d6aaa910068f0f0', '部门管理', '授权部门权限', '/admin/position/{*}/depart', '2018-02-13 15:41:08', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('152', '1ec08564dcc344018d6aaa910068f0f0', '部门管理', '授权部门权限', '/admin/position/{*}/depart', '2018-02-13 16:06:30', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('153', '1ec08564dcc344018d6aaa910068f0f0', '部门管理', '授权部门权限', '/admin/position/{*}/depart', '2018-02-13 16:06:44', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('154', '1ec08564dcc344018d6aaa910068f0f0', '部门管理', '分配岗位角色', '/admin/position/{*}/group', '2018-02-13 16:18:25', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('155', 'ac88ceb386aa4231b09bf472cb937c24', '租户管理', '编辑租户', '/admin/tenant', '2018-02-15 09:58:38', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('156', '1ec08564dcc344018d6aaa910068f0f0', '部门管理', '分配岗位人员', '/admin/position/{*}/user', '2018-02-15 10:26:01', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('157', '1ec08564dcc344018d6aaa910068f0f0', '部门管理', '删除部门值', '/admin/depart', '2018-02-15 10:26:31', '59c28b8593c2472e99e54e3ed9b6efe8', '王小七', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('158', '1ec08564dcc344018d6aaa910068f0f0', '部门管理', '删除部门值', '/admin/depart', '2018-02-15 10:26:36', '59c28b8593c2472e99e54e3ed9b6efe8', '王小七', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('159', '1ec08564dcc344018d6aaa910068f0f0', '部门管理', '分配岗位人员', '/admin/position/{*}/user', '2018-02-15 10:29:54', 'a1a2cb17b4f24e50bbde2f1b8a233bcb', '测试租户', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('160', 'ac88ceb386aa4231b09bf472cb937c24', '租户管理', '编辑租户', '/admin/tenant', '2018-02-15 10:57:12', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('161', 'ac88ceb386aa4231b09bf472cb937c24', '部门管理', '新增岗位', '/admin/position', '2018-02-15 10:57:27', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('162', 'ac88ceb386aa4231b09bf472cb937c24', '用户管理', '编辑', '/admin/user', '2018-02-15 13:46:32', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('163', 'ac88ceb386aa4231b09bf472cb937c24', '角色权限管理', '新增', '/admin/group', '2018-02-15 13:46:44', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('164', 'ac88ceb386aa4231b09bf472cb937c24', '部门管理', '授权部门权限', '/admin/position/{*}/depart', '2018-02-15 13:47:20', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('165', 'ac88ceb386aa4231b09bf472cb937c24', '菜单管理', '新增', '/admin/menu', '2018-02-25 13:30:49', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('166', 'ac88ceb386aa4231b09bf472cb937c24', '菜单管理', '新增元素', '/admin/element', '2018-02-25 13:42:23', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('167', 'ac88ceb386aa4231b09bf472cb937c24', '菜单管理', '新增元素', '/admin/element', '2018-02-25 13:43:16', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('168', 'ac88ceb386aa4231b09bf472cb937c24', '菜单管理', '新增元素', '/admin/element', '2018-02-25 13:46:18', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('169', 'ac88ceb386aa4231b09bf472cb937c24', '菜单管理', '新增元素', '/admin/element', '2018-02-25 13:47:18', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('170', 'ac88ceb386aa4231b09bf472cb937c24', '菜单管理', '编辑元素', '/admin/element', '2018-02-25 13:51:39', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('171', 'ac88ceb386aa4231b09bf472cb937c24', '菜单管理', '编辑元素', '/admin/element', '2018-02-25 13:51:48', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('172', 'ac88ceb386aa4231b09bf472cb937c24', '菜单管理', '编辑元素', '/admin/element', '2018-02-25 13:51:55', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('173', 'ac88ceb386aa4231b09bf472cb937c24', '菜单管理', '编辑元素', '/admin/element', '2018-02-25 13:52:01', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('174', 'ac88ceb386aa4231b09bf472cb937c24', '网关路由管理', '编辑路由', '/auth/gatewayRoute', '2018-02-25 14:29:44', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('175', 'ac88ceb386aa4231b09bf472cb937c24', '网关路由管理', '编辑路由', '/auth/gatewayRoute', '2018-02-25 14:29:50', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('176', 'ac88ceb386aa4231b09bf472cb937c24', '网关路由管理', '编辑路由', '/auth/gatewayRoute', '2018-02-25 14:29:54', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('177', 'ac88ceb386aa4231b09bf472cb937c24', '网关路由管理', '禁用路由', '/auth/gatewayRoute', '2018-02-25 14:32:17', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('178', 'ac88ceb386aa4231b09bf472cb937c24', '网关路由管理', '新建路由', '/auth/gatewayRoute', '2018-02-25 14:33:29', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('179', 'ac88ceb386aa4231b09bf472cb937c24', '网关路由管理', '禁用路由', '/auth/gatewayRoute', '2018-02-25 14:36:33', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('180', 'ac88ceb386aa4231b09bf472cb937c24', '网关路由管理', '编辑路由', '/auth/gatewayRoute', '2018-02-25 14:38:31', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('181', 'ac88ceb386aa4231b09bf472cb937c24', '网关路由管理', '禁用路由', '/auth/gatewayRoute', '2018-02-25 14:40:37', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('182', 'ac88ceb386aa4231b09bf472cb937c24', '网关路由管理', '编辑路由', '/auth/gatewayRoute', '2018-02-25 14:41:07', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('183', 'ac88ceb386aa4231b09bf472cb937c24', '网关路由管理', '新建路由', '/auth/gatewayRoute', '2018-02-26 12:50:50', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('184', 'ac88ceb386aa4231b09bf472cb937c24', '用户管理', '编辑', '/admin/user', '2018-03-10 13:19:09', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('185', 'ac88ceb386aa4231b09bf472cb937c24', '用户管理', '编辑', '/admin/user', '2018-03-10 15:49:14', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('186', 'ac88ceb386aa4231b09bf472cb937c24', '用户管理', '编辑', '/admin/user', '2018-03-10 15:50:16', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('187', 'ac88ceb386aa4231b09bf472cb937c24', '用户管理', '新增', '/admin/user', '2018-03-11 09:15:58', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('188', 'ac88ceb386aa4231b09bf472cb937c24', '用户管理', '新增', '/admin/user', '2018-03-11 09:16:05', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('189', 'ac88ceb386aa4231b09bf472cb937c24', '用户管理', '新增', '/admin/user', '2018-03-11 09:16:13', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('190', 'ac88ceb386aa4231b09bf472cb937c24', '用户管理', '新增', '/admin/user', '2018-03-11 09:16:28', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('191', 'ac88ceb386aa4231b09bf472cb937c24', '用户管理', '新增', '/admin/user', '2018-03-11 09:21:04', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('192', 'ac88ceb386aa4231b09bf472cb937c24', '用户管理', '新增', '/admin/user', '2018-03-11 09:21:56', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('193', 'ac88ceb386aa4231b09bf472cb937c24', '用户管理', '新增', '/admin/user', '2018-03-11 09:22:47', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('194', 'ac88ceb386aa4231b09bf472cb937c24', '用户管理', '新增', '/admin/user', '2018-03-11 09:23:16', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('195', 'ac88ceb386aa4231b09bf472cb937c24', '用户管理', '新增', '/admin/user', '2018-03-11 09:23:52', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('196', 'ac88ceb386aa4231b09bf472cb937c24', '用户管理', '新增', '/admin/user', '2018-03-11 09:30:17', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('197', 'ac88ceb386aa4231b09bf472cb937c24', '用户管理', '新增', '/admin/user', '2018-03-11 09:33:01', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('198', 'ac88ceb386aa4231b09bf472cb937c24', '用户管理', '新增', '/admin/user', '2018-03-11 09:33:28', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('199', 'ac88ceb386aa4231b09bf472cb937c24', '用户管理', '新增', '/admin/user', '2018-03-11 09:34:48', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('200', 'ac88ceb386aa4231b09bf472cb937c24', '用户管理', '新增', '/admin/user', '2018-03-11 09:36:10', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('201', 'ac88ceb386aa4231b09bf472cb937c24', '服务权限管理', '编辑', '/auth/service', '2018-03-27 15:28:50', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('202', 'ac88ceb386aa4231b09bf472cb937c24', '服务权限管理', '编辑', '/auth/service', '2018-03-27 15:28:56', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('203', 'ac88ceb386aa4231b09bf472cb937c24', '用户管理', '新增', '/admin/user', '2018-03-28 20:11:04', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('204', 'ac88ceb386aa4231b09bf472cb937c24', '菜单管理', '新增元素', '/admin/element', '2018-03-28 20:19:07', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('205', 'ac88ceb386aa4231b09bf472cb937c24', '菜单管理', '新增元素', '/admin/element', '2018-03-28 20:19:54', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('206', 'ac88ceb386aa4231b09bf472cb937c24', '菜单管理', '新增元素', '/admin/element', '2018-03-28 20:20:22', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('207', 'ac88ceb386aa4231b09bf472cb937c24', '菜单管理', '新增元素', '/admin/element', '2018-03-28 20:20:47', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('208', 'ac88ceb386aa4231b09bf472cb937c24', '数据字典', '新增字典目录', '/dict/dictType', '2018-04-04 18:56:29', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('209', 'ac88ceb386aa4231b09bf472cb937c24', '数据字典', '新增字典值', '/dict/dictValue', '2018-04-04 18:57:26', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('210', 'ac88ceb386aa4231b09bf472cb937c24', '数据字典', '新增字典值', '/dict/dictValue', '2018-04-04 18:57:38', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('211', 'ac88ceb386aa4231b09bf472cb937c24', '用户管理', '新增', '/admin/user', '2018-04-04 19:43:50', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('212', 'ac88ceb386aa4231b09bf472cb937c24', '部门管理', '删除部门人员', '/admin/depart/user', '2018-04-04 19:44:52', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('213', 'ac88ceb386aa4231b09bf472cb937c24', '部门管理', '新增岗位', '/admin/position', '2018-08-22 10:34:53', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('214', 'ac88ceb386aa4231b09bf472cb937c24', '部门管理', '授权部门权限', '/admin/position/{*}/depart', '2018-08-22 10:35:38', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('215', 'ac88ceb386aa4231b09bf472cb937c24', '角色权限管理', '新增', '/admin/group', '2018-08-22 10:38:17', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('216', 'ac88ceb386aa4231b09bf472cb937c24', '部门管理', '分配岗位角色', '/admin/position/{*}/group', '2018-08-22 11:14:31', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('217', 'ac88ceb386aa4231b09bf472cb937c24', '服务权限管理', '新增', '/auth/service', '2018-08-22 11:30:38', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('218', 'ac88ceb386aa4231b09bf472cb937c24', '菜单管理', '新增', '/admin/menu', '2018-08-22 16:31:17', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('219', 'ac88ceb386aa4231b09bf472cb937c24', '菜单管理', '新增', '/admin/menu', '2018-08-22 16:31:40', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('220', 'ac88ceb386aa4231b09bf472cb937c24', '菜单管理', '删除', '/admin/menu/{*}', '2018-08-22 16:31:54', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('221', 'ac88ceb386aa4231b09bf472cb937c24', '菜单管理', '删除', '/admin/menu/{*}', '2018-08-22 16:31:57', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('222', 'ac88ceb386aa4231b09bf472cb937c24', '菜单管理', '新增', '/admin/menu', '2018-08-22 16:32:16', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('223', 'ac88ceb386aa4231b09bf472cb937c24', '菜单管理', '新增', '/admin/menu', '2018-08-22 16:32:22', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('224', 'ac88ceb386aa4231b09bf472cb937c24', '菜单管理', '新增', '/admin/menu', '2018-08-22 16:32:25', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('225', 'ac88ceb386aa4231b09bf472cb937c24', '菜单管理', '新增', '/admin/menu', '2018-08-22 16:32:33', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('226', 'ac88ceb386aa4231b09bf472cb937c24', '菜单管理', '新增', '/admin/menu', '2018-08-22 16:32:37', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('227', 'ac88ceb386aa4231b09bf472cb937c24', '菜单管理', '新增', '/admin/menu', '2018-08-22 16:32:56', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('228', 'ac88ceb386aa4231b09bf472cb937c24', '菜单管理', '新增', '/admin/menu', '2018-08-22 16:33:09', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('229', 'ac88ceb386aa4231b09bf472cb937c24', '菜单管理', '新增', '/admin/menu', '2018-08-22 16:34:03', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('230', 'ac88ceb386aa4231b09bf472cb937c24', '菜单管理', '新增', '/admin/menu', '2018-08-22 16:34:15', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('231', 'ac88ceb386aa4231b09bf472cb937c24', '菜单管理', '新增', '/admin/menu', '2018-08-22 16:34:26', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('232', 'ac88ceb386aa4231b09bf472cb937c24', '菜单管理', '新增', '/admin/menu', '2018-08-22 16:34:36', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('233', 'ac88ceb386aa4231b09bf472cb937c24', '菜单管理', '删除', '/admin/menu/{*}', '2018-08-22 16:35:11', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('234', 'ac88ceb386aa4231b09bf472cb937c24', '菜单管理', '删除', '/admin/menu/{*}', '2018-08-22 16:35:14', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('235', 'ac88ceb386aa4231b09bf472cb937c24', '菜单管理', '新增元素', '/admin/element', '2018-08-22 16:42:00', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('236', 'ac88ceb386aa4231b09bf472cb937c24', '菜单管理', '编辑元素', '/admin/element/{*}', '2018-08-22 16:43:20', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('237', 'ac88ceb386aa4231b09bf472cb937c24', '菜单管理', '删除元素', '/admin/element/{*}', '2018-08-22 16:43:41', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('238', 'ac88ceb386aa4231b09bf472cb937c24', '服务权限管理', '新增', '/auth/service', '2018-08-23 11:13:57', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('239', 'ac88ceb386aa4231b09bf472cb937c24', '服务权限管理', '新增', '/auth/service', '2018-08-23 11:14:37', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('240', 'ac88ceb386aa4231b09bf472cb937c24', '服务权限管理', '新增', '/auth/service', '2018-08-23 11:16:04', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('241', 'ac88ceb386aa4231b09bf472cb937c24', '服务权限管理', '新增', '/auth/service', '2018-08-23 11:17:19', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('242', 'ac88ceb386aa4231b09bf472cb937c24', '服务权限管理', '新增', '/auth/service', '2018-08-23 11:18:01', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('243', 'ac88ceb386aa4231b09bf472cb937c24', '网关路由管理', '新建路由', '/auth/gatewayRoute', '2018-08-23 11:57:58', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('244', 'ac88ceb386aa4231b09bf472cb937c24', '网关路由管理', '编辑路由', '/auth/gatewayRoute/{*}', '2018-08-23 14:52:32', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('245', 'ac88ceb386aa4231b09bf472cb937c24', '服务权限管理', '编辑', '/auth/service/{*}', '2018-08-23 14:52:58', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('246', 'ac88ceb386aa4231b09bf472cb937c24', '服务权限管理', '编辑', '/auth/service/{*}', '2018-08-27 14:48:30', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('247', 'ac88ceb386aa4231b09bf472cb937c24', '网关路由管理', '新建路由', '/auth/gatewayRoute', '2018-08-27 15:04:40', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('248', 'ac88ceb386aa4231b09bf472cb937c24', '客户端管理', '新增客户端', '/auth/oauthClientDetails', '2018-08-27 15:25:30', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('249', 'ac88ceb386aa4231b09bf472cb937c24', '租户管理', '新增租户', '/admin/tenant', '2018-08-29 09:24:43', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('250', 'ac88ceb386aa4231b09bf472cb937c24', '租户管理', '新增租户', '/admin/tenant', '2018-08-29 09:25:30', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('251', 'ac88ceb386aa4231b09bf472cb937c24', '租户管理', '新增租户', '/admin/tenant', '2018-08-29 09:26:16', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('252', 'ac88ceb386aa4231b09bf472cb937c24', '租户管理', '编辑租户', '/admin/tenant/{*}', '2018-08-29 09:26:26', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('253', 'ac88ceb386aa4231b09bf472cb937c24', '租户管理', '新增租户', '/admin/tenant', '2018-08-29 09:27:00', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('254', 'ac88ceb386aa4231b09bf472cb937c24', '租户管理', '编辑租户', '/admin/tenant/{*}', '2018-08-29 09:27:11', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('255', 'ac88ceb386aa4231b09bf472cb937c24', '菜单管理', '编辑', '/admin/menu/{*}', '2018-08-29 17:01:00', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('256', 'ac88ceb386aa4231b09bf472cb937c24', '菜单管理', '编辑', '/admin/menu/{*}', '2018-08-29 17:01:05', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('257', 'ac88ceb386aa4231b09bf472cb937c24', '菜单管理', '新增', '/admin/menu', '2018-08-29 17:02:28', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('258', 'ac88ceb386aa4231b09bf472cb937c24', '菜单管理', '删除', '/admin/menu/{*}', '2018-08-29 17:02:35', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('259', 'ac88ceb386aa4231b09bf472cb937c24', '菜单管理', '新增', '/admin/menu', '2018-08-29 17:02:51', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('260', 'ac88ceb386aa4231b09bf472cb937c24', '菜单管理', '新增', '/admin/menu', '2018-08-29 17:03:07', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('261', 'ac88ceb386aa4231b09bf472cb937c24', '菜单管理', '删除', '/admin/menu/{*}', '2018-08-29 17:03:30', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('262', 'ac88ceb386aa4231b09bf472cb937c24', '菜单管理', '新增', '/admin/menu', '2018-08-29 17:31:16', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('263', 'ac88ceb386aa4231b09bf472cb937c24', '菜单管理', '新增', '/admin/menu', '2018-08-29 17:31:57', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('264', 'ac88ceb386aa4231b09bf472cb937c24', '菜单管理', '删除', '/admin/menu/{*}', '2018-08-29 17:32:04', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('265', 'ac88ceb386aa4231b09bf472cb937c24', '菜单管理', '删除', '/admin/menu/{*}', '2018-08-29 17:32:07', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('266', 'ac88ceb386aa4231b09bf472cb937c24', '网关路由管理', '新建路由', '/auth/gatewayRoute', '2018-08-29 17:50:33', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('267', 'ac88ceb386aa4231b09bf472cb937c24', '网关路由管理', '新建路由', '/auth/gatewayRoute', '2018-08-30 15:08:17', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('268', 'ac88ceb386aa4231b09bf472cb937c24', '服务权限管理', '新增', '/auth/service', '2018-08-30 15:20:01', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('269', 'ac88ceb386aa4231b09bf472cb937c24', '服务权限管理', '删除', '/auth/service/{*}', '2018-08-30 15:20:26', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('270', 'ac88ceb386aa4231b09bf472cb937c24', '服务权限管理', '新增', '/auth/service', '2018-08-30 16:11:20', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('271', 'ac88ceb386aa4231b09bf472cb937c24', '网关路由管理', '编辑路由', '/auth/gatewayRoute/{*}', '2018-09-21 18:50:15', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('272', 'ac88ceb386aa4231b09bf472cb937c24', '服务权限管理', '删除', '/auth/service/{*}', '2019-07-02 20:00:48', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('273', 'ac88ceb386aa4231b09bf472cb937c24', '服务权限管理', '删除', '/auth/service/{*}', '2019-07-02 20:00:51', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('274', 'ac88ceb386aa4231b09bf472cb937c24', '服务权限管理', '删除', '/auth/service/{*}', '2019-07-02 20:00:55', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('275', 'ac88ceb386aa4231b09bf472cb937c24', '服务权限管理', '删除', '/auth/service/{*}', '2019-07-02 20:00:59', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('276', 'ac88ceb386aa4231b09bf472cb937c24', '服务权限管理', '删除', '/auth/service/{*}', '2019-07-02 20:01:04', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('277', 'ac88ceb386aa4231b09bf472cb937c24', '服务权限管理', '删除', '/auth/service/{*}', '2019-07-02 20:01:13', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('278', 'ac88ceb386aa4231b09bf472cb937c24', '服务权限管理', '删除', '/auth/service/{*}', '2019-07-02 20:01:23', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('279', 'ac88ceb386aa4231b09bf472cb937c24', '服务权限管理', '删除', '/auth/service/{*}', '2019-07-02 20:01:26', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('280', 'ac88ceb386aa4231b09bf472cb937c24', '服务权限管理', '删除', '/auth/service/{*}', '2019-07-02 20:01:31', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('281', 'ac88ceb386aa4231b09bf472cb937c24', '服务权限管理', '删除', '/auth/service/{*}', '2019-07-02 20:01:35', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('282', 'ac88ceb386aa4231b09bf472cb937c24', '网关路由管理', '新建路由', '/auth/gatewayRoute', '2019-07-02 20:41:17', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('283', 'ac88ceb386aa4231b09bf472cb937c24', '菜单管理', '新增', '/admin/menu', '2019-07-03 09:54:48', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('284', 'ac88ceb386aa4231b09bf472cb937c24', '菜单管理', '新增', '/admin/menu', '2019-07-03 09:55:38', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('285', 'ac88ceb386aa4231b09bf472cb937c24', '菜单管理', '新增', '/admin/menu', '2019-07-03 09:57:52', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('286', 'ac88ceb386aa4231b09bf472cb937c24', '角色权限管理', '分配菜单', '/admin/group/{*}/authority/menu', '2019-07-03 10:05:32', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('287', 'ac88ceb386aa4231b09bf472cb937c24', '菜单管理', '新增', '/admin/menu', '2019-07-04 09:46:33', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('288', 'ac88ceb386aa4231b09bf472cb937c24', '菜单管理', '新增', '/admin/menu', '2019-07-04 11:34:32', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('289', 'ac88ceb386aa4231b09bf472cb937c24', '菜单管理', '新增', '/admin/menu', '2019-07-04 11:35:30', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('290', 'ac88ceb386aa4231b09bf472cb937c24', '菜单管理', '新增', '/admin/menu', '2019-07-04 11:36:28', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('291', 'ac88ceb386aa4231b09bf472cb937c24', '菜单管理', '新增', '/admin/menu', '2019-07-12 09:55:38', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('292', 'ac88ceb386aa4231b09bf472cb937c24', '菜单管理', '编辑', '/admin/menu/{*}', '2019-07-12 09:57:11', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('293', 'ac88ceb386aa4231b09bf472cb937c24', '菜单管理', '编辑', '/admin/menu/{*}', '2019-07-12 09:57:16', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('294', 'ac88ceb386aa4231b09bf472cb937c24', '菜单管理', '编辑', '/admin/menu/{*}', '2019-07-12 09:57:22', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('295', 'ac88ceb386aa4231b09bf472cb937c24', '菜单管理', '编辑', '/admin/menu/{*}', '2019-07-12 09:57:28', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('296', 'ac88ceb386aa4231b09bf472cb937c24', '部门管理', '新增部门值', '/admin/depart', '2019-07-12 12:00:47', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('297', 'ac88ceb386aa4231b09bf472cb937c24', '部门管理', '新增部门值', '/admin/depart', '2019-07-12 13:52:26', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('298', 'ac88ceb386aa4231b09bf472cb937c24', '部门管理', '新增部门值', '/admin/depart', '2019-07-12 13:52:29', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('299', 'ac88ceb386aa4231b09bf472cb937c24', '部门管理', '新增部门值', '/admin/depart', '2019-07-12 13:56:46', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('300', 'ac88ceb386aa4231b09bf472cb937c24', '部门管理', '新增部门值', '/admin/depart', '2019-07-12 13:56:48', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('301', 'ac88ceb386aa4231b09bf472cb937c24', '部门管理', '新增部门值', '/admin/depart', '2019-07-12 15:01:29', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('302', 'ac88ceb386aa4231b09bf472cb937c24', '部门管理', '新增部门值', '/admin/depart', '2019-07-12 15:04:46', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('303', 'ac88ceb386aa4231b09bf472cb937c24', '部门管理', '删除部门值', '/admin/depart/{*}', '2019-07-12 16:09:05', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('304', 'ac88ceb386aa4231b09bf472cb937c24', '部门管理', '删除部门值', '/admin/depart/{*}', '2019-07-12 16:09:07', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('305', 'ac88ceb386aa4231b09bf472cb937c24', '部门管理', '删除部门值', '/admin/depart/{*}', '2019-07-12 16:09:08', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('306', 'ac88ceb386aa4231b09bf472cb937c24', '部门管理', '删除部门值', '/admin/depart/{*}', '2019-07-12 16:09:10', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('307', 'ac88ceb386aa4231b09bf472cb937c24', '部门管理', '删除部门值', '/admin/depart/{*}', '2019-07-12 16:09:11', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('308', 'ac88ceb386aa4231b09bf472cb937c24', '部门管理', '删除部门值', '/admin/depart/{*}', '2019-07-12 16:09:13', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('309', 'ac88ceb386aa4231b09bf472cb937c24', '部门管理', '删除部门值', '/admin/depart/{*}', '2019-07-12 16:09:14', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('310', 'ac88ceb386aa4231b09bf472cb937c24', '菜单管理', '新增', '/admin/menu', '2019-07-15 13:52:18', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('311', 'ac88ceb386aa4231b09bf472cb937c24', '用户管理', '新增', '/admin/user', '2019-07-16 12:46:28', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('312', 'ac88ceb386aa4231b09bf472cb937c24', '菜单管理', '编辑', '/admin/menu/{*}', '2019-08-02 09:56:14', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('313', 'ac88ceb386aa4231b09bf472cb937c24', '菜单管理', '新增', '/admin/menu', '2019-08-02 10:05:09', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('314', 'ac88ceb386aa4231b09bf472cb937c24', '菜单管理', '新增', '/admin/menu', '2019-08-02 10:05:32', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('315', 'ac88ceb386aa4231b09bf472cb937c24', '菜单管理', '编辑', '/admin/menu/{*}', '2019-08-02 10:05:50', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('316', 'ac88ceb386aa4231b09bf472cb937c24', '菜单管理', '编辑', '/admin/menu/{*}', '2019-08-02 10:05:54', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('317', 'ac88ceb386aa4231b09bf472cb937c24', '服务权限管理', '新增', '/auth/service', '2019-08-02 11:07:09', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('318', 'ac88ceb386aa4231b09bf472cb937c24', '网关路由管理', '新建路由', '/auth/gatewayRoute', '2019-08-02 11:11:06', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('319', 'ac88ceb386aa4231b09bf472cb937c24', '服务权限管理', '删除', '/auth/service/{*}', '2019-08-08 13:44:44', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('320', 'ac88ceb386aa4231b09bf472cb937c24', '服务权限管理', '删除', '/auth/service/{*}', '2019-08-08 13:44:48', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('321', 'ac88ceb386aa4231b09bf472cb937c24', '服务权限管理', '新增', '/auth/service', '2019-08-08 13:45:46', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('322', 'ac88ceb386aa4231b09bf472cb937c24', '服务权限管理', '编辑', '/auth/service/{*}', '2019-08-08 13:46:37', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('323', 'ac88ceb386aa4231b09bf472cb937c24', '服务权限管理', '新增', '/auth/service', '2019-08-08 13:47:02', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('324', 'ac88ceb386aa4231b09bf472cb937c24', '服务权限管理', '新增', '/auth/service', '2019-08-08 13:47:33', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('325', 'ac88ceb386aa4231b09bf472cb937c24', '服务权限管理', '新增', '/auth/service', '2019-08-08 13:48:04', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('326', 'ac88ceb386aa4231b09bf472cb937c24', '服务权限管理', '新增', '/auth/service', '2019-08-08 13:48:27', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('327', 'ac88ceb386aa4231b09bf472cb937c24', '服务权限管理', '新增', '/auth/service', '2019-08-08 13:48:55', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('328', 'ac88ceb386aa4231b09bf472cb937c24', '服务权限管理', '新增', '/auth/service', '2019-08-08 13:49:14', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('329', 'ac88ceb386aa4231b09bf472cb937c24', '服务权限管理', '新增', '/auth/service', '2019-08-08 13:49:35', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('330', 'ac88ceb386aa4231b09bf472cb937c24', '服务权限管理', '新增', '/auth/service', '2019-08-08 13:49:56', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('331', 'ac88ceb386aa4231b09bf472cb937c24', '服务权限管理', '新增', '/auth/service', '2019-08-08 13:50:10', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('332', 'ac88ceb386aa4231b09bf472cb937c24', '服务权限管理', '编辑', '/auth/service/{*}', '2019-08-08 13:58:58', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('333', 'ac88ceb386aa4231b09bf472cb937c24', '服务权限管理', '删除', '/auth/service/{*}', '2019-08-08 14:03:08', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('334', 'ac88ceb386aa4231b09bf472cb937c24', '网关路由管理', '新建路由', '/auth/gatewayRoute', '2019-08-08 14:09:21', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('335', 'ac88ceb386aa4231b09bf472cb937c24', '网关路由管理', '编辑路由', '/auth/gatewayRoute/{*}', '2019-08-08 14:09:33', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('336', 'ac88ceb386aa4231b09bf472cb937c24', '网关路由管理', '新建路由', '/auth/gatewayRoute', '2019-08-08 14:10:06', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('337', 'ac88ceb386aa4231b09bf472cb937c24', '服务权限管理', '新增', '/auth/service', '2019-08-08 15:04:56', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('338', 'ac88ceb386aa4231b09bf472cb937c24', '服务权限管理', '新增', '/auth/service', '2019-08-08 15:44:58', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('339', 'ac88ceb386aa4231b09bf472cb937c24', '网关路由管理', '新建路由', '/auth/gatewayRoute', '2019-08-08 15:45:45', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('340', 'ac88ceb386aa4231b09bf472cb937c24', '网关路由管理', '新建路由', '/auth/gatewayRoute', '2019-08-08 15:46:31', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('341', 'ac88ceb386aa4231b09bf472cb937c24', '网关路由管理', '新建路由', '/auth/gatewayRoute', '2019-08-08 15:46:59', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('342', 'ac88ceb386aa4231b09bf472cb937c24', '网关路由管理', '新建路由', '/auth/gatewayRoute', '2019-08-08 15:47:30', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('343', 'ac88ceb386aa4231b09bf472cb937c24', '网关路由管理', '新建路由', '/auth/gatewayRoute', '2019-08-08 15:47:52', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('344', 'ac88ceb386aa4231b09bf472cb937c24', '网关路由管理', '新建路由', '/auth/gatewayRoute', '2019-08-08 15:48:13', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('345', 'ac88ceb386aa4231b09bf472cb937c24', '网关路由管理', '编辑路由', '/auth/gatewayRoute/{*}', '2019-08-08 15:48:35', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('346', 'ac88ceb386aa4231b09bf472cb937c24', '网关路由管理', '编辑路由', '/auth/gatewayRoute/{*}', '2019-08-08 15:48:41', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('347', 'ac88ceb386aa4231b09bf472cb937c24', '网关路由管理', '新建路由', '/auth/gatewayRoute', '2019-08-08 15:49:28', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('348', 'ac88ceb386aa4231b09bf472cb937c24', '网关路由管理', '新建路由', '/auth/gatewayRoute', '2019-08-08 15:49:51', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('349', 'ac88ceb386aa4231b09bf472cb937c24', '服务权限管理', '新增', '/auth/service', '2019-08-29 17:06:24', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('350', 'ac88ceb386aa4231b09bf472cb937c24', '网关路由管理', '新建路由', '/auth/gatewayRoute', '2019-08-29 17:07:11', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('351', 'ac88ceb386aa4231b09bf472cb937c24', '网关路由管理', '新建路由', '/auth/gatewayRoute', '2019-09-02 11:02:54', '1', 'Mr.AG', '127.0.0.1');
INSERT INTO `gate_log` VALUES ('352', 'ac88ceb386aa4231b09bf472cb937c24', '服务权限管理', '新增', '/auth/service', '2019-09-02 11:03:26', '1', 'Mr.AG', '127.0.0.1');

-- ----------------------------
-- Table structure for oauth_client_details
-- ----------------------------
DROP TABLE IF EXISTS `oauth_client_details`;
CREATE TABLE `oauth_client_details` (
  `client_id` varchar(100) NOT NULL,
  `resource_ids` varchar(256) DEFAULT NULL,
  `client_secret` varchar(256) DEFAULT NULL,
  `scope` varchar(256) DEFAULT NULL,
  `authorized_grant_types` varchar(256) DEFAULT NULL,
  `web_server_redirect_uri` varchar(256) DEFAULT NULL,
  `authorities` varchar(256) DEFAULT NULL,
  `access_token_validity` int(11) DEFAULT NULL,
  `refresh_token_validity` int(11) DEFAULT NULL,
  `additional_information` varchar(4096) DEFAULT NULL,
  `autoapprove` varchar(256) DEFAULT NULL,
  `description` varchar(256) DEFAULT NULL,
  `crt_user_name` varchar(255) DEFAULT NULL,
  `crt_user_id` varchar(36) DEFAULT NULL,
  `crt_time` datetime DEFAULT NULL,
  `upd_user_name` varchar(255) DEFAULT NULL,
  `upd_user_id` varchar(36) DEFAULT NULL,
  `upd_time` datetime DEFAULT NULL,
  `is_deleted` char(1) DEFAULT NULL,
  `is_disabled` char(1) DEFAULT NULL,
  PRIMARY KEY (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of oauth_client_details
-- ----------------------------
INSERT INTO `oauth_client_details` VALUES ('app', null, '$2a$10$WyTe8zdZMYmlxYua/kjTWO4QIKRVjtWv7uafuvVhBA0ML.Qe72.Pe', 'app', 'password,refresh_token', 'http://localhost:9527/#/', null, '2', '2592000', null, 'true', null, 'Mr.AG', '1', '2018-08-27 15:25:30', 'Mr.AG', '1', '2018-08-27 15:25:30', null, null);
INSERT INTO `oauth_client_details` VALUES ('client', '', '$2a$10$mWd26766PyVBuQ10bZLul.uPm4R1n/DsHRCcQVZatS1XCv6OllcE2', 'read', 'password,refresh_token,authorization_code', 'http://localhost:4040/sso/login', null, '3600', '2592000', '{}', 'true', null, null, null, null, null, null, null, null, null);
INSERT INTO `oauth_client_details` VALUES ('vue', null, '$2a$10$poEvPgiBh3iuyfBdqUaYhOT.j4z8QG5bNRL96ZmHZHhbbeRZN8OAO', 'read', 'password,refresh_token', 'http://localhost:9527/#/', null, '14400', '2592000', '{}', 'true', '', null, null, null, 'Mr.AG', '1', '2018-03-28 20:43:14', null, null);
