# salt-cloud

#### 介绍
新一代微服务，多租户，快速开发框架

#### 软件架构
软件架构说明

cloud-auth: 认证中心，提供用户账号、服务模块调用统一授权

cloud-auth-client: 认证客户端jar，所有业务模块均需引用此jar，方可接入框架进行模块间相互调用授权。

cloud-auth-server:独立运行微服务模块。提供授权访问服务，利用Oauth2.0主要提供系统用户的授权服务。

cloud-base:基础jar 封装一些基本操作工具类

cloud-common:微服务业务模块通用jar，提供数据库操作，基础Controller等服务。

cloud-core:核心模块 提供其他模块的基础支持

cloud-merge:跨服务的数据聚合功能。可利用此模块进行跨服务的数据聚合查询服务。举个例子：业务模块引用数据字典服务模块的性别，sex 0男 1女。 业务模块存储的为key. 可通过@MergeField(key = "comm_sex", feign = DictFeign.class, method = "getDictValues") 完成数据的一次性查询到方法体内

cloud-control:控制中心包括 注册中心、流量卫兵的运行jar

cloud-nacos:nacos启动程序，打开bin文件夹，直接运行startip的cmd或者sh脚本即可运行。默认端口8848.

cloud-sentinel:流量卫兵，可提供微服务的熔断、流量限流等服务。

cloud-gate:网关中心，提供路由转发和安全拦截。

cloud-generator:代码生成器，框架配套代码生成器

cloud-modules:框架微服务模块集合

cloud-admin:提供基础功能管理，包括用户、角色、权限、微服务的定义、路由设置、访问授权管理。

cloud-dict:数据字典服务

cloud-security:前端用户访问授权服务，支持用户名密码、手机短信、第三方账号(微信、QQ、微博等）的访问，派发Token。

cloud-tool:工具服务，提供常用的一些工具服务，如文件存储（本地存储、阿里云、七牛、腾讯云）、短信发送(阿里云）、极光推送、微信公众号SDK等。


#### 使用说明

启动顺序： 

注册中心(nacos) -认证中心(cloud-auth)-基础模块（cloud-admin)-网关（cloud-gate)-其他业务模块

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
