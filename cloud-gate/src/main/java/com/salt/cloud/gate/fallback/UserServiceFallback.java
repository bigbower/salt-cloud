package com.salt.cloud.gate.fallback;

import com.salt.cloud.core.vo.PermissionInfo;
import com.salt.cloud.gate.feign.IUserFeign;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

/**
 * @author kian
 * @create 2018/3/7.
 */
@Service
@Slf4j
public class UserServiceFallback implements IUserFeign {

    @Override
    public List<PermissionInfo> getPermissionByUserId(@PathVariable("username") String username) {
        log.error("调用{}异常{}","getPermissionByUsername",username);
        return null;
    }

    @Override
    public List<PermissionInfo> getAllPermissionInfo() {
        log.error("调用{}异常","getPermissionByUsername");
        return null;
    }

}
