package com.salt.cloud.gate.route;

import com.alibaba.fastjson.JSON;
import com.salt.cloud.core.constants.RedisKeyConstants;
import com.salt.cloud.gate.vo.GatewayRouteVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.FilterDefinition;
import org.springframework.cloud.gateway.handler.predicate.PredicateDefinition;
import org.springframework.cloud.gateway.route.RouteDefinition;
import org.springframework.cloud.gateway.route.RouteDefinitionRepository;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@Slf4j
public class RedisRouteDefinitionRepository implements RouteDefinitionRepository {
    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    private static final String routePrefix = "lb://";


    /*
    * 获取网关路由信息
    *
    * args 可以翻看源码获取key,也可以使用_genkey_0 _genkey_1 这种形式
    * */
    @Override
    public Flux<RouteDefinition> getRouteDefinitions() {
        List<RouteDefinition> routeDefinitions = new ArrayList<>();
        List<GatewayRouteVO> results = JSON.parseArray(redisTemplate.opsForValue().get(RedisKeyConstants.ZUUL_ROUTE_KEY), GatewayRouteVO.class);
        log.info("加载Redis缓存路由信息...");
        if (results!=null) {
            for (GatewayRouteVO result : results) {
                if (!result.getEnabled()) {
                    continue;
                }
                if (StringUtils.isEmpty(result.getPath())) {
                    continue;
                }
                if (StringUtils.isEmpty(result.getServiceId()) && StringUtils.isEmpty(result.getUrl())) {
                    continue;
                }
                RouteDefinition routeDefinition = new RouteDefinition();
                routeDefinition.setId(result.getServiceId());
                routeDefinition.setUri(URI.create(routePrefix + result.getServiceId()));

                //设置断言
                List<PredicateDefinition> pdList = new ArrayList<>();
                PredicateDefinition predicate = new PredicateDefinition();
                Map<String, String> predicateParams = new HashMap<>(8);
                predicateParams.put("pattern", result.getPath());
                predicate.setName("Path");
                predicate.setArgs(predicateParams);
                pdList.add(predicate);

                //设置过滤器
                List<FilterDefinition> filters = new ArrayList();
                FilterDefinition filterDefinition=new FilterDefinition();
                Map<String, String> filterDefinitionParams = new HashMap<>(8);
                filterDefinitionParams.put("parts","2");
                filterDefinition.setName("StripPrefix");
                filterDefinition.setArgs(filterDefinitionParams);
                filters.add(filterDefinition);




                routeDefinition.setPredicates(pdList);
                routeDefinition.setFilters(filters);
                routeDefinitions.add(routeDefinition);

            }
            log.info("加载Redis缓存路由信息完成");
        }else{
            log.info("未获得Redis缓存路由信息");
        }
        return Flux.fromIterable(routeDefinitions);
    }

    @Override
    public Mono<Void> save(Mono<RouteDefinition> route) {
        return null;
    }

    @Override
    public Mono<Void> delete(Mono<String> routeId) {
        return null;
    }
}





