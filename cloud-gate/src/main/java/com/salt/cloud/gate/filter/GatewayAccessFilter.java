package com.salt.cloud.gate.filter;

import com.alibaba.fastjson.JSONObject;
import com.salt.cloud.auth.client.config.ServiceAuthConfig;
import com.salt.cloud.auth.client.config.UserAuthConfig;
import com.salt.cloud.auth.client.jwt.ServiceAuthUtil;
import com.salt.cloud.auth.client.jwt.UserAuthUtil;
import com.salt.cloud.core.constants.RequestHeaderConstants;
import com.salt.cloud.core.context.BaseContextHandler;
import com.salt.cloud.core.msg.BaseResponse;
import com.salt.cloud.core.msg.auth.TokenForbiddenResponse;
import com.salt.cloud.core.util.jwt.IJWTInfo;
import com.salt.cloud.core.vo.LogInfo;
import com.salt.cloud.core.vo.PermissionInfo;
import com.salt.cloud.gate.feign.ILogFeign;
import com.salt.cloud.gate.feign.IUserFeign;
import com.salt.cloud.gate.handler.RequestBodyRoutePredicateFactory;
import com.salt.cloud.gate.utils.DBLog;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.cloud.gateway.support.ServerWebExchangeUtils;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Configuration
@Slf4j
public class GatewayAccessFilter implements GlobalFilter {

    @Autowired
    @Lazy
    private IUserFeign userService;

    @Autowired
    @Lazy
    private ILogFeign logService;



    @Value("${gate.ignore.startWith}")
    private String startWith;

    private static final String GATE_WAY_PREFIX = "/api";
    @Autowired
    private UserAuthUtil userAuthUtil;

    @Autowired
    private ServiceAuthConfig serviceAuthConfig;

    @Autowired
    private UserAuthConfig userAuthConfig;

    @Autowired
    private ServiceAuthUtil serviceAuthUtil;

    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        log.info("检查用户token和用户权限....");
        LinkedHashSet requiredAttribute = exchange.getRequiredAttribute(ServerWebExchangeUtils.GATEWAY_ORIGINAL_REQUEST_URL_ATTR);
        ServerHttpRequest request = exchange.getRequest();
        String requestUri = request.getPath().pathWithinApplication().value();
        if (requiredAttribute != null) {
            Iterator<URI> iterator = requiredAttribute.iterator();
            while (iterator.hasNext()){
                URI next = iterator.next();
                if(next.getPath().startsWith(GATE_WAY_PREFIX)){
                    requestUri = next.getPath().substring(GATE_WAY_PREFIX.length());
                }
            }
        }
        final String method = request.getMethod().toString();
        BaseContextHandler.setToken(null);
        ServerHttpRequest.Builder mutate = request.mutate();
        // 判断地址是否在拦截白名单外
        if (!isStartWith(requestUri)) {

            IJWTInfo user = null;
            try {
                user = getJWTUser(request, mutate);
            } catch (Exception e) {
                log.error("Token异常", e);
                return getVoidMono(exchange, new TokenForbiddenResponse(e.getMessage()));
            }


            List<PermissionInfo> permissionIfs = userService.getAllPermissionInfo();
            // 判断资源是否启用权限约束
            Stream<PermissionInfo> stream = getPermissionIfs(requestUri, method, permissionIfs);
            List<PermissionInfo> result = stream.collect(Collectors.toList());
            PermissionInfo[] permissions = result.toArray(new PermissionInfo[]{});
            if (permissions.length > 0) {
                if (checkUserPermission(permissions, exchange, user)) {
                    return getVoidMono(exchange, new TokenForbiddenResponse("User Forbidden!Does not has Permission!"));
                }
            }
        }

        //申请客户端密钥头,并将客户端验证的token放在header里面
        mutate.header(serviceAuthConfig.getTokenHeader(),new String[]{serviceAuthUtil.getClientToken()});

        ServerHttpRequest build = mutate.build();
        return chain.filter(exchange.mutate().request(build).build());
    }

    /**
     * 网关抛异常
     *
     * @param body
     */
//    @NotNull
    private Mono<Void> getVoidMono(ServerWebExchange exchange, BaseResponse body) {
        exchange.getResponse().setStatusCode(HttpStatus.OK);
        //exchange.getResponse().getHeaders().setContentType().("application/json;charset=UTF-8");
        byte[] bytes = JSONObject.toJSONString(body).getBytes(StandardCharsets.UTF_8);
        DataBuffer buffer = exchange.getResponse().bufferFactory().wrap(bytes);
        return exchange.getResponse().writeWith(Flux.just(buffer));
    }


    /**
     * 获取目标权限资源
     *
     * @param requestUri
     * @param method
     * @param serviceInfo
     * @return
     */
    private Stream<PermissionInfo> getPermissionIfs(final String requestUri, final String method, List<PermissionInfo> serviceInfo) {
        return serviceInfo.parallelStream().filter(permissionInfo -> {
            String uri = permissionInfo.getUri();
            if (uri.indexOf("{") > 0) {
                uri = uri.replaceAll("\\{\\*\\}", "[a-zA-Z\\\\d]+");
            }
            String regEx = "^" + uri + "$";
            return (Pattern.compile(regEx).matcher(requestUri).find())
                    && method.equals(permissionInfo.getMethod());
        });
    }

    private void setCurrentUserInfoAndLog(ServerWebExchange exchange, IJWTInfo user, PermissionInfo pm) {
        String host = exchange.getRequest().getRemoteAddress().toString();
        LogInfo logInfo = new LogInfo(pm.getMenu(), pm.getName(), pm.getUri(), new Date(), user.getId(), user.getName(), host, String.valueOf(exchange.getAttributes().get(RequestBodyRoutePredicateFactory.REQUEST_BODY_ATTR)));
        DBLog.getInstance().setLogService(logService).offerQueue(logInfo);
    }

    /**
     * 返回session中的用户信息
     *
     * @param request
     * @param ctx
     * @return
     */
    private IJWTInfo getJWTUser(ServerHttpRequest request, ServerHttpRequest.Builder ctx) throws Exception {
        List<String> strings = request.getHeaders().get(userAuthConfig.getTokenHeader());
        String authToken = null;
        if (strings != null) {
            authToken = strings.get(0);
        }
        if (StringUtils.isBlank(authToken)) {
            strings = request.getQueryParams().get("token");
            if (strings != null) {
                authToken = strings.get(0);
            }
        }
        ctx.header(userAuthConfig.getTokenHeader(), new String[]{authToken});
        BaseContextHandler.setToken(authToken);
        return userAuthUtil.getInfoFromToken(authToken.substring(RequestHeaderConstants.JWT_TOKEN_TYPE.length()));
    }


    private boolean checkUserPermission(PermissionInfo[] permissions, ServerWebExchange ctx, IJWTInfo user) {
        List<PermissionInfo> permissionInfos = userService.getPermissionByUserId(user.getId());
        PermissionInfo current = null;
        for (PermissionInfo info : permissions) {
            boolean anyMatch = permissionInfos.parallelStream().anyMatch(permissionInfo -> permissionInfo.getCode().equals(info.getCode()));
            if (anyMatch) {
                current = info;
                break;
            }
        }
        if (current == null) {
            return true;
        } else {
            if (!RequestMethod.GET.toString().equals(current.getMethod())) {
                setCurrentUserInfoAndLog(ctx, user, current);
            }
            return false;
        }
    }


    /**
     * URI是否以什么打头
     *
     * @param requestUri
     * @return
     */
    private boolean isStartWith(String requestUri) {
        boolean flag = false;
        for (String s : startWith.split(",")) {
            if (requestUri.startsWith(s)) {
                return true;
            }
        }
        return flag;
    }

    /**
     * 网关抛异常
     *
     * @param body
     * @param code
     */
    private Mono<Void> setFailedRequest(ServerWebExchange exchange, String body, int code) {
        exchange.getResponse().setStatusCode(HttpStatus.OK);
        return exchange.getResponse().setComplete();
    }
}
