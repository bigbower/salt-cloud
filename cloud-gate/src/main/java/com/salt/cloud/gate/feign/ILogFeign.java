package com.salt.cloud.gate.feign;

import com.salt.cloud.core.vo.LogInfo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * ${DESCRIPTION}
 *
 * @author kian
 * @version 2017-07-01 15:16
 */
@FeignClient("cloud-admin")
public interface ILogFeign {
  /**
   * 接口调用日志
   * @return
   */
  @RequestMapping(value="/api/log/save",method = RequestMethod.POST)
  void saveLog(LogInfo info);
}
