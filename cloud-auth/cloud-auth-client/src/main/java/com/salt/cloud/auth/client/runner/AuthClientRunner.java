package com.salt.cloud.auth.client.runner;

import com.salt.cloud.auth.client.config.ServiceAuthConfig;
import com.salt.cloud.auth.client.config.UserAuthConfig;
import com.salt.cloud.auth.client.feign.ServiceAuthFeign;
import com.salt.cloud.core.msg.BaseResponse;
import com.salt.cloud.core.msg.ObjectRestResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;

/**
 * 监听完成时触发
 *
 * @author kian
 * @version 2017/11/29.
 */
@Configuration
@Slf4j
public class AuthClientRunner implements CommandLineRunner {
    @Autowired
    private ServiceAuthConfig serviceAuthConfig;
    @Autowired
    private UserAuthConfig userAuthConfig;
    @Autowired
    private ServiceAuthFeign serviceAuthFeign;

    @Override
    public void run(String... args) throws Exception {
        refreshUserPubKey();
        refreshServicePubKey();
    }
//    @Scheduled(cron = "0 0/1 * * * ?")
    public void refreshUserPubKey(){
        try {
            BaseResponse resp = serviceAuthFeign.getUserPublicKey(serviceAuthConfig.getClientId(), serviceAuthConfig.getClientSecret());
            if (resp.getStatus() == HttpStatus.OK.value()) {
                ObjectRestResponse<byte[]> userResponse = (ObjectRestResponse<byte[]>) resp;
                this.userAuthConfig.setPubKeyByte(userResponse.getData());
                log.info("完成初始化加载用户端认证pubKey...");
            }
        }catch (Exception e){
            log.error("初始化加载用户端认证pubKey失败,请检查auth服务是否正常启动.",e.getMessage());
        }

    }
//    @Scheduled(cron = "0 0/1 * * * ?")
    public void refreshServicePubKey(){
        try {
            BaseResponse resp = serviceAuthFeign.getServicePublicKey(serviceAuthConfig.getClientId(), serviceAuthConfig.getClientSecret());
            if (resp.getStatus() == HttpStatus.OK.value()) {
                ObjectRestResponse<byte[]> userResponse = (ObjectRestResponse<byte[]>) resp;
                this.serviceAuthConfig.setPubKeyByte(userResponse.getData());
                log.info("完成初始化加载服务端认证pubKey...");
            }
        }catch (Exception e){
            log.error("初始化加载服务端认证失败,请检查auth服务是否正常启动.",e.getMessage());
        }
    }
}
