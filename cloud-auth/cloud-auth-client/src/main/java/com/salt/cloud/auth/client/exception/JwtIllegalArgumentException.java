package com.salt.cloud.auth.client.exception;

import com.salt.cloud.core.constants.RestCodeConstants;
import com.salt.cloud.core.exception.BaseException;

/**
 * Created by ace on 2017/9/15.
 */
public class JwtIllegalArgumentException extends BaseException {
    public JwtIllegalArgumentException(String s) {
        super(s, RestCodeConstants.EX_JWT_ILLEGAL_CODE);
    }
}
