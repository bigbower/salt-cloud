package com.salt.cloud.auth.client.annotation;

import com.salt.cloud.core.context.BaseContextHandler;
import com.salt.cloud.core.msg.ObjectRestResponse;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.concurrent.TimeUnit;


/**
 * @功能描述 aop解析注解
 * @author www.gaozz.club
 * @date 2018-11-02
 */
@Aspect
@Component
public class NoRepeatSubmitAop {

    private Log logger = LogFactory.getLog(getClass());

    @Autowired
    private RedisTemplate<String, Integer> template;

    @Around("execution(* com.salt.cloud..*Controller.*(..)) && @annotation(nrs)")
    public ObjectRestResponse arround(ProceedingJoinPoint pjp, NoRepeatSubmit nrs) {
        ValueOperations<String, Integer> opsForValue = template.opsForValue();
        try {
           ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder
                    .getRequestAttributes();
//            String sessionId = RequestContextHolder.getRequestAttributes().getSessionId();
           HttpServletRequest request = attributes.getRequest();
            String userid=BaseContextHandler.getUserID();
            String key = userid + "-" + request.getServletPath();
            if (opsForValue.get(key) == null) {// 如果缓存中有这个url视为重复提交
                Object o = pjp.proceed();
                opsForValue.set(key, 0, 2, TimeUnit.SECONDS);
                return ObjectRestResponse.ok(o);
            } else {
                logger.error("重复提交");
                return ObjectRestResponse.error("重复提交");
            }
        } catch (Throwable e) {
            e.printStackTrace();
            logger.error("验证重复提交时出现未知异常!");
            //return "{\"code\":-889,\"message\":\"验证重复提交时出现未知异常!\"}";
            return ObjectRestResponse.error("验证重复提交时出现未知异常!");
        }

    }
}


