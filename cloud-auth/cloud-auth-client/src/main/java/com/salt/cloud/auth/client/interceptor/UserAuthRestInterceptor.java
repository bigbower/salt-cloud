package com.salt.cloud.auth.client.interceptor;

import com.alibaba.fastjson.JSON;
import com.salt.cloud.auth.client.annotation.CheckUserToken;
import com.salt.cloud.auth.client.annotation.IgnoreUserToken;
import com.salt.cloud.auth.client.config.UserAuthConfig;
import com.salt.cloud.auth.client.jwt.UserAuthUtil;
import com.salt.cloud.core.constants.CommonConstants;
import com.salt.cloud.core.constants.RequestHeaderConstants;
import com.salt.cloud.core.context.BaseContextHandler;
import com.salt.cloud.core.exception.auth.NonLoginException;
import com.salt.cloud.core.msg.BaseResponse;
import com.salt.cloud.core.util.jwt.IJWTInfo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 用户token拦截认证
 *
 * @author kian
 * @version 2017/9/10
 */
@Slf4j
public class UserAuthRestInterceptor extends HandlerInterceptorAdapter {
    @Autowired
    private UserAuthUtil userAuthUtil;

    @Autowired
    private UserAuthConfig userAuthConfig;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String method = request.getMethod();
        if (HttpMethod.OPTIONS.matches(method)) {
            return super.preHandle(request, response, handler);
        }
        HandlerMethod handlerMethod = (HandlerMethod) handler;
        // 配置该注解，说明不进行用户拦截。class级别定义
        CheckUserToken annotation = handlerMethod.getBeanType().getAnnotation(CheckUserToken.class);

        //配置忽略用户身份验证注解，仅限在method级别定义
        IgnoreUserToken ignoreUserToken = handlerMethod.getMethodAnnotation(IgnoreUserToken.class);
        //如果CheckUserToken注解未配置在类上，则找对应的方法上有无该注解
        if (annotation == null) {
            annotation = handlerMethod.getMethodAnnotation(CheckUserToken.class);
        }
        //如果CheckUserToken注解未配置在类上,或者方法上有IgnoreUserToken
        if (annotation == null || ignoreUserToken != null) {
            return super.preHandle(request, response, handler);
        }
        //CheckUserToken注解在类、方法上，并且没有IgnoreUserToken
        else {
            try {
                setBaseContextHandlerUser(request);
            } catch (NonLoginException ex) {
                response.setStatus(HttpStatus.UNAUTHORIZED.value());
                log.error(ex.getMessage(), ex);
                response.setContentType("application/json;charset=UTF-8");
                response.getWriter().write(JSON.toJSONString(new BaseResponse(ex.getStatus(), ex.getMessage())));
                return false;
            }

            return super.preHandle(request, response, handler);
        }
    }

    //设置上下文菜单里的用户信息
    private void setBaseContextHandlerUser(HttpServletRequest request) throws Exception {
        String token = request.getHeader(userAuthConfig.getTokenHeader());
        if (StringUtils.isEmpty(token)) {
            if (request.getCookies() != null) {
                for (Cookie cookie : request.getCookies()) {
                    if (cookie.getName().equals(userAuthConfig.getTokenHeader())) {
                        token = cookie.getValue();
                    }
                }
            }
        }

        if (token != null && token.startsWith(RequestHeaderConstants.JWT_TOKEN_TYPE)) {
            token = token.replaceAll("%20", " ");
            token = token.substring(RequestHeaderConstants.JWT_TOKEN_TYPE.length());
        }

        String tenantID = BaseContextHandler.getTenantID();
        IJWTInfo infoFromToken = userAuthUtil.getInfoFromToken(token);
        BaseContextHandler.setToken(token);
        BaseContextHandler.setUsername(infoFromToken.getUniqueName());
        BaseContextHandler.setName(infoFromToken.getName());
        BaseContextHandler.setUserID(infoFromToken.getId());
        BaseContextHandler.setDepartID(infoFromToken.getOtherInfo().get(CommonConstants.JWT_KEY_DEPART_ID));
        String userTenantId = infoFromToken.getOtherInfo().get(CommonConstants.JWT_KEY_TENANT_ID);
        if (StringUtils.isNoneBlank(tenantID)) {
            if (!tenantID.equals(userTenantId)) {
                throw new NonLoginException("用户不合法!");
            }
        }
        BaseContextHandler.setTenantID(userTenantId);


    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        BaseContextHandler.remove();
        super.afterCompletion(request, response, handler, ex);
    }
}
