package com.salt.cloud.auth.client.annotation;

import java.lang.annotation.*;


/**
 * @功能描述 防止重复提交标记注解
 * @author www.srping.tsh
 * @date 2018-08-26
 */
@Target(ElementType.METHOD) // 作用到方法上
@Retention(RetentionPolicy.RUNTIME) // 运行时有效
@Inherited
@Documented
public @interface NoRepeatSubmit {
}
