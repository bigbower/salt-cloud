

package com.salt.cloud.auth.client.interceptor;

import com.salt.cloud.auth.client.annotation.CheckClientToken;
import com.salt.cloud.auth.client.annotation.IgnoreClientToken;
import com.salt.cloud.auth.client.config.ServiceAuthConfig;
import com.salt.cloud.auth.client.jwt.ServiceAuthUtil;
import com.salt.cloud.core.exception.auth.ClientForbiddenException;
import com.salt.cloud.core.util.jwt.IJWTInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 服务token认证
 *
 * @author kian
 * @version 2017/9/12
 */
@SuppressWarnings("ALL")
@Slf4j
public class ServiceAuthRestInterceptor extends HandlerInterceptorAdapter {

    @Autowired
    private ServiceAuthUtil serviceAuthUtil;

    @Autowired
    private ServiceAuthConfig serviceAuthConfig;

    private List<String> allowedClient;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String method = request.getMethod();
        if (HttpMethod.OPTIONS.matches(method)){
            return super.preHandle(request, response, handler);
        }
        HandlerMethod handlerMethod = (HandlerMethod) handler;
        // 配置该注解，说明不进行服务拦截
        CheckClientToken annotation = handlerMethod.getBeanType().getAnnotation(CheckClientToken.class);
        IgnoreClientToken ignoreClientToken = handlerMethod.getMethodAnnotation(IgnoreClientToken.class);
        if (annotation == null) {
            annotation = handlerMethod.getMethodAnnotation(CheckClientToken.class);
        }
        if (annotation == null || ignoreClientToken != null) {
            log.info("检测到IgnoreClientToken注解,不进行拦截...");
            return super.preHandle(request, response, handler);
        } else {
            log.info("检测到CheckClientToken注解,进行拦截...");
            String token = request.getHeader(serviceAuthConfig.getTokenHeader());


            IJWTInfo infoFromToken = serviceAuthUtil.getInfoFromToken(token);
            String uniqueName = infoFromToken.getUniqueName();
            if( serviceAuthUtil.getAllowedClient().contains(uniqueName)){
                return super.preHandle(request, response, handler);
            }
            throw new ClientForbiddenException("服务端访问未授权，请配置");

//            try {
//                IJWTInfo infoFromToken = serviceAuthUtil.getInfoFromToken(token);
//                String uniqueName = infoFromToken.getUniqueName();
//                if( serviceAuthUtil.getAllowedClient().contains(uniqueName)){
//                    return super.preHandle(request, response, handler);
//                }
////                for (String client : serviceAuthUtil.getAllowedClient()) {
////                    if (client.equals(uniqueName)) {
////                        return super.preHandle(request, response, handler);
////                    }
////                }
//            }catch(ClientTokenException ex){
//                response.setStatus(HttpStatus.FORBIDDEN.value());
//                log.error(ex.getMessage(),ex);
//                response.setContentType("UTF-8");
//                response.getOutputStream().println(JSON.toJSONString(new BaseResponse(ex.getStatus(), ex.getMessage())));
//                return false;
//            }
//            response.setStatus(HttpStatus.FORBIDDEN.value());
//            response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
//            response.getOutputStream().println(JSON.toJSONString(new BaseResponse(RestCodeConstants.EX_CLIENT_FORBIDDEN_CODE,"Client is Forbidden!")));
//            return false;
        }
    }
}
