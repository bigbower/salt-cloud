package com.salt.cloud.auth.client.exception;

import com.salt.cloud.core.constants.RestCodeConstants;
import com.salt.cloud.core.exception.BaseException;

/**
 *
 * @author kian
 * @version 2017/9/15
 */
public class JwtSignatureException extends BaseException {
    public JwtSignatureException(String s) {
        super(s, RestCodeConstants.EX_JWT_INVALID_CODE);
    }
}
