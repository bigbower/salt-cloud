package com.salt.cloud.auth.client.jwt;

import com.alibaba.fastjson.JSONObject;
import com.salt.cloud.auth.client.config.UserAuthConfig;
import com.salt.cloud.core.exception.auth.NonLoginException;
import com.salt.cloud.core.util.RedisKeyUtil;
import com.salt.cloud.core.util.jwt.IJWTInfo;
import com.salt.cloud.core.util.jwt.JWTHelper;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureException;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.Date;

/**
 * Created by ace on 2017/9/15.
 */
@Configuration
@Slf4j
public class UserAuthUtil {

    @Autowired
    private UserAuthConfig userAuthConfig;


    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    public IJWTInfo getInfoFromToken(String token)  {
        try {
            IJWTInfo infoFromToken = JWTHelper.getInfoFromToken(token, userAuthConfig.getPubKeyByte());
            log.debug("解析后的JWT信息为："+JSONObject.toJSONString(infoFromToken));
            //判断Redis中是否存在该Token，存在则判断是否过期。不存在直接禁止登录
            if (redisTemplate.hasKey(RedisKeyUtil.buildUserAbleKey(infoFromToken.getUniqueName(), infoFromToken.getOtherInfo().get("client_id"),infoFromToken.getExpireTime())))
            {
                if (new DateTime(infoFromToken.getExpireTime()).plusMinutes(userAuthConfig.getTokenLimitExpire()).isBeforeNow()) {
                    redisTemplate.delete(RedisKeyUtil.buildUserAbleKey(infoFromToken.getUniqueName(), infoFromToken.getOtherInfo().get("client_id"), infoFromToken.getExpireTime()));
                    throw new NonLoginException("Token已过期");
                }
                return infoFromToken;
            }else{
                throw new NonLoginException("Token无效");
            }


        } catch (ExpiredJwtException ex) {
            throw new NonLoginException("Token已过期");
        }catch (MalformedJwtException ex){
            throw new NonLoginException("Token不能解析");
        } catch (SignatureException ex) {
            throw new NonLoginException("Token签名错误");
        } catch (IllegalArgumentException ex) {
            throw new NonLoginException("Token不存在");
        }catch(Exception ex){
            throw new NonLoginException(ex.getMessage());
        }
    }

    public static void main(String[] args) {
        DateTime expireTime=new DateTime(new Long("1575598093181"));
        DateTime nowTime=DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss").parseDateTime("2019-12-5 11:30:30");
        DateTime beginTime= expireTime.plusMinutes(1440);

        System.out.print("过期时间："+expireTime.toString("yyyy-MM-dd HH:mm:ss")+"\n");
        System.out.print("Token派发时间："+beginTime.toString("yyyy-MM-dd HH:mm:ss")+"\n");
        System.out.print("当前时间："+new DateTime(new Date()).toString("yyyy-MM-dd HH:mm:ss")+"\n");
        System.out.print("是否过期："+beginTime.isBeforeNow()+"\n");
    }
}
