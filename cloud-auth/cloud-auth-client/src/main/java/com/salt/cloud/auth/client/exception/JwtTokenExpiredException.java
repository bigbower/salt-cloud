

package com.salt.cloud.auth.client.exception;

import com.salt.cloud.core.constants.RestCodeConstants;
import com.salt.cloud.core.exception.BaseException;

/**
 * Created by ace on 2017/9/15.
 */
public class JwtTokenExpiredException extends BaseException {
    public JwtTokenExpiredException(String s) {
        super(s, RestCodeConstants.EX_JWT_EXPIRE_CODE);
    }
}
