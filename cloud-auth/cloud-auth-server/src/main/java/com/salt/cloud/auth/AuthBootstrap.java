package com.salt.cloud.auth;


import com.spring4all.swagger.EnableSwagger2Doc;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.web.bind.annotation.SessionAttributes;
import tk.mybatis.spring.annotation.MapperScan;

/**
 * Created by Ace on 2017/6/2.
 */
@SpringBootApplication(scanBasePackages = {"com.salt.cloud.security","com.salt.cloud.auth"})
@EnableDiscoveryClient
@EnableFeignClients
@MapperScan("com.salt.cloud.auth.module.*.mapper")
@EnableSwagger2Doc
@SessionAttributes("authorizationRequest")
@EnableResourceServer
public class AuthBootstrap {
    public static void main(String[] args) {
        SpringApplication.run(AuthBootstrap.class, args);
    }
}


