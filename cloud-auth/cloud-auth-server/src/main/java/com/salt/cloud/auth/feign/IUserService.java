package com.salt.cloud.auth.feign;

import com.salt.cloud.core.msg.ObjectRestResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;


/**
 * ${DESCRIPTION}
 *
 * @author kian
 * @version 2017-06-21 8:11
 */
//,configuration = FeignConfiguration.class
@FeignClient(value = "${jwt.user-service}")
public interface IUserService {
  /**
   * 通过账户\密码的方式登陆
   * @param username
   * @param password
   * @return
     */
  @RequestMapping(value = "/user/validate", method = RequestMethod.POST)
  public ObjectRestResponse<Map<String,String>> validate(@RequestParam("username") String username, @RequestParam("password") String password);
  @RequestMapping(value = "/user/info", method = RequestMethod.POST)
  public ObjectRestResponse<Map<String,String>> getUserInfoByUsername(@RequestParam("username") String username);
}
