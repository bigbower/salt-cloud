//package com.salt.cloud.auth.module.oauth.config;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
//import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
//
//import javax.servlet.http.HttpServletResponse;
//
///**
// * @author kian
// * @create 2018/3/21.
// */
//@Configuration
//public class ResourceServerConfig extends ResourceServerConfigurerAdapter {
//
////    @Autowired
////    private CustomAccessDeniedHandler customAccessDeniedHandler;
//    @Override
//    public void configure(HttpSecurity http) throws Exception {
//        http.formLogin().loginPage("/login").permitAll()
//                .and()
//                .authorizeRequests().antMatchers("/static/**", "/favicon.ico", "/webjars/**","/client/**","/v2/api-docs","/generator/build")
//                .permitAll()
//                .and()
//                .authorizeRequests()
//                .anyRequest().authenticated()
//
//        ;
//    }
//
////    @Override
////    public void configure(ResourceServerSecurityConfigurer resources) throws Exception {
////
////        //配置自定义异常解析
////        resources.authenticationEntryPoint(new AuthExceptionEntryPoint())
////                .accessDeniedHandler(customAccessDeniedHandler);
////    }
//}