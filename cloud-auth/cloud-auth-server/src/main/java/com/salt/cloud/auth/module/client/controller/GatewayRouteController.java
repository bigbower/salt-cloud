

package com.salt.cloud.auth.module.client.controller;

import com.salt.cloud.auth.module.client.biz.GatewayRouteBiz;
import com.salt.cloud.auth.module.client.entity.GatewayRoute;
import com.salt.cloud.common.rest.BaseController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("gatewayRoute")
public class GatewayRouteController extends BaseController<GatewayRouteBiz, GatewayRoute,String> {

}