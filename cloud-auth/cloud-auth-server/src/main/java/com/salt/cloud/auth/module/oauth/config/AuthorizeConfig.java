package com.salt.cloud.auth.module.oauth.config;

import com.salt.cloud.security.core.authorize.AuthorizeConfigProvider;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.stereotype.Component;

@Component
public class AuthorizeConfig implements AuthorizeConfigProvider {

    @Override
    public boolean config(ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry config) {
        //设置路由 不需要用户登录
        config.antMatchers("/v2/api-docs").permitAll()
                .antMatchers("/swagger-resources/**").permitAll()
                .antMatchers("/client/**").permitAll()
                .antMatchers("/gatewayRoute/**").permitAll()
                .antMatchers("/service/**").permitAll()
                .antMatchers("/oauthClientDetails/**").permitAll()
                .antMatchers("/jwt/**").permitAll()
                .antMatchers("/oauth/**").permitAll()
                .antMatchers("/code/**").permitAll()
                .antMatchers("/generator/build").permitAll()
                .antMatchers("/login").permitAll()
                .antMatchers("/error").permitAll();

         return false;
    }
}
