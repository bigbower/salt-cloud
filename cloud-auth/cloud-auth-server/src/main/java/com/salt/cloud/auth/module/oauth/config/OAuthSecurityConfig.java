//package com.salt.cloud.auth.module.oauth.config;
//
//import com.salt.cloud.auth.configuration.KeyConfiguration;
//import com.salt.cloud.auth.jwt.user.JwtTokenUtil;
//import com.salt.cloud.auth.module.oauth.bean.OauthUser;
//import com.salt.cloud.auth.module.oauth.service.OauthUserDetailsService;
//import CommonConstants;
//import RedisKeyConstants;
//import RsaKeyHelper;
//import lombok.extern.slf4j.Slf4j;
//import org.joda.time.DateTime;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.data.redis.connection.RedisConnectionFactory;
//import org.springframework.data.redis.core.RedisTemplate;
//import org.springframework.security.authentication.AuthenticationManager;
//import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
//import org.springframework.security.oauth2.common.OAuth2AccessToken;
//import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
//import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
//import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
//import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
//import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
//import org.springframework.security.oauth2.provider.OAuth2Authentication;
//import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
//import org.springframework.security.oauth2.provider.token.store.redis.RedisTokenStore;
//
//import javax.sql.DataSource;
//import java.io.IOException;
//import java.security.InvalidKeyException;
//import java.security.KeyPair;
//import java.security.NoSuchAlgorithmException;
//import java.util.Date;
//import java.util.HashMap;
//import java.util.Map;
//
//@Configuration
//@EnableAuthorizationServer
//@Slf4j
//public class OAuthSecurityConfig extends AuthorizationServerConfigurerAdapter {
//
//    @Autowired
//    private AuthenticationManager auth;
//
//    @Autowired
//    private DataSource dataSource;
//
//    @Autowired
//    private RedisTemplate<String, String> redisTemplate;
//
//    @Autowired
//    private JwtTokenUtil jwtTokenUtil;
//    @Autowired
//    private KeyConfiguration keyConfiguration;
//    @Autowired
//    private RedisConnectionFactory redisConnectionFactory;
//
//    @Autowired
//    private OauthUserDetailsService oauthUserDetailsService;
//
//    @Bean
//    public RedisTokenStore redisTokenStore() {
//        RedisTokenStore redisTokenStore = new RedisTokenStore(redisConnectionFactory);
//        redisTokenStore.setPrefix("AG:OAUTH:");
//        return redisTokenStore;
//    }
//
//    @Override
//    public void configure(AuthorizationServerSecurityConfigurer security) {
//        security.tokenKeyAccess("permitAll()");
//        security.checkTokenAccess("isAuthenticated()");
//
////        //如果需要更换加密模式,则设置此处，默认为BCryptPasswordEncoder
////        security.passwordEncoder(NoOpPasswordEncoder.getInstance());
//    }
//
//    @Override
//    public void configure(AuthorizationServerEndpointsConfigurer endpoints)
//            throws Exception {
//        endpoints
//                .authenticationManager(auth)
//                .userDetailsService(oauthUserDetailsService)//若无，refresh_token会有UserDetailsService is required错误
//                .tokenStore(redisTokenStore()).accessTokenConverter(accessTokenConverter())
//        ;
//
////        //配置自己的错误信息解释器
////        endpoints.exceptionTranslator(customWebResponseExceptionTranslator);
//    }
//
//    @Override
//    public void configure(ClientDetailsServiceConfigurer clients)
//            throws Exception {
//        //设置客户端为数据库存储模式
//        clients.jdbc(dataSource);
//    }
//
////    @Configuration
////    @Order(100)
////    protected static class AuthenticationManagerConfiguration extends GlobalAuthenticationConfigurerAdapter {
////        @Autowired
////        private OauthUserDetailsService oauthUserDetailsService;
////
////        @Override
////        public void init(AuthenticationManagerBuilder auth) throws Exception {
////            auth.userDetailsService(oauthUserDetailsService).passwordEncoder(new Sha256PasswordEncoder());
////        }
////    }
//
//
//    @Bean
//    public JwtAccessTokenConverter accessTokenConverter() throws IOException, InvalidKeyException, NoSuchAlgorithmException {
//        byte[] pri, pub = null;
//        try {
//            pri = RsaKeyHelper.toBytes(redisTemplate.opsForValue().get(RedisKeyConstants.REDIS_USER_PRI_KEY).toString());
//            pub = RsaKeyHelper.toBytes(redisTemplate.opsForValue().get(RedisKeyConstants.REDIS_USER_PUB_KEY).toString());
//        } catch (Exception e) {
//            Map<String, byte[]> keyMap = RsaKeyHelper.generateKey(keyConfiguration.getUserSecret());
//            redisTemplate.opsForValue().set(RedisKeyConstants.REDIS_USER_PRI_KEY, RsaKeyHelper.toHexString(keyMap.get("pri")));
//            redisTemplate.opsForValue().set(RedisKeyConstants.REDIS_USER_PUB_KEY, RsaKeyHelper.toHexString(keyMap.get("pub")));
//            pri = keyMap.get("pri");
//            pub = keyMap.get("pub");
//        }
//        JwtAccessTokenConverter accessTokenConverter = new JwtAccessTokenConverter() {
//            /***
//             * 重写增强token方法,用于自定义一些token返回的信息
//             */
//            @Override
//            public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
//                OauthUser user = (OauthUser) authentication.getUserAuthentication().getPrincipal();// 与登录时候放进去的UserDetail实现类一直查看link{SecurityConfiguration}
//                /** 自定义一些token属性 ***/
//                final Map<String, Object> additionalInformation = new HashMap<>();
//                Date expireTime = DateTime.now().plusSeconds(jwtTokenUtil.getExpire()).toDate();
//                additionalInformation.put(CommonConstants.JWT_KEY_EXPIRE, expireTime);
//                additionalInformation.put(CommonConstants.JWT_KEY_USER_ID, user.getId());
//                additionalInformation.put(CommonConstants.JWT_KEY_TENANT_ID, user.getTenantId());
//                additionalInformation.put(CommonConstants.JWT_KEY_DEPART_ID, user.getDepartId());
//                additionalInformation.put(CommonConstants.JWT_KEY_NAME, user.getName());
//                additionalInformation.put("sub", user.getUsername());
//                ((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(additionalInformation);
//                OAuth2AccessToken enhancedToken = super.enhance(accessToken, authentication);
//                return enhancedToken;
//            }
//
//        };
//        try {
//            accessTokenConverter.setKeyPair(new KeyPair(RsaKeyHelper.getPublicKey(pub), RsaKeyHelper.getPrivateKey(pri)));
//        } catch (Exception e) {
//            log.error("初始化用户认证公钥/密钥异常...", e);
//            throw new RuntimeException("Auth端设置jwt的Converter异常...错误消息:" + e.getMessage());
//        }
//        return accessTokenConverter;
//    }
//}