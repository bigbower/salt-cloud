package com.salt.cloud.auth.module.client.mapper;

import com.salt.cloud.auth.module.client.entity.GatewayRoute;
import com.salt.cloud.common.mapper.CommonMapper;

/**
 * 
 * 
 * @author Mr.AG
 * @email 463540703@qq.com
 * @version 2018-02-25 12:04:28
 */
public interface GatewayRouteMapper extends CommonMapper<GatewayRoute> {
	
}
