/**
 * 
 */
package com.salt.cloud.auth.module.oauth.service;

import com.salt.cloud.security.core.authentication.social.userdetail.SocialUserDetailsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;


/**
 * @author zhailiang
 *
 */
@Component
@Slf4j
public class DefaultSocialUserDetailsService implements SocialUserDetailsService {


//	@Autowired
//	private IUserService userService;

	@Override
	public UserDetails loadUserByUsername(String providerId, String openId) throws UsernameNotFoundException {
		//        if (StringUtils.isBlank(username)) {
//            throw new UsernameNotFoundException("用户名为空");
//        }
//
//        ObjectRestResponse<Map<String, String>> response = userService.getUserInfoByUsername(username);
//        Map<String, String> data = response.getData();
//        if (StringUtils.isEmpty(data.get("id"))) {
//            throw new UsernameNotFoundException("用户名不合法");
//        }
//
//        return new OauthUserDetails(data.get("id"),data.get("username"),data.get("name"), data.get("departId"),data.get("tenantId"), data.get("password"),
//                true, true, true, true,
//                AuthorityUtils.commaSeparatedStringToAuthorityList("ROLE_USER"));

		return null;
	}





}
