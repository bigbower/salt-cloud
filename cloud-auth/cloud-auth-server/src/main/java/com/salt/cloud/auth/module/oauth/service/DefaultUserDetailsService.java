/**
 * 
 */
package com.salt.cloud.auth.module.oauth.service;

import com.salt.cloud.auth.feign.IUserService;
import com.salt.cloud.core.msg.ObjectRestResponse;
import com.salt.cloud.security.core.bean.OauthUserDetails;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.Map;


/**
 * @author zhailiang
 *
 */
@Component
@Slf4j
public class DefaultUserDetailsService implements UserDetailsService {

	@Autowired
	private IUserService userService;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		if (StringUtils.isBlank(username)) {
			throw new UsernameNotFoundException("用户名为空");
		}
		ObjectRestResponse<Map<String, String>> response = userService.getUserInfoByUsername(username);
		Map<String, String> data = response.getData();
		if (StringUtils.isEmpty(data.get("id"))) {
			throw new UsernameNotFoundException("用户名不合法");
		}
		boolean accountNonLocked=true;
		if (!StringUtils.isEmpty(data.get("status"))) {
			accountNonLocked=data.get("status").equals("1")?false:true;
		}
		return new OauthUserDetails(data.get("id"),data.get("username"),data.get("name"), data.get("departId"),data.get("tenantId"), data.get("password"),
				true, accountNonLocked, true, true,
				AuthorityUtils.commaSeparatedStringToAuthorityList("ROLE_USER"));
	}


}
