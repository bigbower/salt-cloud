package com.salt.cloud.auth.module.oauth.biz;

import com.salt.cloud.auth.module.oauth.entity.OauthClientDetails;
import com.salt.cloud.auth.module.oauth.mapper.OauthClientDetailsMapper;
import com.salt.cloud.common.biz.BusinessBiz;
import org.springframework.stereotype.Service;

/**
 * 
 *
 * @author Mr.AG
 * @email 463540703@qq.com
 * @version 2018-03-28 13:02:20
 */
@Service
public class OauthClientDetailsBiz extends BusinessBiz<OauthClientDetailsMapper, OauthClientDetails> {

}