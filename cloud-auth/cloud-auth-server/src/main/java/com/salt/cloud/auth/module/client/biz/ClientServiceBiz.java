package com.salt.cloud.auth.module.client.biz;

import com.salt.cloud.auth.module.client.entity.ClientService;
import com.salt.cloud.auth.module.client.mapper.ClientServiceMapper;
import com.salt.cloud.common.biz.BaseBiz;
import org.springframework.stereotype.Service;

/**
 * @author kian
 * @version 2017/12/30.
 */
@Service
public class ClientServiceBiz extends BaseBiz<ClientServiceMapper, ClientService> {
}
