

package com.salt.cloud.auth.jwt.user;

import com.salt.cloud.core.constants.RequestHeaderConstants;

import java.io.Serializable;

public class JwtAuthenticationResponse implements Serializable {
    private static final long serialVersionUID = 1250166508152483573L;

    private final String token;

    public JwtAuthenticationResponse(String token) {

        this.token = RequestHeaderConstants.JWT_TOKEN_TYPE + token;
    }

    public String getToken() {
        return this.token;
    }
}
