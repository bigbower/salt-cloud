package com.salt.cloud.auth.interceptor;

import com.salt.cloud.auth.configuration.ClientConfiguration;
import com.salt.cloud.auth.jwt.client.ClientTokenUtil;
import com.salt.cloud.auth.module.client.service.AuthClientService;
import com.salt.cloud.core.exception.auth.ClientForbiddenException;
import com.salt.cloud.core.util.jwt.IJWTInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by ace on 2017/9/12.
 */
@SuppressWarnings("ALL")
public class ServiceAuthRestInterceptor extends HandlerInterceptorAdapter {
    private Logger logger = LoggerFactory.getLogger(ServiceAuthRestInterceptor.class);

    @Autowired
    private ClientTokenUtil clientTokenUtil;
    @Autowired
    private AuthClientService authClientService;
    @Autowired
    private ClientConfiguration clientConfiguration;
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        HandlerMethod handlerMethod = (HandlerMethod) handler;
        String token = request.getHeader(clientConfiguration.getClientTokenHeader());
        IJWTInfo infoFromToken = clientTokenUtil.getInfoFromToken(token);
        String uniqueName = infoFromToken.getUniqueName();
        //调用自己的Clientservice，获取可授权访问的服务模块
       if(authClientService.getAllowedClient(clientConfiguration.getClientId()).contains(uniqueName)){
           return super.preHandle(request, response, handler);
       }
//        for(String client: authClientService.getAllowedClient(clientConfiguration.getClientId())){
//            if(client.equals(uniqueName)){
//                return super.preHandle(request, response, handler);
//            }
//        }
        throw new ClientForbiddenException("服务端访问未授权，请配置");
    }
}
