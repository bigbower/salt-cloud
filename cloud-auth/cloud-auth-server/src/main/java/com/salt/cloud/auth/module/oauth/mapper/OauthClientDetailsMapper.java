package com.salt.cloud.auth.module.oauth.mapper;

import com.salt.cloud.auth.module.oauth.entity.OauthClientDetails;
import com.salt.cloud.common.mapper.CommonMapper;

/**
 * 
 * 
 * @author Mr.AG
 * @email 463540703@qq.com
 * @version 2018-03-28 13:02:20
 */
public interface OauthClientDetailsMapper extends CommonMapper<OauthClientDetails> {
	
}
