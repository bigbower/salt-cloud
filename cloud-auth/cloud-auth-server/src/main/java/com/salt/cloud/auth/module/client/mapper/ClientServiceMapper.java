package com.salt.cloud.auth.module.client.mapper;

import com.salt.cloud.auth.module.client.entity.ClientService;
import com.salt.cloud.common.mapper.CommonMapper;

public interface ClientServiceMapper extends CommonMapper<ClientService> {
    void deleteByServiceId(String id);
}
