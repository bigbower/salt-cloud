package com.salt.cloud.auth.module.oauth.rest;

import com.salt.cloud.auth.module.oauth.biz.OauthClientDetailsBiz;
import com.salt.cloud.auth.module.oauth.entity.OauthClientDetails;
import com.salt.cloud.common.rest.BaseController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("oauthClientDetails")
public class OauthClientDetailsController extends BaseController<OauthClientDetailsBiz,OauthClientDetails,String> {

}