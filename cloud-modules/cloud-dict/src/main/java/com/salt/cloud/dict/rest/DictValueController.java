


package com.salt.cloud.dict.rest;

import com.salt.cloud.auth.client.annotation.CheckClientToken;
import com.salt.cloud.auth.client.annotation.CheckUserToken;
import com.salt.cloud.auth.client.annotation.IgnoreClientToken;
import com.salt.cloud.auth.client.annotation.IgnoreUserToken;
import com.salt.cloud.common.rest.BaseController;
import com.salt.cloud.core.msg.ObjectRestResponse;
import com.salt.cloud.core.msg.TableResultResponse;
import com.salt.cloud.dict.biz.DictTypeBiz;
import com.salt.cloud.dict.entity.DictType;
import com.salt.cloud.dict.entity.DictValue;
import com.salt.cloud.dict.biz.DictValueBiz;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import tk.mybatis.mapper.entity.Example;

import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("dictValue")
@CheckClientToken
@CheckUserToken
@Api(tags = "字典值服务",description = "字典值服务")
public class DictValueController extends BaseController<DictValueBiz, DictValue,String> {
    @Autowired
    private DictTypeBiz dictTypeBiz;
    @IgnoreClientToken
    @IgnoreUserToken
    @RequestMapping(value = "/type/{code}",method = RequestMethod.GET)
    public TableResultResponse<DictValue> getDictValueByDictTypeCode(@PathVariable("code") String code){
        Example example = new Example(DictValue.class);
        example.createCriteria().andLike("code",code+"%");
        List<DictValue> dictValues = this.baseBiz.selectByExample(example).stream().sorted(new Comparator<DictValue>() {
            @Override
            public int compare(DictValue o1, DictValue o2) {
                return o1.getOrderNum() - o2.getOrderNum();
            }
        }).collect(Collectors.toList());
        return new TableResultResponse<DictValue>(dictValues.size(),dictValues);
    }

    //此方法给服务模块调用，返回值不统一使用ObjectRestResponse
    @IgnoreClientToken
    @IgnoreUserToken
    @RequestMapping(value = "/feign/{code}",method = RequestMethod.GET)
    public Map<String,String> getDictValueByCode(@PathVariable("code") String code){
        Example example = new Example(DictValue.class);
        example.createCriteria().andLike("code",code+"%");
        List<DictValue> dictValues = this.baseBiz.selectByExample(example);
        Map<String, String> result = dictValues.stream().collect(
                Collectors.toMap(DictValue::getValue, DictValue::getLabelDefault));
        return result;
    }

    @IgnoreUserToken
    @RequestMapping(value = "logisticsnum",method = RequestMethod.GET)
    public ObjectRestResponse logisticsnum(){
        Map<String,Object> map=new HashMap<>();
        DictType dictType=new DictType();
        dictType.setCode("logistics");
        DictType loginstictype=dictTypeBiz.selectOne(dictType);
        if(loginstictype==null){
            map.put("state",false);
            return ObjectRestResponse.ok(map);
        }
        DictValue dictValue=new DictValue();
        dictValue.setTypeId(loginstictype.getId());
        List<DictValue> dictValueList=baseBiz.selectList(dictValue);
        List<Map<String,Object>> dictList=new ArrayList<>();
        for(DictValue value:dictValueList){
            Map<String,Object> newmap=new HashMap<>();
            newmap.put("logisticsname",value.getLabelDefault());
            newmap.put("logisticsno",value.getLabelEnUs());
            dictList.add(newmap);
        }
        map.put("state",true);
        map.put("logistics",dictList);
        return ObjectRestResponse.ok(map);
    }

    @RequestMapping(value = "/type/bycode",method = RequestMethod.GET)
    @IgnoreClientToken
    @IgnoreUserToken
    public TableResultResponse<DictValue> getByDictTypeCode(String code){
        Example example = new Example(DictValue.class);
        example.createCriteria().andLike("code",code+"%");
        List<DictValue> dictValues = this.baseBiz.selectByExample(example).stream().sorted(new Comparator<DictValue>() {
            @Override
            public int compare(DictValue o1, DictValue o2) {
                return o1.getOrderNum() - o2.getOrderNum();
            }
        }).collect(Collectors.toList());
        return new TableResultResponse<DictValue>(dictValues.size(),dictValues);
    }

    @GetMapping(value = "/type/dictValueByCode")
    @IgnoreClientToken
    @IgnoreUserToken
    public ObjectRestResponse dictValueByCode(String code){
        List<Map<String,Object>> dictMapList=new ArrayList<>();
        Example example = new Example(DictValue.class);
        example.createCriteria().andLike("code",code+"%");
        List<DictValue> dictValues = this.baseBiz.selectByExample(example);
        for(DictValue dictValue:dictValues){
            Map<String,Object> dicMap=new HashMap<>();
            dicMap.put("id",dictValue.getId());
            dicMap.put("value",dictValue.getValue());
            dicMap.put("code",dictValue.getValue());
            dicMap.put("labelZhCh",dictValue.getLabelZhCh());
            dictMapList.add(dicMap);
        }
        return ObjectRestResponse.ok(dictMapList);
    }
}