
package com.salt.cloud.dict;

import com.salt.cloud.auth.client.EnableAceAuthClient;
import com.spring4all.swagger.EnableSwagger2Doc;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import tk.mybatis.spring.annotation.MapperScan;

/**
 * @author kian
 * @version 2017/12/26.
 */
@SpringBootApplication
@EnableDiscoveryClient
// 开启事务
@EnableTransactionManagement
// 开启熔断监控
@EnableCircuitBreaker
// 开启服务鉴权
@EnableFeignClients
@MapperScan("com.salt.cloud.dict.mapper")
@EnableAceAuthClient
@EnableSwagger2Doc
public class DictBootstrap {
    public static void main(String[] args) {
        new SpringApplicationBuilder(DictBootstrap.class).run(args);    }
}
