


package com.salt.cloud.dict.mapper;

import com.salt.cloud.common.mapper.CommonMapper;
import com.salt.cloud.dict.entity.DictType;

/**
 * 
 * 
 * @author Mr.AG
 * @email 463540703@qq.com
 * @version 2018-01-30 19:45:55
 */
public interface DictTypeMapper extends CommonMapper<DictType> {
	
}
