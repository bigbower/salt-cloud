


package com.salt.cloud.dict.biz;

import com.salt.cloud.common.biz.BusinessBiz;
import com.salt.cloud.common.util.UUIDUtils;
import com.salt.cloud.dict.entity.DictValue;
import com.salt.cloud.dict.mapper.DictValueMapper;
import org.springframework.stereotype.Service;

/**
 * 
 *
 * @author Mr.AG
 * @email 463540703@qq.com
 * @version 2018-01-30 19:45:55
 */
@Service
public class DictValueBiz extends BusinessBiz<DictValueMapper, DictValue> {
    @Override
    public void insertSelective(DictValue entity) {
        entity.setId(UUIDUtils.generateUuid());
        super.insertSelective(entity);
    }
}