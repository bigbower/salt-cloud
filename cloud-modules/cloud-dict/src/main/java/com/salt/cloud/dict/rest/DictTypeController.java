


package com.salt.cloud.dict.rest;

import com.salt.cloud.auth.client.annotation.CheckClientToken;
import com.salt.cloud.auth.client.annotation.CheckUserToken;
import com.salt.cloud.common.rest.BaseController;
import com.salt.cloud.common.util.TreeUtil;
import com.salt.cloud.core.msg.ObjectRestResponse;
import com.salt.cloud.dict.biz.DictTypeBiz;
import com.salt.cloud.dict.entity.DictType;
import com.salt.cloud.dict.vo.DictTree;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("dictType")
@CheckClientToken
@CheckUserToken
public class DictTypeController extends BaseController<DictTypeBiz, DictType,String> {
    @RequestMapping(value = "/tree", method = RequestMethod.GET)
    public ObjectRestResponse getTree() {
        List<DictType> dictTypes = this.baseBiz.selectListAll();
        List<DictTree> trees = new ArrayList<>();
        dictTypes.forEach(dictType -> {
            trees.add(new DictTree(dictType.getId(), dictType.getParentId(), dictType.getName(),dictType.getCode()));
        });
        return ObjectRestResponse.ok(TreeUtil.bulid(trees, "-1", null));
    }

}