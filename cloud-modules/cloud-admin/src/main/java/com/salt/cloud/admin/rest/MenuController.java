

package com.salt.cloud.admin.rest;

import com.salt.cloud.admin.biz.MenuBiz;
import com.salt.cloud.admin.biz.UserBiz;
import com.salt.cloud.admin.constant.AdminCommonConstant;
import com.salt.cloud.admin.vo.MenuTree;
import com.salt.cloud.admin.entity.Menu;
import com.salt.cloud.auth.client.annotation.CheckClientToken;
import com.salt.cloud.auth.client.annotation.CheckUserToken;
import com.salt.cloud.common.rest.BaseController;
import com.salt.cloud.core.msg.ObjectRestResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import tk.mybatis.mapper.entity.Example;

/**
 * ${DESCRIPTION}
 *
 * @author kian
 * @version 2017-06-12 8:49
 */
@Controller
@RequestMapping("menu")
@CheckUserToken
@CheckClientToken
@Api(tags="菜单模块")
public class MenuController extends BaseController<MenuBiz, Menu,String> {
    @Autowired
    private UserBiz userBiz;

    @ApiOperation("获取菜单列表")
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    @ResponseBody
    public ObjectRestResponse list(String title) {
        Example example = new Example(Menu.class);
        if (StringUtils.isNotBlank(title)) {
            example.createCriteria().andLike("title", "%" + title + "%");
        }
        return ObjectRestResponse.ok(baseBiz.selectByExample(example));
    }

    @ApiOperation("获取菜单树")
    @RequestMapping(value = "/tree", method = RequestMethod.GET)
    @ResponseBody
    public ObjectRestResponse getTree(String title) {
        Example example = new Example(Menu.class);
        if (StringUtils.isNotBlank(title)) {
            example.createCriteria().andLike("title", "%" + title + "%");
        }
        return ObjectRestResponse.ok(MenuTree.buildTree(baseBiz.selectByExample(example), AdminCommonConstant.ROOT));
    }

}
