

package com.salt.cloud.admin.rest;

import com.salt.cloud.admin.biz.MenuBiz;
import com.salt.cloud.admin.biz.PositionBiz;
import com.salt.cloud.admin.biz.UserBiz;
import com.salt.cloud.admin.rpc.service.PermissionService;
import com.salt.cloud.admin.vo.AuthUser;
import com.salt.cloud.admin.entity.Position;
import com.salt.cloud.admin.entity.User;
import com.salt.cloud.auth.client.annotation.CheckClientToken;
import com.salt.cloud.auth.client.annotation.CheckUserToken;
import com.salt.cloud.auth.client.annotation.IgnoreClientToken;
import com.salt.cloud.auth.client.annotation.IgnoreUserToken;
import com.salt.cloud.common.rest.BaseController;
import com.salt.cloud.core.context.BaseContextHandler;
import com.salt.cloud.core.msg.ObjectRestResponse;
import com.salt.cloud.core.vo.UserInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * ${DESCRIPTION}
 *
 * @author kian
 * @version 2017-06-08 11:51
 */
@RestController
@RequestMapping("user")
@CheckUserToken
@CheckClientToken
@Api(tags = "用户模块")
public class UserController extends BaseController<UserBiz, User, String> {

    @Autowired
    private PermissionService permissionService;

    @Autowired
    private PositionBiz positionBiz;

    @Autowired
    private MenuBiz menuBiz;

    @IgnoreUserToken
    @ApiOperation("账户密码验证")
    @RequestMapping(value = "/validate", method = RequestMethod.POST)
    public ObjectRestResponse<UserInfo> validate(String username, String password) {
        return new ObjectRestResponse<UserInfo>().data(permissionService.validate(username, password));
    }

    @IgnoreUserToken
    @IgnoreClientToken
    @ApiOperation("根据账户名获取用户信息")
    @RequestMapping(value = "/info", method = RequestMethod.POST)
    public ObjectRestResponse<AuthUser> validate(String username) {
        AuthUser user = new AuthUser();
        User userorign = baseBiz.getUserByUsername(username);
        if (userorign != null) {
            BeanUtils.copyProperties(userorign, user);
        }
        return new ObjectRestResponse<AuthUser>().data(user);
    }


    @ApiOperation("账户修改密码")
    @RequestMapping(value = "/changePassword", method = RequestMethod.POST)
    public ObjectRestResponse<Boolean> changePassword(String oldPass, String newPass) {
        return new ObjectRestResponse<Boolean>().data(baseBiz.changePassword(oldPass, newPass));
    }


    @ApiOperation("获取用户信息接口")
    @RequestMapping(value = "/front/info", method = RequestMethod.GET)
    public ObjectRestResponse getUserInfo() throws Exception {
        return ObjectRestResponse.ok(permissionService.getUserInfo());
    }

    @ApiOperation("获取用户访问菜单")
    @RequestMapping(value = "/front/menus", method = RequestMethod.GET)
    public ObjectRestResponse getMenusByUsername() throws Exception {
        return ObjectRestResponse.ok(permissionService.getMenusByUsername());
    }

    @ApiOperation("获取所有菜单")
    @RequestMapping(value = "/front/menu/all", method = RequestMethod.GET)
    ObjectRestResponse getAllMenus()  {
        return ObjectRestResponse.ok(menuBiz.selectListAll());
    }

    @ApiOperation("获取用户可管辖部门id列表")
    @RequestMapping(value = "/dataDepart", method = RequestMethod.GET)
    public ObjectRestResponse getUserDataDepartIds(String userId) {
        List<String> depart=new ArrayList<>();
        if (BaseContextHandler.getUserID().equals(userId)) {
            depart= baseBiz.getUserDataDepartIds(userId);
        }
        return ObjectRestResponse.ok(depart);
    }

    @ApiOperation("获取用户流程审批岗位")
    @RequestMapping(value = "/flowPosition", method = RequestMethod.GET)
    public ObjectRestResponse getUserFlowPositions(String userId) {
        List<String> positions=new ArrayList<>();
        if (BaseContextHandler.getUserID().equals(userId)) {
            positions= positionBiz.getUserFlowPosition(userId).stream().map(Position::getName).collect(Collectors.toList());
        }
        return ObjectRestResponse.ok(positions);
    }
}
