

package com.salt.cloud.admin.constant;

/**
 * @author kian
 * @version 2018/1/15.
 */
public class UserConstant {
    /**
     * 密码腌制
     */
    public static int PW_ENCORDER_SALT = 15;
}
