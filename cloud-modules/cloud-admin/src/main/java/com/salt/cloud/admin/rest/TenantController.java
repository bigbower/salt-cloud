

package com.salt.cloud.admin.rest;

import com.salt.cloud.admin.biz.TenantBiz;
import com.salt.cloud.admin.biz.UserBiz;
import com.salt.cloud.admin.entity.Tenant;
import com.salt.cloud.admin.entity.User;
import com.salt.cloud.auth.client.annotation.CheckClientToken;
import com.salt.cloud.auth.client.annotation.CheckUserToken;
import com.salt.cloud.common.rest.BaseController;
import com.salt.cloud.core.msg.ObjectRestResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * 租户管理
 * @author kian
 */
@RestController
@RequestMapping("tenant")
@CheckClientToken
@CheckUserToken
@Api(tags = "租户模块")
public class TenantController extends BaseController<TenantBiz, Tenant,String> {
    @Autowired
    private UserBiz userBiz;
    @ApiOperation("租户授予用户")
    @RequestMapping(value = "/{id}/user",method = RequestMethod.PUT)
    public ObjectRestResponse<Boolean> updateUser(@PathVariable("id") String id, String userId){
        baseBiz.updateUser(id,userId);
        return new ObjectRestResponse<>();
    }

    @ApiOperation("获取租户授予用户")
    @RequestMapping(value = "/{id}/user",method = RequestMethod.GET)
    public ObjectRestResponse<User> updateUser(@PathVariable("id") String id){
        Tenant tenant = baseBiz.selectById(id);
        return new ObjectRestResponse<>().data(userBiz.selectById(tenant.getOwner()));
    }

    @ApiOperation("根据code获取租户id")
    @RequestMapping(value = "/{code}",method = RequestMethod.GET)
    public ObjectRestResponse selectTenantId(@PathVariable("code") String code){
        Tenant tenant = new Tenant();
        tenant.setCode(code);
        tenant = baseBiz.selectOne(tenant);
        Map<String,String> map = new HashMap<>();
        map.put("tenantId",tenant.getId());
        return ObjectRestResponse.ok(map);
    }
}