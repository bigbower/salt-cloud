

package com.salt.cloud.admin.rest;

import com.salt.cloud.admin.biz.GroupTypeBiz;
import com.salt.cloud.admin.entity.GroupType;
import com.salt.cloud.auth.client.annotation.CheckClientToken;
import com.salt.cloud.auth.client.annotation.CheckUserToken;
import com.salt.cloud.common.rest.BaseController;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * ${DESCRIPTION}
 *
 * @author kian
 * @version 2017-06-08 11:51
 */
@RestController
@RequestMapping("groupType")
@CheckUserToken
@CheckClientToken
@Api(tags = "角色组")
public class GroupTypeController extends BaseController<GroupTypeBiz, GroupType, String> {

//    @RequestMapping(value = "/page",method = RequestMethod.GET)
//    public TableResultResponse<Object> page(@RequestParam(defaultValue = "10") int limit, @RequestParam(defaultValue = "1")int page, String name){
//        Example example = new Example(GroupType.class);
//        if(StringUtils.isNotBlank(name))
//            example.createCriteria().andLike("name", "%" + name + "%");
//        Page<Object> result = PageHelper.startPage(page, limit);
//        baseBiz.selectByExample(example);
//        return new TableResultResponse<Object>(result.getTotal(),result.getResult());
//    }

}
