

package com.salt.cloud.admin.service;

import com.salt.cloud.core.msg.TableResultResponse;
import com.salt.cloud.merge.facade.IMergeResultParser;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author kian
 * @create 2018/2/4.
 */
@Component
public class TableResultParser implements IMergeResultParser {
    @Override
    public List parser(Object o) {
        TableResultResponse response = (TableResultResponse) o;
        TableResultResponse.TableData data = (TableResultResponse.TableData) response.getData();
        List result = data.getRows();
        return result;
    }
}
