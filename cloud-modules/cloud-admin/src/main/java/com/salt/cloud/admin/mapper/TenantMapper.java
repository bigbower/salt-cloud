package com.salt.cloud.admin.mapper;

import com.salt.cloud.admin.entity.Tenant;
import com.salt.cloud.common.mapper.CommonMapper;

/**
 * 租户表
 * 
 * @author Mr.AG
 * @email 463540703@qq.com
 * @version 2018-02-08 21:42:09
 */
@com.salt.cloud.common.data.Tenant
public interface TenantMapper extends CommonMapper<Tenant> {
	
}
