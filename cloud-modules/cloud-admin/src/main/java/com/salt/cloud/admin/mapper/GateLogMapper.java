

package com.salt.cloud.admin.mapper;

import com.salt.cloud.admin.entity.GateLog;
import com.salt.cloud.common.data.Tenant;
import com.salt.cloud.common.mapper.CommonMapper;

@Tenant(userField = "crt_user")
public interface GateLogMapper extends CommonMapper<GateLog> {
}
