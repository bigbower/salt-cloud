

package com.salt.cloud.admin.mapper;

import com.salt.cloud.admin.entity.User;
import com.salt.cloud.common.data.Tenant;
import com.salt.cloud.common.mapper.CommonMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Tenant
public interface UserMapper extends CommonMapper<User> {
    public List<User> selectMemberByGroupId(@Param("groupId") String groupId);
    public List<User> selectLeaderByGroupId(@Param("groupId") String groupId);
    List<String> selectUserDataDepartIds(String userId);
}
