

package com.salt.cloud.admin.rpc;

import com.salt.cloud.admin.rpc.service.PermissionService;
import com.salt.cloud.auth.client.annotation.CheckClientToken;
import com.salt.cloud.auth.client.annotation.CheckUserToken;
import com.salt.cloud.auth.client.annotation.IgnoreClientToken;
import com.salt.cloud.auth.client.annotation.IgnoreUserToken;
import com.salt.cloud.core.vo.PermissionInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * ${DESCRIPTION}
 *
 * @author kian
 * @version 2017-06-21 8:15
 */
@RestController
@RequestMapping("api")
@CheckUserToken
@CheckClientToken
public class UserRest {
    @Autowired
    private PermissionService permissionService;

//    @Cache(key="permission")
    @RequestMapping(value = "/permissions", method = RequestMethod.GET)
    @IgnoreUserToken
    @IgnoreClientToken
    public @ResponseBody List<PermissionInfo> getAllPermission(){
        return permissionService.getAllPermission();
    }

    @RequestMapping(value = "/user/{userId}/permissions", method = RequestMethod.GET)
    @IgnoreUserToken
    @IgnoreClientToken
    public @ResponseBody List<PermissionInfo> getPermissionByUserId(@PathVariable("userId") String userId){
        return permissionService.getPermissionByUserId(userId);
    }




}
