package com.salt.cloud.admin.rpc;

import com.salt.cloud.admin.biz.GateLogBiz;
import com.salt.cloud.admin.entity.GateLog;
import com.salt.cloud.auth.client.annotation.CheckClientToken;
import com.salt.cloud.auth.client.annotation.CheckUserToken;
import com.salt.cloud.auth.client.annotation.IgnoreClientToken;
import com.salt.cloud.auth.client.annotation.IgnoreUserToken;
import com.salt.cloud.core.vo.LogInfo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * ${DESCRIPTION}
 *
 * @author kian
 * @version 2017-07-01 14:39
 */
@RequestMapping("api")
@RestController
@CheckUserToken
@CheckClientToken
public class LogRest {
    @Autowired
    private GateLogBiz gateLogBiz;

    @IgnoreUserToken
    @IgnoreClientToken
    @RequestMapping(value="/log/save",method = RequestMethod.POST)
    public @ResponseBody void saveLog(@RequestBody LogInfo info){
        GateLog log = new GateLog();
        BeanUtils.copyProperties(info,log);
        gateLogBiz.insertSelective(log);
    }
}
