package com.salt.cloud.admin.rest;

import com.salt.cloud.admin.biz.DepartBiz;
import com.salt.cloud.admin.entity.Depart;
import com.salt.cloud.admin.entity.User;
import com.salt.cloud.admin.vo.DepartTree;
import com.salt.cloud.auth.client.annotation.CheckClientToken;
import com.salt.cloud.auth.client.annotation.CheckUserToken;
import com.salt.cloud.common.rest.BaseController;
import com.salt.cloud.common.util.TreeUtil;
import com.salt.cloud.core.msg.ObjectRestResponse;
import com.salt.cloud.core.msg.TableResultResponse;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @author kian
 */
@RestController
@RequestMapping("depart")
@CheckClientToken
@CheckUserToken
@Api(tags = "部门管理")
public class DepartController extends BaseController<DepartBiz, Depart,String> {
    @ApiOperation("获取部门树")
    @RequestMapping(value = "/tree", method = RequestMethod.GET)
    public ObjectRestResponse getTree() {
        List<Depart> departs = this.baseBiz.selectListAll();
        List<DepartTree> trees = new ArrayList<>();
        departs.forEach(dictType -> {
            trees.add(new DepartTree(dictType.getId(), dictType.getParentId(), dictType.getName(),dictType.getCode()));
        });
        return ObjectRestResponse.ok(TreeUtil.bulid(trees, "-1", null));
    }
    @ApiOperation("获取部门关联用户")
    @RequestMapping(value = "user",method = RequestMethod.GET)
    public TableResultResponse<User> getDepartUsers(String departId, String userName){
        return this.baseBiz.getDepartUsers(departId,userName);
    }

    @ApiOperation("部门添加用户")
    @RequestMapping(value = "user",method = RequestMethod.POST)
    public ObjectRestResponse<Boolean> addDepartUser(String departId, String userIds){
        this.baseBiz.addDepartUser(departId,userIds);
        return new ObjectRestResponse<>().data(true);
    }

    @ApiOperation("部门移除用户")
    @RequestMapping(value = "user",method = RequestMethod.DELETE)
    public ObjectRestResponse<Boolean> delDepartUser(String departId,String userId){
        this.baseBiz.delDepartUser(departId,userId);
        return new ObjectRestResponse<>().data(true);
    }

    @ApiOperation("获取部门信息")
    @RequestMapping(value = "getByPK/{id}",method = RequestMethod.GET)
    public ObjectRestResponse getDepart(@PathVariable String id){
        return ObjectRestResponse.ok(this.baseBiz.getDeparts(id));
    }

}