

package com.salt.cloud.admin.biz;

import com.salt.cloud.admin.constant.AdminCommonConstant;
import com.salt.cloud.admin.entity.Element;
import com.salt.cloud.admin.mapper.ElementMapper;
import com.salt.cloud.admin.mapper.UserMapper;
import com.salt.cloud.common.biz.BusinessBiz;
import com.salt.cloud.common.util.BooleanUtil;
import com.salt.cloud.merge.annonation.MergeResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * ${DESCRIPTION}
 *
 * @author kian
 * @version 2017-06-23 20:27
 */
@Service
@Transactional(rollbackFor = Exception.class)
@Slf4j
public class ElementBiz extends BusinessBiz<ElementMapper,Element> {
    @Autowired
    private UserMapper userMapper;
    /**
     * 获取用户可以访问的资源
     * @param userId
     * @return
     */
    @Cacheable(value = "permission:ele:{userId}", key = "#root.targetClass.getName() + #root.methodName + #userId")
    public List<Element> getAuthorityElementByUserId(String userId){
        log.info("没有进缓存，从数据获取"+userId+"的资源数据");
        if(BooleanUtil.BOOLEAN_TRUE.equals(userMapper.selectByPrimaryKey(userId).getIsSuperAdmin())){
            return mapper.selectAllElementPermissions();
        }
       return mapper.selectAuthorityElementByUserId(userId,AdminCommonConstant.RESOURCE_TYPE_VIEW);
    }

    public List<Element> getAuthorityElementByUserId(String userId,String menuId){
        return mapper.selectAuthorityMenuElementByUserId(userId,menuId,AdminCommonConstant.RESOURCE_TYPE_VIEW);
    }

//    @Cache(key="permission:ele")
    @Cacheable(value = "permission:ele", key = "#root.targetClass.getName() + #root.methodName")
    public List<Element> getAllElementPermissions(){
        log.info("没有进缓存，从数据获取全部的资源数据");
        return mapper.selectAllElementPermissions();
    }

    @Override
//    @CacheClear(keys={"permission:ele","permission"})
    @CacheEvict(value = "permission:ele", key = "#root.targetClass.getName() + #root.methodName + #userId",allEntries = true)
    public void insertSelective(Element entity) {
        log.info("清除缓存资源数据");
        super.insertSelective(entity);
    }

    @Override
//    @CacheClear(keys={"permission:ele","permission"})
    public void updateSelectiveById(Element entity) {
        super.updateSelectiveById(entity);
    }

    @MergeResult
    @Override
    public List<Element> selectByExample(Object example) {
        return super.selectByExample(example);
    }
}
