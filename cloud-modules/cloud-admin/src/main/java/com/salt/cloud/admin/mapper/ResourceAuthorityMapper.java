

package com.salt.cloud.admin.mapper;

import com.salt.cloud.admin.entity.ResourceAuthority;
import com.salt.cloud.common.mapper.CommonMapper;
import org.apache.ibatis.annotations.Param;

public interface ResourceAuthorityMapper extends CommonMapper<ResourceAuthority> {
    public void deleteByAuthorityIdAndResourceType(@Param("authorityId") String authorityId, @Param("resourceType") String resourceType, @Param("type") String type);
}
