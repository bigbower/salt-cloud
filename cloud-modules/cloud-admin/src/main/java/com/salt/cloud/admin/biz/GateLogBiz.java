

package com.salt.cloud.admin.biz;

import com.salt.cloud.admin.entity.GateLog;
import com.salt.cloud.admin.mapper.GateLogMapper;
import com.salt.cloud.common.biz.BusinessBiz;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * ${DESCRIPTION}
 *
 * @author kian
 * @version 2017-07-01 14:36
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class GateLogBiz extends BusinessBiz<GateLogMapper, GateLog> {

    @Override
    public void insertSelective(GateLog entity) {
        mapper.insertSelective(entity);
    }
}
