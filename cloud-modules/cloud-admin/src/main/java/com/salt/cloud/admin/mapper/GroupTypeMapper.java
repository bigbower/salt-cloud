

package com.salt.cloud.admin.mapper;

import com.salt.cloud.admin.entity.GroupType;
import com.salt.cloud.common.mapper.CommonMapper;

public interface GroupTypeMapper extends CommonMapper<GroupType> {
}
