

package com.salt.cloud.admin.rest;

import com.salt.cloud.admin.biz.GateLogBiz;
import com.salt.cloud.admin.entity.GateLog;
import com.salt.cloud.auth.client.annotation.CheckClientToken;
import com.salt.cloud.auth.client.annotation.CheckUserToken;
import com.salt.cloud.common.rest.BaseController;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * ${DESCRIPTION}
 *
 * @author kian
 * @version 2017-07-01 20:32
 */
@RestController
@CheckUserToken
@CheckClientToken
@RequestMapping("gateLog")
@Api(tags = "日志管理")
public class GateLogController extends BaseController<GateLogBiz, GateLog,Integer> {

//    @RequestMapping(value = "/page",method = RequestMethod.GET)
//    public TableResultResponse<GateLog> page(@RequestParam(defaultValue = "10") int limit, @RequestParam(defaultValue = "1")int offset, String name){
//        Example example = new Example(GateLog.class);
//        if(StringUtils.isNotBlank(name)) {
//            example.createCriteria().andLike("menu", "%" + name + "%");
//        }
//        int count = baseBiz.selectCountByExample(example);
//        PageHelper.startPage(offset, limit);
//        return new TableResultResponse<GateLog>(count,baseBiz.selectByExample(example));
//    }
}
