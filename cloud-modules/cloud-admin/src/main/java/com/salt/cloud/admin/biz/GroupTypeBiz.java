

package com.salt.cloud.admin.biz;

import com.salt.cloud.admin.entity.GroupType;
import com.salt.cloud.admin.mapper.GroupTypeMapper;
import com.salt.cloud.common.biz.BusinessBiz;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * ${DESCRIPTION}
 *
 * @author kian
 * @version 2017-06-12 8:48
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class GroupTypeBiz extends BusinessBiz<GroupTypeMapper,GroupType> {
}
