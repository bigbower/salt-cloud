

package com.salt.cloud.admin.service;

import com.salt.cloud.admin.biz.UserBiz;
import com.salt.cloud.common.data.IUserDepartDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author kian
 * @create 2020/1/21.
 */
@Component
public class UserDepartDataService implements IUserDepartDataService {
    @Autowired
    private UserBiz userFeign;
    @Override
    public List<String> getUserDataDepartIds(String userId) {
        // 获取用户授权的部门数据权限,此处模拟两个账户
        return userFeign.getUserDataDepartIds(userId);
    }
}
