package com.salt.cloud.admin;

import com.spring4all.swagger.EnableSwagger2Doc;
import com.salt.cloud.auth.client.EnableAceAuthClient;
import com.salt.cloud.merge.EnableAceMerge;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import tk.mybatis.spring.annotation.MapperScan;

/**
 * ${DESCRIPTION}
 *
 * @author kian
 * @version 2020-01-21
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
@EnableScheduling
@EnableCaching
@EnableTransactionManagement
@MapperScan("com.salt.cloud.admin.mapper")
@EnableAceAuthClient
@EnableSwagger2Doc
@EnableAceMerge
public class AdminBootstrap {
    public static void main(String[] args) {
        new SpringApplicationBuilder(AdminBootstrap.class).run(args);     }
}
