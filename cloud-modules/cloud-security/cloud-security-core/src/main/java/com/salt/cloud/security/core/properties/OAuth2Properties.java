/**
 * 
 */
package com.salt.cloud.security.core.properties;

/**
 * @author zhailiang
 *
 */
public class OAuth2Properties {

	/**
	 * 客户端配置
	 */
	private OAuth2ClientProperties[] clients = {};

	public OAuth2ClientProperties[] getClients() {
		return clients;
	}

	public void setClients(OAuth2ClientProperties[] clients) {
		this.clients = clients;
	}




	/**
	 *普通用户登录是否需要验证码
	*/
	private boolean showImage=true;

	public boolean isShowImage() {
		return showImage;
	}

	public void setShowImage(boolean showImage) {
		this.showImage = showImage;
	}

	/**
	 *是否需要单一设备登录
	 */
	private  boolean singlePlaceLogin=false;

	public boolean isSinglePlaceLogin() {
		return singlePlaceLogin;
	}

	public void setSinglePlaceLogin(boolean singlePlaceLogin) {
		this.singlePlaceLogin = singlePlaceLogin;
	}


}
