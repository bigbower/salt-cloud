/**
 * 
 */
package com.salt.cloud.security.core.validatecode.sms;

/**
 * @author zhailiang
 *
 */
public interface SmsCodeSender {
	
	/**
	 * @param mobile
	 * @param code
	 */
	void send(String mobile, String code) throws Exception;

}
