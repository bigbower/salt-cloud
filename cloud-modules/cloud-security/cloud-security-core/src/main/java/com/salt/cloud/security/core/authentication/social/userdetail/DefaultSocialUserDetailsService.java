/**
 * 
 */
package com.salt.cloud.security.core.authentication.social.userdetail;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 * 
 * 默认的 DefaultSocialUserDetailsService 实现
 * 
 * 不做任何处理，只在控制台打印一句日志，然后抛出异常，提醒业务系统自己配置 DefaultSocialUserDetailsService。
 * 
 * @author zhailiang
 *
 */
public class DefaultSocialUserDetailsService implements SocialUserDetailsService{

	private Logger logger = LoggerFactory.getLogger(getClass());

	@Override
	public UserDetails loadUserByUsername(String providerId, String openId) throws UsernameNotFoundException {
		logger.warn("请配置 SocialUserDetailsService 接口的实现.");
		throw new UsernameNotFoundException("请配置社交登录"+providerId+"的实现");
	}
}
