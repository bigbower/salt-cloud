package com.salt.cloud.security.core.authentication.social.userdetail;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public interface SocialUserDetailsService {

    UserDetails loadUserByUsername(String providerId,String openId) throws UsernameNotFoundException;

}
