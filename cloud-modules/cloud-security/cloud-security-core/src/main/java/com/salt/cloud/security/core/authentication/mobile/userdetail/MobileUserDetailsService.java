package com.salt.cloud.security.core.authentication.mobile.userdetail;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

public interface MobileUserDetailsService {

    UserDetails loadUserByUsername(String var1) throws UsernameNotFoundException;

}
