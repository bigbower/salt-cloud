/**
 *
 */
package com.salt.cloud.security.token.authentication;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.salt.cloud.core.constants.CommonConstants;
import com.salt.cloud.core.msg.ObjectRestResponse;
import com.salt.cloud.core.util.RedisKeyUtil;
import com.salt.cloud.security.core.properties.SecurityProperties;
import com.salt.cloud.security.token.utils.HeaderHelper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.MapUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.exceptions.UnapprovedClientAuthenticationException;
import org.springframework.security.oauth2.provider.*;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * APP环境下认证成功处理器
 *
 * @author zhailiang
 *
 */
@Component("customAuthenticationSuccessHandler")
@Slf4j
public class CustomAuthenticationSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {


    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private ClientDetailsService clientDetailsService;

    @Autowired
    private AuthorizationServerTokenServices authorizationServerTokenServices;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private SecurityProperties securityProperties;

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    /*
     * (non-Javadoc)
     *
     * @see org.springframework.security.web.authentication.
     * AuthenticationSuccessHandler#onAuthenticationSuccess(javax.servlet.http.
     * HttpServletRequest, javax.servlet.http.HttpServletResponse,
     * org.springframework.security.core.Authentication)
     */
    @SuppressWarnings("unchecked")
    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
                                        Authentication authentication) throws IOException {

        logger.info("登录成功");
        String[] tokens = HeaderHelper.extractAndDecodeHeader(request);
        assert tokens.length == 2;

        String clientId = tokens[0];
        String clientSecret = tokens[1];

        ClientDetails clientDetails = clientDetailsService.loadClientByClientId(clientId);

        if (clientDetails == null) {
            throw new UnapprovedClientAuthenticationException("clientId对应的配置信息不存在:" + clientId);
        } else if (!passwordEncoder.matches(clientSecret, clientDetails.getClientSecret())) {
            throw new UnapprovedClientAuthenticationException("clientSecret不匹配:" + clientId);
        }

        if (securityProperties.getOauth2().isSinglePlaceLogin()) {
            //设置登陆后，原有的token全部失效
//
//            Collection<OAuth2AccessToken> oAuth2AccessTokens = redisTokenStore.findTokensByClientIdAndUserName(clientId, authentication.getName());
//            for (OAuth2AccessToken o : oAuth2AccessTokens) {
//                redisTokenStore.removeAccessToken(o);
//            }
            log.info("清除Redis该用户的信息");
            redisTemplate.delete(redisTemplate.keys(RedisKeyUtil.buildUserAbleKeyLike(authentication.getName(),clientId)));
        }

        //生成accessToken
        TokenRequest tokenRequest = new TokenRequest(MapUtils.EMPTY_MAP, clientId, clientDetails.getScope(), "custom");
        OAuth2Request oAuth2Request = tokenRequest.createOAuth2Request(clientDetails);
        OAuth2Authentication oAuth2Authentication = new OAuth2Authentication(oAuth2Request, authentication);
        OAuth2AccessToken token = authorizationServerTokenServices.createAccessToken(oAuth2Authentication);

        //设置redis 当前可登录的token
        String redisKey=RedisKeyUtil.buildUserAbleKey(authentication.getName(),clientId,new DateTime(token.getAdditionalInformation().get(CommonConstants.JWT_KEY_EXPIRE)).toDate());
        redisTemplate.opsForValue().set(redisKey,token.getValue());



        response.setContentType("application/json;charset=UTF-8");
        response.getWriter().write(objectMapper.writeValueAsString(ObjectRestResponse.ok(token)));

    }


}
