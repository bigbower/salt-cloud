package com.salt.cloud.security.token.utils;

import com.salt.cloud.core.util.jwt.IJWTInfo;
import com.salt.cloud.core.util.jwt.JWTHelper;
import com.salt.cloud.security.token.server.KeyConfiguration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Map;

/**
 * @author kian
 * @version 2017/9/10
 */
@Component
public class JwtTokenUtil {

    public int getExpire() {
        return expire;
    }

    @Value("${jwt.expire}")
    private int expire;

    @Autowired
    private KeyConfiguration keyConfiguration;


    public String generateToken(IJWTInfo jwtInfo, Map<String, String> otherInfo,Date expireTime) throws Exception {
        return JWTHelper.generateToken(jwtInfo, keyConfiguration.getUserPriKey(), expireTime, otherInfo);
    }

    public IJWTInfo getInfoFromToken(String token) throws Exception {
        IJWTInfo infoFromToken = JWTHelper.getInfoFromToken(token, keyConfiguration.getUserPubKey());
        return infoFromToken;
    }



}
