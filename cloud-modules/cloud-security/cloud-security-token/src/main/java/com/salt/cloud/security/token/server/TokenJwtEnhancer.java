/**
 * 
 */
package com.salt.cloud.security.token.server;

import com.salt.cloud.core.constants.CommonConstants;
import com.salt.cloud.security.core.bean.OauthUserDetails;
import com.salt.cloud.security.token.utils.JwtTokenUtil;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author zhailiang
 *
 */
public class TokenJwtEnhancer implements TokenEnhancer {

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Override
	public OAuth2AccessToken enhance(OAuth2AccessToken accessToken, OAuth2Authentication authentication) {
//		Map<String, Object> info = new HashMap<>();
//		info.put("company", "imooc");
//
//		((DefaultOAuth2AccessToken)accessToken).setAdditionalInformation(info);
//
//		return accessToken;

		OauthUserDetails user = (OauthUserDetails) authentication.getUserAuthentication().getPrincipal();// 与登录时候放进去的UserDetail实现类一直查看link{SecurityConfiguration}
		/** 自定义一些token属性 ***/
		final Map<String, Object> additionalInformation = new HashMap<>();
		Date expireTime = DateTime.now().plusSeconds(jwtTokenUtil.getExpire()).toDate();
		additionalInformation.put(CommonConstants.JWT_KEY_EXPIRE, expireTime);
		additionalInformation.put(CommonConstants.JWT_KEY_USER_ID, user.getId());
		additionalInformation.put(CommonConstants.JWT_KEY_TENANT_ID, user.getTenantId());
		additionalInformation.put(CommonConstants.JWT_KEY_DEPART_ID, user.getDepartId());
		additionalInformation.put(CommonConstants.JWT_KEY_NAME, user.getName());
		additionalInformation.put("sub", user.getUsername());
		((DefaultOAuth2AccessToken) accessToken).setAdditionalInformation(additionalInformation);
		
		return accessToken;
	}

}
