/**
 * 
 */
package com.salt.cloud.security.token.server;

import com.salt.cloud.core.constants.RedisKeyConstants;
import com.salt.cloud.core.util.RsaKeyHelper;
import com.salt.cloud.security.core.properties.SecurityProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.oauth2.provider.token.TokenEnhancer;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.redis.RedisTokenStore;


import java.security.KeyPair;
import java.util.Map;

/**
 * @author zhailiang
 *
 */
@Configuration
public class TokenStoreConfig {

	
	/**
	 * 使用redis存储token的配置，只有在imooc.security.oauth2.tokenStore配置为redis时生效
	 * @author zhailiang
	 *
	 */
	@Configuration
	@ConditionalOnProperty(prefix = "salt.security.oauth2", name = "tokenStore", havingValue = "redis")
	@Slf4j
	public static class RedisConfig {
		
		@Autowired
		private RedisConnectionFactory redisConnectionFactory;
		
		/**
		 * @return
		 */
		@Bean
		public TokenStore redisTokenStore() {
			log.info("使用的是 redis的配置");
//			return new RedisTokenStore(redisConnectionFactory);
			RedisTokenStore redisTokenStore = new RedisTokenStore(redisConnectionFactory);
			redisTokenStore.setPrefix("AG:OAUTH:");
			//设置redis每次登陆的token 不重复
			redisTokenStore.setAuthenticationKeyGenerator(new RandomAuthenticationKeyGenerator());
			return redisTokenStore;
		}
		
	}

	/**
	 * 使用jwt时的配置，默认生效
	 * 
	 * @author zhailiang
	 *
	 */
	@Configuration
	@ConditionalOnProperty(prefix = "salt.security.oauth2", name = "tokenStore", havingValue = "jwt", matchIfMissing = true)
	@Slf4j
	public static class JwtConfig {

		@Autowired
		private RedisTemplate<String, String> redisTemplate;

		@Autowired
		private RedisConnectionFactory redisConnectionFactory;

		@Autowired
		private SecurityProperties securityProperties;
		
		/**
		 * @return
		 */
//		@Bean
//		public TokenStore jwtTokenStore() {
//
//			JwtTokenStore tokenStore=new JwtTokenStore(jwtAccessTokenConverter());
//			log.info("使用的是 jwt的配置");
//
//
//			return tokenStore;
//		}
		@Bean
		public TokenStore redisTokenStore() {
			log.info("使用的是 jwt的配置");
//			return new RedisTokenStore(redisConnectionFactory);
			RedisTokenStore redisTokenStore = new RedisTokenStore(redisConnectionFactory);
			redisTokenStore.setPrefix("AG:OAUTH:");
			//设置redis每次登陆的token 不重复
			redisTokenStore.setAuthenticationKeyGenerator(new RandomAuthenticationKeyGenerator());
			return redisTokenStore;
		}

		@Autowired
		private KeyConfiguration keyConfiguration;
		
		/**
		 * @return
		 */
		@Bean
		public JwtAccessTokenConverter jwtAccessTokenConverter()   {
			JwtAccessTokenConverter converter = new JwtAccessTokenConverter();
			byte[] pri,pub = null;
			try {
				if ( !redisTemplate.hasKey(RedisKeyConstants.REDIS_USER_PRI_KEY)&&!redisTemplate.hasKey(RedisKeyConstants.REDIS_USER_PUB_KEY)) {
					log.info("初始化用户认证公钥/密钥...");
					Map<String, byte[]> keyMap = RsaKeyHelper.generateKey(keyConfiguration.getUserSecret());
					redisTemplate.opsForValue().set(RedisKeyConstants.REDIS_USER_PRI_KEY, RsaKeyHelper.toHexString(keyMap.get("pri")));
					redisTemplate.opsForValue().set(RedisKeyConstants.REDIS_USER_PUB_KEY, RsaKeyHelper.toHexString(keyMap.get("pub")));
				}
				pri = RsaKeyHelper.toBytes(redisTemplate.opsForValue().get(RedisKeyConstants.REDIS_USER_PRI_KEY));
				pub = RsaKeyHelper.toBytes(redisTemplate.opsForValue().get(RedisKeyConstants.REDIS_USER_PUB_KEY));

				converter.setKeyPair( new KeyPair(RsaKeyHelper.getPublicKey(pub), RsaKeyHelper.getPrivateKey(pri)));
				log.info("初始化用户认证公钥/密钥成功...");
			}catch(Exception e){
				log.error("初始化用户认证公钥/密钥异常...", e);
				throw new RuntimeException("初始化用户认证公钥/密钥异常..."+e.getMessage());
			}

			return converter;

		}





		
		/**
		 * @return
		 */
		@Bean
		@ConditionalOnBean(TokenEnhancer.class)
		public TokenEnhancer jwtTokenEnhancer(){
			return new TokenJwtEnhancer();
		}
		
	}
	
	

}
