package com.salt.cloud.security.token.server;

import com.salt.cloud.core.constants.RequestHeaderConstants;
import com.salt.cloud.core.msg.ObjectRestResponse;
import com.salt.cloud.core.util.RedisKeyUtil;
import com.salt.cloud.core.util.jwt.IJWTInfo;
import com.salt.cloud.security.token.utils.JwtTokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.security.oauth2.provider.token.ConsumerTokenServices;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 提供登录注销功能
 * @author kianlaw
 * @create 2018/3/27.
 */
@RestController
public class RevokeTokenEndpoint {

    @Autowired
    @Qualifier("consumerTokenServices")
    ConsumerTokenServices consumerTokenServices;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @RequestMapping(method = RequestMethod.POST, value = "/authentication/logout")
    @ResponseBody
    public ObjectRestResponse revokeToken(String access_token) throws Exception {
        String realToken = getRealToken(access_token);
        if (consumerTokenServices.revokeToken(realToken)){
           //清除登录成功的自定义JWT Token
            IJWTInfo infoFromToken = jwtTokenUtil.getInfoFromToken(realToken);
            redisTemplate.delete(RedisKeyUtil.buildUserAbleKey(infoFromToken.getUniqueName(), infoFromToken.getOtherInfo().get("client_id"), infoFromToken.getExpireTime()));
            return new ObjectRestResponse<Boolean>().data(true);
        }else{
            return new ObjectRestResponse<Boolean>().data(false);
        }
    }

    private String getRealToken(String originToken) {
        if (originToken != null && originToken.startsWith(RequestHeaderConstants.JWT_TOKEN_TYPE)) {
            originToken = originToken.substring(RequestHeaderConstants.JWT_TOKEN_TYPE.length());
        }
        return originToken;
    }
}
