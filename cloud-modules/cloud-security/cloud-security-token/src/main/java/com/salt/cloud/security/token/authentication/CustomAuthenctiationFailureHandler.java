/**
 *
 */
package com.salt.cloud.security.token.authentication;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.salt.cloud.core.msg.ObjectRestResponse;
import com.salt.cloud.core.util.RedisKeyUtil;
import com.salt.cloud.security.core.properties.SecurityConstants;
import com.salt.cloud.security.token.utils.HeaderHelper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * APP环境下认证失败处理器
 *
 * @author zhailiang
 *
 */
@Component("customAuthenctiationFailureHandler")
@Slf4j
public class CustomAuthenctiationFailureHandler extends SimpleUrlAuthenticationFailureHandler {


    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Autowired
    private ObjectMapper objectMapper;

    /* (non-Javadoc)
     * @see org.springframework.security.web.authentication.AuthenticationFailureHandler#onAuthenticationFailure(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse, org.springframework.security.core.AuthenticationException)
     */
    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
                                        AuthenticationException exception) throws IOException {

        //账号锁定异常，需要处理token
        if (exception instanceof LockedException) {

            String[] tokens = HeaderHelper.extractAndDecodeHeader(request);
            assert tokens.length == 2;

            String clientId = tokens[0];
            String redisKey = "";

            switch (request.getRequestURI()) {
                case SecurityConstants.DEFAULT_SIGN_IN_PROCESSING_URL_MOBILE:
                    redisKey = request.getParameter(SecurityConstants.DEFAULT_PARAMETER_NAME_MOBILE);
                    break;
                case SecurityConstants.DEFAULT_SIGN_IN_PROCESSING_URL_FORM:
                    redisKey = request.getParameter(SecurityConstants.DEFAULT_PARAMETER_NAME_FORM);
                    break;
                case SecurityConstants.DEFAULT_SIGN_IN_PROCESSING_URL_OPENID:
                    redisKey = request.getParameter(SecurityConstants.DEFAULT_PARAMETER_NAME_OPENID);
                    break;

            }

            log.info("账号异常后，清除Redis该用户的信息");
            if (StringUtils.isNoneEmpty(redisKey)) {
                redisTemplate.delete(redisTemplate.keys(RedisKeyUtil.buildUserAbleKeyLike(redisKey, clientId)));
            }
        }

        logger.info("登录失败");

        response.setStatus(HttpStatus.UNAUTHORIZED.value());
        response.setContentType("application/json;charset=UTF-8");
        response.getWriter().write(objectMapper.writeValueAsString(ObjectRestResponse.error(exception.getMessage())));


    }


}
