/**
 * 
 */
package com.salt.cloud.security.token.server;

import com.salt.cloud.security.core.authentication.form.FormAuthenticationConfig;
import com.salt.cloud.security.core.authentication.mobile.SmsCodeAuthenticationSecurityConfig;
import com.salt.cloud.security.core.authentication.social.OpenIdAuthenticationSecurityConfig;
import com.salt.cloud.security.core.authorize.AuthorizeConfigManager;
import com.salt.cloud.security.core.properties.SecurityConstants;
import com.salt.cloud.security.core.validatecode.ValidateCodeSecurityConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

/**
 * 资源服务器配置
 * 
 * @author zhailiang
 *
 */
@Configuration
@EnableResourceServer
public class CloudResourceServerConfig extends ResourceServerConfigurerAdapter {
	
	@Autowired
	protected AuthenticationSuccessHandler customAuthenticationSuccessHandler;
	
	@Autowired
	protected AuthenticationFailureHandler customAuthenctiationFailureHandler;
	
	@Autowired
	private SmsCodeAuthenticationSecurityConfig smsCodeAuthenticationSecurityConfig;
	

	@Autowired
	private ValidateCodeSecurityConfig validateCodeSecurityConfig;
	

	@Autowired
	private AuthorizeConfigManager authorizeConfigManager;
	
	@Autowired
	private FormAuthenticationConfig formAuthenticationConfig;

	@Autowired
	private OpenIdAuthenticationSecurityConfig openIdAuthenticationSecurityConfig;

	@Override
	public void configure(HttpSecurity http) throws Exception {
		
		formAuthenticationConfig.configure(http);
		
		http.apply(validateCodeSecurityConfig)
				.and()
			.apply(smsCodeAuthenticationSecurityConfig)
				.and()
				.apply(openIdAuthenticationSecurityConfig)
				.and()
				.authorizeRequests().antMatchers(SecurityConstants.DEFAULT_SIGN_IN_PROCESSING_URL_SOCIAL,SecurityConstants.DEFAULT_LOGOUT_IN_PAGE_URL).permitAll()
				.and()
			.csrf().disable();
		
		authorizeConfigManager.config(http.authorizeRequests());
	}
	
}