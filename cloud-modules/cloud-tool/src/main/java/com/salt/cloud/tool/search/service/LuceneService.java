package com.salt.cloud.tool.search.service;


import com.salt.cloud.core.msg.TableResultResponse;
import com.salt.cloud.tool.search.vo.IndexObject;

/**
 * lucense 接口
 * @author kian
 */
public interface LuceneService {

    void save(IndexObject indexObject);

    void update(IndexObject indexObject);

    void delete(IndexObject indexObject);

    void deleteAll();

    TableResultResponse page(Integer pageNumber, Integer pageSize, String keyword);
}
