package com.salt.cloud.tool.wechat.rest;

import com.salt.cloud.core.msg.ObjectRestResponse;
import com.salt.cloud.tool.wechat.config.WxMpConfiguration;
import me.chanjar.weixin.common.bean.WxJsapiSignature;
import me.chanjar.weixin.common.error.WxErrorException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/wechat")
public class JssdkController {

    @Value("${wx.mp.mainappid}")
    private String appid;

    @GetMapping("/config")
    @ResponseBody
    public ObjectRestResponse sign(@RequestParam String pageUrl) throws WxErrorException {
        WxJsapiSignature signature=WxMpConfiguration.getMpServices().get(appid).createJsapiSignature(pageUrl);
        return  ObjectRestResponse.ok(signature);

    }
    @GetMapping("/getAccesToken")
    @ResponseBody
    public ObjectRestResponse getAccesToken() throws WxErrorException {
        String accessToken= WxMpConfiguration.getMpServices().get(appid).getAccessToken();
        return  ObjectRestResponse.ok(accessToken);

    }
}
