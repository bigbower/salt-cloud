package com.salt.cloud.tool.oss.controller;


import com.salt.cloud.core.exception.base.BusinessException;
import com.salt.cloud.core.msg.ObjectRestResponse;
import com.salt.cloud.tool.oss.cloud.ImgTools;
import com.salt.cloud.tool.oss.cloud.OSSFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;


/**
 * 文件上传
 *
 * @author kian
 */
@RestController
@RequestMapping("/oss")
public class OssController{
	@Autowired
	private OSSFactory ossFactory;
	/**
	 * 上传文件
	 */
	@RequestMapping("/upload")
	public ObjectRestResponse upload(@RequestParam("file") MultipartFile file, HttpServletRequest request) throws Exception {
		if (file.isEmpty()) {
			throw new BusinessException("上传文件不能为空");
		}
		byte[] filebytes= ImgTools.compressUnderSize(file.getBytes(),1000*1024);
		//上传文件
		String suffix = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
		String url = ossFactory.build().uploadSuffix(filebytes, suffix);
		return new ObjectRestResponse<>().data(url);
	}



}
