package com.salt.cloud.tool.kd100.controller;

import com.salt.cloud.core.msg.ObjectRestResponse;
import com.salt.cloud.tool.kd100.cloud.HttpRequest;
import com.salt.cloud.tool.kd100.cloud.MD5;
import net.sf.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

@RestController
@RequestMapping("/express")
public class Expressinquiry {

    @Value("${kd100.customer}")
    private  String customer;

    @Value("${kd100.key}")
    private  String key;

    @Value("${kd100.url}")
    private  String url;
    /**
     * 订单查询
     * @param logisticsname
     * @param logisticsno
     * @return
     * @throws Exception
     */
    @RequestMapping("inquiry")
    public ObjectRestResponse query(String logisticsname, String logisticsno)throws Exception{
        String param ="{\"com\":\""+logisticsname+"\",\"num\":\""+logisticsno+"\"}";
        String sign = MD5.encode(param+key+customer);
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("param",param);
        params.put("sign",sign);
        params.put("customer",customer);
        String  resp = new HttpRequest().postData(url, params, "utf-8").toString();
        JSONObject respjosn=JSONObject.fromObject(resp);
        return ObjectRestResponse.ok(respjosn);
    }
}
