

package com.salt.cloud.tool.oss.cloud;

import com.salt.cloud.tool.oss.constants.OSSConstant;
import com.salt.cloud.tool.config.CloudStorageConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 文件上传Factory
 *
 * @author kian
 */
@Component
public class OSSFactory {
    @Autowired
    private CloudStorageConfig config;

    public CloudStorageService build() {
        if (config.getType().equals(OSSConstant.TYPE_QINIU)) {
            return new QiniuCloudStorageService(config);
        } else if (config.getType().equals(OSSConstant.TYPE_ALIYUN)) {
            return new AliyunCloudStorageService(config);
        } else if (config.getType().equals(OSSConstant.TYPE_QCLOUD)) {
            return new QcloudCloudStorageService(config);
        }else if(config.getType().equals(OSSConstant.TYPE_LOCAL)){
            return  new LocalUploadService(config);
        }
        return null;
    }

}
