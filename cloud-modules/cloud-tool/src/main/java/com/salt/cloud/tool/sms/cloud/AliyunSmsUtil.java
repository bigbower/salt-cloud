package com.salt.cloud.tool.sms.cloud;

import com.salt.cloud.core.msg.ObjectRestResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsRequest;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;


public class AliyunSmsUtil {
    private static final Logger logger = LoggerFactory.getLogger(AliyunSmsUtil.class);



    //产品名称:云通信短信API产品,开发者无需替换
    static final String product = "Dysmsapi";
    //产品域名,开发者无需替换
    static final String domain = "dysmsapi.aliyuncs.com";

    // TODO 此处需要替换成开发者自己的AK(在阿里云访问控制台寻找)
//    static final String accessKeyId = "LTAIbRlDApjys6Gs";
//    static final String accessKeySecret = "Gy97kVh6cJpEnTACNjR7jHerMwSdEJ";

    public static ObjectRestResponse sendSms(String signName, String accessKeyId, String accessKeySecret, String phone, String templateCode, String val){
        ObjectRestResponse objectRestResponse=new ObjectRestResponse();
        try{
            System.setProperty("sun.net.client.defaultConnectTimeout", "10000");
            System.setProperty("sun.net.client.defaultReadTimeout", "10000");
            //初始化acsClient,暂不支持region化
            IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou", accessKeyId, accessKeySecret);
            DefaultProfile.addEndpoint("cn-hangzhou", "cn-hangzhou", product, domain);
            IAcsClient acsClient = new DefaultAcsClient(profile);
            //组装请求对象-具体描述见控制台-文档部分内容

            SendSmsRequest request = new SendSmsRequest();
            //必填:待发送手机号
            request.setPhoneNumbers(phone);
            //必填:短信签名-可在短信控制台中找到
            request.setSignName(signName);
            //必填:短信模板-可在短信控制台中找到
            //smsType 短信类型  注册(0),修改密码(1),手机号绑定(2),找回密码(3);.
            //String templateCode=null;
//        if(smstype==0){
//        	templateCode="SMS_145596451";
//        }else if(smstype==1) {
//            templateCode = "SMS_145591418";
//        }else if(smstype==2){
//            templateCode = "SMS_152805207";
//        }else{
//        	objectRestResponse.setStatus(101);
//        	objectRestResponse.setMessage("模板有误");
//        	return  objectRestResponse;
//        }
            //templateCode="SMS_168590441";
            request.setTemplateCode(templateCode);
            //可选:模板中的变量替换JSON串,如模板内容为"亲爱的${name},您的验证码为${code}"时,此处的值为
            request.setTemplateParam("{\"code\":\""+val+"\"}");

            //选填-上行短信扩展码(无特殊需求用户请忽略此字段)
            //request.setSmsUpExtendCode("90997");

            //可选:outId为提供给业务方扩展字段,最终在短信回执消息中将此值带回给调用者
            request.setOutId("yourOutId");

            //hint 此处可能会抛出异常，注意catch
            SendSmsResponse sendSmsResponse = acsClient.getAcsResponse(request);
            if(!"OK".equals(sendSmsResponse.getCode())){
                objectRestResponse.setStatus(102);
                objectRestResponse.setMessage(sendSmsResponse.getMessage());
                return objectRestResponse;
            }
            System.out.println(sendSmsResponse.getCode());
            System.out.println(sendSmsResponse.getMessage());
        }catch (Exception e) {
            logger.error("发送短息异常e：", e.getMessage());
            objectRestResponse.setStatus(103);
            objectRestResponse.setMessage("出现异常");
            return  objectRestResponse;
        }

        return ObjectRestResponse.ok("发送成功");


    }
}
