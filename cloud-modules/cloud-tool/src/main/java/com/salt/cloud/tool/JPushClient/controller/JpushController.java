package com.salt.cloud.tool.JPushClient.controller;

import cn.jpush.api.push.model.audience.Audience;
import com.google.gson.JsonObject;
import com.salt.cloud.tool.JPushClient.utils.JpushClientUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/jpush")
public class JpushController {

    @Value("${Jpush.appKey}")
    private   String appKey;

    @Value("${Jpush.masterSecret}")
    private  String masterSecret;

    @RequestMapping("/sendRemaid")
    public void sendRemaid(String userId,String shopId,String id, String title,String content){
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("id",id);
        jsonObject.addProperty("shopId",shopId);
        JpushClientUtil.sendToAll(masterSecret,appKey,Audience.alias(userId),title,title,content,jsonObject.toString());
    }


}
