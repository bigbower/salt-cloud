package com.salt.cloud.tool.sms.controller;

import com.salt.cloud.core.msg.ObjectRestResponse;
import com.salt.cloud.tool.sms.cloud.AliyunSmsUtil;
import com.salt.cloud.tool.sms.cloud.SmsUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/sms")
public class AliyunSmsController {
    @Value("${AliyunSms.accessKeyId}")
    private   String accessKeyId;

    @Value("${AliyunSms.accessKeySecret}")
    private  String accessKeySecret;

    @Value("${AliyunSms.signName}")
    private  String signName;

    //SMS_168590441
    @RequestMapping("/sendout")
    public ObjectRestResponse sendout(String phone, String templateCode, String value){
        return AliyunSmsUtil.sendSms(signName,accessKeyId,accessKeySecret,phone,templateCode,value);
    }

    @RequestMapping("/sendMessage")
    public ObjectRestResponse sendMessage(String  phone, String content){
        return SmsUtil.sendSms1(phone,content);
    }


}
