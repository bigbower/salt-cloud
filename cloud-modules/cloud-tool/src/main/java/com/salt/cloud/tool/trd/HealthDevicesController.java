package com.salt.cloud.tool.trd;

import com.salt.cloud.core.msg.ObjectRestResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/health")
@Slf4j
public class HealthDevicesController {

    @PostMapping("content")
    public ObjectRestResponse test(String content){
        log.info(content);
        return ObjectRestResponse.ok(content);
    }
}
