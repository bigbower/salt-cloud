

package com.salt.cloud.tool.oss.constants;

/**
 * @author kian
 * @create 2018/3/4.
 */
public class OSSConstant {
    //类型 1：七牛  2：阿里云  3：腾讯云 4:本地
    public final static Integer TYPE_QINIU = 1;
    public final static Integer TYPE_ALIYUN = 2;
    public final static Integer TYPE_QCLOUD = 3;
    public final static Integer TYPE_LOCAL = 4;
}
