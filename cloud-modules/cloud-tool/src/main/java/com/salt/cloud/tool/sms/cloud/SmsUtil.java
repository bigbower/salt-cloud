package com.salt.cloud.tool.sms.cloud;


import com.salt.cloud.core.msg.ObjectRestResponse;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

/** 
 * 说明：短信接口类
 * @version
 */
public class SmsUtil {
	private static final String account ="71038";
	private static final String accountName ="yecf";
	private static final String password ="e66f36fd7de33c46";
	public static void main(String [] args) {
		
		sendSms1("18155183642","【汽车之家】您今天有5件待办事项，请注意查看。");

		//sendSms1();
	}

	 //短信商 一  http://www.dxton.com/ =====================================================================================
	/**
	 * 给一个人发送单条短信
	 * @param mobile 手机号
	 * @param code  短信内容
	 */
 	public static ObjectRestResponse sendSms1(String mobile, String code){
		ObjectRestResponse restResponse = new ObjectRestResponse();
 		String PostData = "";
		try {
			PostData = "f="+1+"&uid="+account+"&un="+accountName+"&pw="+password+"&p="+mobile+"&i="+URLEncoder.encode(code,"utf-8");
		} catch (UnsupportedEncodingException e) {
			System.out.println("短信提交失败");
		}
		 //System.out.println(PostData);
 	     String ret = SMS(PostData, "http://61.191.26.189:8888/smser.ashx");
		System.out.printf("___"+ret+"___");
		if("00".equals(ret.replace("\n",""))){
			restResponse.setStatus(200);
			restResponse.setMessage("验证码发送成功");
		}else{
			restResponse.setStatus(-1);
			restResponse.setMessage("短信接口返回码："+ret);
		}
 	     return restResponse;
 	   /*  
 	   00			发送成功
 	   01			参数无效
 	   02			权鉴失败
 	   03			一天内登录错误次数超过最大限制（50 次），ip 被拦截。
 	   04			用户的 IP 不在已绑定 IP 范围内
 	   106			账户余额不足
 	   -99			服务器接收失败
 	   表现为某个字、词组			内容有屏蔽字
		*/
 	     
	}
	public static String SMS(String postData, String postUrl) {
		try {
			//发送POST请求
			URL url = new URL(postUrl);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
			conn.setRequestProperty("Connection", "Keep-Alive");
			conn.setUseCaches(false);
			conn.setDoOutput(true);
			conn.setRequestProperty("Content-Length", "" + postData.length());
			OutputStreamWriter out = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
			out.write(postData);
			out.flush();
			out.close();
			//获取响应状态
			if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
				System.out.println("connect failed!");
				return "";
			}
			//获取响应内容体
			String line, result = "";
			BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "utf-8"));
			while ((line = in.readLine()) != null) {
				result += line + "\n";
			}
			in.close();
			return result;
		} catch (IOException e) {
			e.printStackTrace(System.out);
		}
		return "";
	}

	
	/**
	 * 给多个人发送单条短信
	 * @param list 手机号验证码
	 */
//	public static void sendSmsAll(List<PageData> list){
//		String code;
//		String mobile;
//		for(int i=0;i<list.size();i++){
//			code=list.get(i).get("code").toString();
//			mobile=list.get(i).get("mobile").toString();
//			sendSms2(mobile,code);
//		}
//	}
	// =================================================================================================
	
	
	
}

