package com.salt.cloud.tool.JPushClient.utils;

import com.alibaba.fastjson.JSONObject;
import com.google.gson.JsonObject;

import cn.jpush.api.JPushClient;
import cn.jpush.api.push.PushResult;
import cn.jpush.api.push.model.Message;
import cn.jpush.api.push.model.Options;
import cn.jpush.api.push.model.Platform;
import cn.jpush.api.push.model.PushPayload;
import cn.jpush.api.push.model.audience.Audience;
import cn.jpush.api.push.model.notification.AndroidNotification;
import cn.jpush.api.push.model.notification.IosNotification;
import cn.jpush.api.push.model.notification.Notification;

public class JpushClientUtil {


//	private static String appKey = "edce7f5e3f420fc124509f19";
//
//   private static String masterSecret = "23c562d4dffb518e1b301ff9";

    //private static JPushClient jPushClient = new JPushClient(masterSecret,appKey);

    /**
     * 发送给所有用户
     * @param notification_title 通知内容标题
     * @param msg_title 消息内容标题
     * @param msg_content 消息内容
     * @param extrasparam 扩展字段
     * @return 0推送失败，1推送成功
     */
    public static int sendToAll(String masterSecret,String appKey,Audience audience, String notification_title, String msg_title, String msg_content, String extrasparam) {
        int result = 0;
        try {
            JPushClient jPushClient = new JPushClient(masterSecret,appKey);
            PushPayload pushPayload= JpushClientUtil.buildPushObject_android_and_ios(audience,notification_title,msg_title,msg_content,extrasparam);
            System.out.println(pushPayload);
            PushResult pushResult=jPushClient.sendPush(pushPayload);
            System.out.println(pushResult);
            if(pushResult.getResponseCode()==200){
                result=1;
            }
        } catch (Exception e) {

            e.printStackTrace();
        }

        return result;
    }

    /**
     * 发送给所有安卓用户
     * @param notification_title 通知内容标题
     * @param msg_title 消息内容标题
     * @param msg_content 消息内容
     * @param extrasparam 扩展字段
     * @return 0推送失败，1推送成功
     */
    public static int sendToAllAndroid(String masterSecret,String appKey,Audience audience, String notification_title, String msg_title, String msg_content, String extrasparam) {
        int result = 0;
        try {
            JPushClient jPushClient = new JPushClient(masterSecret,appKey);
            PushPayload pushPayload= JpushClientUtil.buildPushObject_android_all_alertWithTitle(audience,notification_title,msg_title,msg_content,extrasparam);
            System.out.println(pushPayload);
            PushResult pushResult=jPushClient.sendPush(pushPayload);
            System.out.println(pushResult);
            if(pushResult.getResponseCode()==200){
                result=1;
            }
        } catch (Exception e) {

            e.printStackTrace();
        }

        return result;
    }
    /**
     * * 发送给所有安卓用户
     * notification_title 通知内容标题
     * msg_title 消息内容标题
     * msg_content 消息内容
     * extrasparam 扩展字段
     * @return 0推送失败，1推送成功
     * @param args
     */
    public static void main(String[] args){

        JsonObject jsonObject = new JsonObject();

//        if(JpushClientUtil.sendToAllAndroid(Audience.all(),"testIos","AAAAAAAAAA","巴巴爸爸不不不不不不不不","")==1){
//        	 System.out.println("success");
//        }
    }


    private static PushPayload buildPushObject_android_all_alertWithTitle(Audience audience , String notification_title, String msg_title, String msg_content, String extrasparam) {
        System.out.println("----------buildPushObject_android_registrationId_alertWithTitle");
        return PushPayload.newBuilder()
                //指定要推送的平台，all代表当前应用配置了的所有平台，也可以传android等具体平台
                .setPlatform(Platform.android())
                //指定推送的接收对象，all代表所有人，也可以指定已经设置成功的tag或alias或该应应用客户端调用接口获取到的registration id
                .setAudience(audience)
                //jpush的通知，android的由jpush直接下发，iOS的由apns服务器下发，Winphone的由mpns下发
                .setNotification(Notification.newBuilder()
                        //指定当前推送的android通知
                        .addPlatformNotification(AndroidNotification.newBuilder()
                                .setAlert(notification_title)
                                .setTitle(notification_title)
                                //此字段为透传字段，不会显示在通知栏。用户可以通过此字段来做一些定制需求，如特定的key传要指定跳转的页面（value）
                                .addExtra("extrasparam",extrasparam)
                                .build())
                        .build()
                )
                //Platform指定了哪些平台就会像指定平台中符合推送条件的设备进行推送。 jpush的自定义消息，
                // sdk默认不做任何处理，不会有通知提示。建议看文档http://docs.jpush.io/guideline/faq/的
                // [通知与自定义消息有什么区别？]了解通知和自定义消息的区别
                .setMessage(Message.newBuilder()
                        .setMsgContent(msg_content)
                        .setTitle(msg_title)
                        .addExtra("extrasparam",extrasparam)
                        .build())

                .setOptions(Options.newBuilder()
                        //此字段的值是用来指定本推送要推送的apns环境，false表示开发，true表示生产；对android和自定义消息无意义
                        .setApnsProduction(false)
                        //此字段是给开发者自己给推送编号，方便推送者分辨推送记录
                        .setSendno(1)
                        //此字段的值是用来指定本推送的离线保存时长，如果不传此字段则默认保存一天，最多指定保留十天，单位为秒
                        .setTimeToLive(86400)
                        .build())
                .build();
    }

    public static PushPayload buildPushObject_android_and_ios(Audience audience ,String notification_title, String msg_title, String msg_content, String extrasparam) {
        JSONObject jsonObject = new JSONObject();
        //@SuppressWarnings("static-access")
        JSONObject parse = jsonObject.parseObject(extrasparam);
        return PushPayload.newBuilder()
                .setPlatform(Platform.android_ios())
                .setAudience(audience)
                .setNotification(Notification.newBuilder()
                                .setAlert(notification_title)
                                .addPlatformNotification(AndroidNotification.newBuilder()
                                                .setAlert(msg_content)
                                                .setTitle(msg_title)
                                                .addExtra("id",parse.getString("id"))
                                                //此字段为透传字段，不会显示在通知栏。用户可以通过此字段来做一些定制需求，如特定的key传要指定跳转的页面（value）
//                                .addExtra("content",parse.getString("content"))
//                        		jsonObject.addProperty("id", server_order_push.getId());
//                				jsonObject.addProperty("user_type", server_order_push.getOrder_state());
//                				jsonObject.addProperty("typeId", server_order_push.getType());
//                				jsonObject.addProperty("dept", server_order_push.getDept());
//                				jsonObject.addProperty("message_id", message.getId());
                                                /*.addExtra("id",parse.getString("id"))
                                                .addExtra("user_type",parse.getString("user_type"))
                                                .addExtra("typeId",parse.getString("typeId"))
                                                .addExtra("dept",parse.getString("dept"))
                                                .addExtra("message_id",parse.getString("message_id"))
                                                .addExtra("type",parse.getString("type"))
                                                .addExtra("server_way",parse.getString("server_way"))//服务方式，现场非现场
                */                                .build()
                                )
                                .addPlatformNotification(IosNotification.newBuilder()
                                                //传一个IosAlert对象，指定apns title、title、subtitle等
                                                .setAlert(notification_title)
                                                //直接传alert
                                                //此项是指定此推送的badge自动加1
                                                .incrBadge(1)
                                                //此字段的值default表示系统默认声音；传sound.caf表示此推送以项目里面打包的sound.caf声音来提醒，
                                                // 如果系统没有此音频则以系统默认声音提醒；此字段如果传空字符串，iOS9及以上的系统是无声音提醒，以下的系统是默认声音
//                                .setSound("sound.caf")
                                                //{"content":"312313","title":"21312","remark":"备注：后台配置的url 链接，需要跳转到一个html页面，将html的内容展示","type":"1"}
                                                //此字段为透传字段，不会显示在通知栏。用户可以通过此字段来做一些定制需求，如特定的key传要指定跳转的页面（value）
//                                .addExtra("content",parse.getString("content"))
//                                .addExtra("id",parse.getString("id"))//订单id
//                                .addExtra("user_type",parse.getString("user_type"))//订单状态
//                                .addExtra("typeId",parse.getString("typeId"))//服务类型
//                                .addExtra("dept",parse.getString("dept"))//服务人员
//                                .addExtra("message_id",parse.getString("message_id"))//消息id
//                                .addExtra("type",parse.getString("type"))//跳转的页面类型
//                                .addExtra("server_way",parse.getString("server_way"))//现场非现场
                                                //此项说明此推送是一个background推送，想了解background看：http://docs.jpush.io/client/ios_tutorials/#ios-7-background-remote-notification
                                                .setContentAvailable(false)

                                                .build()
                                )
                                .build()
                )
                //Platform指定了哪些平台就会像指定平台中符合推送条件的设备进行推送。 jpush的自定义消息，
                // sdk默认不做任何处理，不会有通知提示。建议看文档http://docs.jpush.io/guideline/faq/的
                // [通知与自定义消息有什么区别？]了解通知和自定义消息的区别
                .setMessage(Message.newBuilder()
                        .setMsgContent(msg_content)
                        .setTitle(msg_title)
                        .build())

                .setOptions(Options.newBuilder()
                        //此字段的值是用来指定本推送要推送的apns环境，false表示开发，true表示生产；对android和自定义消息无意义
                        .setApnsProduction(true)
                        //此字段是给开发者自己给推送编号，方便推送者分辨推送记录
                        .setSendno(1)
                        //此字段的值是用来指定本推送的离线保存时长，如果不传此字段则默认保存一天，最多指定保留十天，单位为秒
                        .setTimeToLive(86400)
                        .build()
                )
                .build();
    }

}